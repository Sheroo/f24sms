<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToScheduler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scheduler', function (Blueprint $table) {
            $table->bigInteger('segment_id')->nullable();
            $table->string('schedule_interval',12)->nullable()->change();
            $table->Integer('scheduled_hour')->nullable()->change();
            $table->bigInteger('segment_scheduler_hours')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduler', function (Blueprint $table) {
            //
        });
    }
}
