<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface CrudInterface
{
    public function setDefaultCrudConfiguration();

    public function tableColumns();

    public function index(Request $request);

    public function getItemsData(Request $request, $ajax = true);

    public function searchItemsDataByKeyword(Request $request, $eloquentObj);

    public function applyJoins(Request $request, $eloquentObj);

    public function applyGroupBy(Request $request, $eloquentObj);

    public function selectColumns(Request $request, $eloquentObj);

    public function getItemsView(Request $request);

    public function getOrderByParams(Request $request);

    public function getPaginationParams(Request $request);

    public function ajax_get_items(Request $request);

    public function _prepare_item_eloquent_obj($itemEloquentObj, $data);

    public function store(Request $request);

    public function update(Request $request);

    public function inline_update(Request $request);

    public function destroy(Request $request);

    public function bulkDelete(Request $request);

    public function edit_form(Request $request);

    public function ajaxActions(Request $request);

    public function customAjaxActions(Request $request);

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData);

    public function appendViewDataInGetItemData(Request $request, $viewData);

    public function _post_store_action(Request $request, $itemEloquentObj , &$response);
}
