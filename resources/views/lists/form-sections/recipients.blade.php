<div class="item-form-section message_information-section"
     data-slug="{{$controller_slug}}"
     data-section="message_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Recipients Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">


        <div class="recipient-info-wrapper">
            <div class="recipient-info recipient-info-default">
                <div class="form-group clearfix">

                    <button type="button" class="close  delete_button" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <label class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input class="form-control form-white te-input te-required"
                               name="recipient[first_name][]"
                               value=""
                               type="text"
                               placeholder="First Name"/>
                    </div>
                </div>

                <div class="form-group clearfix">

                    <label class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input class="form-control form-white te-input te-required"
                               name="recipient[last_name][]"
                               value=""
                               type="text"
                               placeholder="Last Name"/>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input class="form-control form-white te-input te-validate-email"
                               name="recipient[email][]"
                               value=""
                               type="text"
                               placeholder="Email"/>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input class="form-control form-white te-input te-required"
                               name="recipient[phone][]"
                               value=""
                               type="text"
                               placeholder="Phone"/>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-success btn-add-recipient">Add More Recipient</button>

    </div>

</div>
