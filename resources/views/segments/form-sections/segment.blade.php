<div class="item-form-section list_message-section"
     data-slug="{{$controller_slug}}"
     data-section="list_message">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Segmentation Lists
        </h3>
    </div>
    <div class="item-form-section-body clearfix">
        <?php
        $lists = isset($lists) ? $lists : [];
        ?>
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">Clicked</label>
                <div class="col-sm-9">
                    <select name="list_id" id="listid" class="select-picker form-control form-white te-input te-required">
                        <option value="">Select List</option>


                        {{@$itemObj->list_id}}

                        @foreach($lists as $list)
                            <option
                                {{@$itemObj->list_id==$list->id ? 'selected="selected"':''}} value="{{$list->id}}">{{$list->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">List</label>
            <div class="col-sm-9">
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" tabindex="0" checked>
                    <label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
            </div>
        </div>




    </div>
</div>
