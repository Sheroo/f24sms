/**
 * Created by usmani on 28/02/2017.
 */


function te_show_hide_func(show_hide, _id) {

    jQuery('[te-' + show_hide + ']').each(function (index, elem) {

        var this_elem_id = '';

        var te_show_hide = String(jQuery(elem).attr('te-' + show_hide));

        if (te_show_hide != '' && te_show_hide != 'undefined') {

            te_show_hide = te_show_hide.trim();

            var te_show_hide_arr = [];

            var conditional_operator = '';

            if (te_show_hide.includes('&&')) {
                te_show_hide_arr = te_show_hide.split('&&');
                conditional_operator = 'AND';
            } else if (te_show_hide.includes('||')) {
                te_show_hide_arr = te_show_hide.split('||');
                conditional_operator = 'OR';
            } else {
                te_show_hide_arr.push(te_show_hide);
            }

            var true_false_count = 0;

            var show_hide_for_only_this_id = false;

            for (var i in te_show_hide_arr) {

                var condition = te_show_hide_arr[i];
                if (condition.includes('!=')) {

                    var $show_parts = condition.split('!=');
                    var id = $show_parts[0].trim();
                    var value_check = $show_parts[1] ? $show_parts[1].trim() : '';
                    value_check = value_check.replace(/'/g, '').replace(/"/g, '');

                    true_false_count += jQuery('#' + id).val() != value_check ? 1 : 0;

                    this_elem_id = id;

                } else if (condition.includes('=')) {

                    var $show_parts = condition.split('=');
                    var id = $show_parts[0].trim();
                    var value_check = $show_parts[1] ? $show_parts[1].trim() : '';
                    value_check = value_check.replace(/'/g, '').replace(/"/g, '');

                    true_false_count += jQuery('#' + id).val() == value_check ? 1 : 0;

                    this_elem_id = id;

                } else if (condition.includes('!')) {

                    var $show_parts = condition.split('!');
                    var id = $show_parts[1] ? $show_parts[1].trim() : '';

                    var _elem = jQuery('#' + id);
                    var dom_elem = _elem[0];
                    if (dom_elem !== undefined) {
                        var input_type = dom_elem.type;
                        if (input_type == 'checkbox' || input_type == 'radio') {
                            true_false_count += !(_elem.is(':checked')) ? 1 : 0;
                        } else {
                            true_false_count += !(_elem.val()) ? 1 : 0;
                        }
                    }

                    this_elem_id = id;
                } else {

                    var id = condition;
                    var _elem = jQuery('#' + id);
                    var dom_elem = _elem[0];
                    if (dom_elem !== undefined) {
                        var input_type = dom_elem.type;
                        if (input_type == 'checkbox' || input_type == 'radio') {
                            true_false_count += (_elem.is(':checked')) ? 1 : 0;
                        } else {
                            true_false_count += (_elem.val()) ? 1 : 0;
                        }
                    }
                    this_elem_id = id;

                }

                if (show_hide_for_only_this_id == false && this_elem_id == _id) {
                    show_hide_for_only_this_id = true;
                }

            }

            var show_hide_check = true_false_count == te_show_hide_arr.length;

            if (conditional_operator == 'OR') {
                show_hide_check = true_false_count > 0;
            }

            if (show_hide_for_only_this_id) {

                if (show_hide_check) {
                    if (show_hide == 'hide') {
                        jQuery(elem).hide();
                    } else if (show_hide == 'show') {
                        jQuery(elem).show();
                    }
                } else {
                    if (show_hide == 'hide') {
                        jQuery(elem).show();
                    } else if (show_hide == 'show') {
                        jQuery(elem).hide();
                    }
                }
            }
        }
    });
}

function te_bind_func(_elem) {
    var $this = jQuery(_elem);
    var $thisId = String($this.attr('id'));

    if ($thisId == 'undefined' || $thisId == '') {
        return;
    }

    if ($this[0].tagName == 'SELECT' || $this[0].tagName == 'INPUT' || $this[0].tagName == 'TEXTAREA') {
        te_show_hide_func('show', $thisId);
        te_show_hide_func('hide', $thisId);
    }
}

jQuery(document).ready(function () {

    jQuery('.te-bind').each(function (index, elem) {
        te_bind_func(jQuery(elem));
    });

    jQuery('body').on('change input', '.te-bind', function (e) {

        e.preventDefault();
        te_bind_func(jQuery(this));

    });

});


function addAttrChangeListener() {
    (function ($) {
        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

        $.fn.attrchange = function (callback) {
            if (MutationObserver) {
                var options = {
                    subtree: false,
                    attributes: true
                };

                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (e) {
                        callback.call(e.target, e.attributeName);
                    });
                });

                return this.each(function () {
                    observer.observe(this, options);
                });

            }
        }
    })(jQuery);

}

addAttrChangeListener();
// jQuery(document).ready(function(){


// });