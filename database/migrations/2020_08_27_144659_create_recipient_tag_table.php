<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipient_tag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('recipient_id');
//            $table->foreign('recipient_id')->references('id')->on('recipients');
            $table->bigInteger('tag_id');
//            $table->foreign('tag_id')->references('id')->on('tags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipient_tag');
    }
}
