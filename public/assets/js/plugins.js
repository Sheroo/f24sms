var te_alert_id = 12345;

function te_alert(msg, type) {
    return '<div class="alert alert-auto-hide alert-' + type + '">' + msg + '</div>';
}

function toObject(arr) {
    var rv = {};
    if (arr) {
        for (var i in arr) {
            rv[i] = arr[i];
        }
    }
    return rv;
}


function show_error_modal_in_ajax_callback(action, responseText, statusText) {


    if (responseText && responseText.indexOf("Unauthorized") >= 0) {
        swal({
            title: "Session Expired",
            text: "Your session is expired, Please login again!",
            type: "info",
            customClass: 'custom_alert_popup',
            html: true,
            confirmButtonColor: "#18A689",
            confirmButtonText: "OK, Close it",
        }, function () {
            redirectURI = encodeURI(window.location.href);
            window.location.href = f24.base_url + '?redirect=' + redirectURI;
        });
    } else {

        if (action != 'browser/checkstatus') {
            if (statusText != "abort") {

                var session_error = false;
                var type = 'error';
                var title = "Whoops, something went wrong.";

                var text = responseText ? responseText : '';// === undefined ? '' : responseText;
                var found = text.search("TokenMismatchException");
                if (found != -1) {
                    session_error = true;
                    title = 'Session Expired';
                    text = "Your session is expired, Please login again!";
                    type = "info";
                } else {
                    text = (typeof text == 'object') ? JSON.stringify(text) : text;
                    text = jQuery('<div>').attr({id: 'custom_swal_alert_popup'}).html(text);
                    text.find('style').remove();
                }
                swal({
                    title: title,
                    text: jQuery('<div>').html(text).html(),
                    type: type,
                    customClass: 'custom_alert_popup',
                    confirmButtonColor: "#18A689",
                    confirmButtonText: "OK, Close it",
                    closeOnConfirm: true,
                    allowOutsideClick: true,
                    html: true
                }, function () {
                    if (session_error) {

                        redirectURI = encodeURI(window.location.href);
                        window.location.href = f24.base_url + '?redirect=' + redirectURI;
                    }
                });
            }
        }

    }
}

function te_ajax(action, serialized_data, success_func, fail_func, error_func, el) {
    error_func = error_func || '';
    if (el) {
        el.show_loading();
    }

    lastrequest = jQuery.ajax({
        type: "POST",
        url: f24.base_url + action,
        data: serialized_data,
        dataType: 'json',
        cache: false,
        headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},
        success: function (resp) {
            if (el) {
                el.hide_loading();
            }
            // trashTimeSession();
            if (resp['status'] == 'success') {
                success_func(resp);

            } else {
                fail_func(resp);

            }
        },
        error: function (response, status, error, xhr) {
            // trashTimeSession();
            if (el) {
                el.hide_loading();
            }

            var respText = response.responseText !== undefined ? response.responseText : false;
            var statusText = response.statusText !== undefined ? response.statusText : false;
            show_error_modal_in_ajax_callback(action, respText, statusText);

        }
    });
    return lastrequest;

}

(function ($) {
    $.fn.loader = function (add) {
        add = typeof add == 'undefined' ? true : add;
        if (add === true) {
            jQuery('<div>').addClass('preloader').append(
                jQuery('<div>').addClass('cssload-speeding-wheel')
            ).appendTo(jQuery(this));
        } else {
            jQuery(this).find('.preloader').remove();
        }
        return this;
    };

    $.fn.ajaxForm = function (options) {
        return this.each(function () {
            var form = jQuery(this);
            var formId = String(form.attr('id'));
            formId = (formId == 'undefined' || formId == '') ? form.attr('name') : formId;
            // Default options
            var settings = $.extend({
                url: form.attr('action'),
                method: (typeof form.attr('method') !== 'undefined') ? form.attr('method') : 'POST',
                redirectTo: (typeof form.data('redirectto') == 'undefined') ? '' : form.data('redirectto'),
                redirectTime: (typeof form.data('redirecttime') == 'undefined') ? 3000 : form.data('redirecttime'),
                removeForm: (typeof form.data('removeform') !== 'undefined'),
                resetForm: (typeof form.data('resetform') !== 'undefined'),
                beforeSubmitCallback: (typeof form.data('beforeSubmitCallback') !== 'undefined') ? form.data('beforeSubmitCallback') : formId + '_beforecallback',
                callback: (typeof form.attr('method') !== 'undefined') ? form.attr('method') : formId + '_callback',
                errorCallback: (typeof form.attr('method') !== 'undefined') ? form.attr('method') : formId + '_errorcallback',
                submitOnEnter: false,
            }, options);

            // preparing ajax request
            var url = settings.url;
            var method = settings.method;
            var formData = form.serialize();
            var redirectUrl = settings.redirectTo;
            var redirectTime = settings.redirectTime;
            form.show_loading();
            var btn = form.find('[type="submit"]');
            btn.addClass('disabled formLoader').attr('disabled', 'disabled');

            // Before form submitting callback
            if (settings.beforeSubmitCallback.length > 0) {
                var beforeSubmitCallback = settings.beforeSubmitCallback;
                if (typeof beforeSubmitCallback !== 'function')
                    beforeSubmitCallback = window[beforeSubmitCallback];
                if (typeof beforeSubmitCallback === 'function') {
                    var cbResponse = beforeSubmitCallback(form, btn, url, formData);
                    url = (typeof cbResponse.url === 'undefined') ? url : cbResponse.url;
                    formData = (typeof cbResponse.formData === 'undefined') ? formData : cbResponse.formData;
                }
            }

            jQuery.ajax({
                url: url,
                method: method,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function (xhr, obj) {
                    form.parent().find('.ajaxErrorContainer').remove();
                },
                error: function (xhr, status, error) {
                    form.hide_loading();
                    var response = xhr.responseText;
                    if (xhr.status == 400) {
                        if (typeof form.data('printresult') !== 'undefined') {
                            $(response).addClass('ajaxErrorContainer').insertBefore(form);
                        }
                    }
                    btn.removeClass('disabled formLoader').removeAttr('disabled');
                    if (settings.errorCallback.length > 0) {
                        var callback = settings.errorCallback;
                        if (typeof callback !== 'function')
                            callback = window[callback];
                        if (typeof callback === 'function')
                            callback(form, formData, xhr);
                    }
                },
                success: function (response) {
                    form.hide_loading();
                    var cbReponse = true;
                    if (settings.callback.length > 0) {
                        var callback = settings.callback;
                        if (typeof callback !== 'function')
                            callback = window[callback];
                        if (typeof callback === 'function')
                            cbReponse = callback(form, formData, response);
                    }
                    if (cbReponse && typeof response.status !== 'undefined' && response.status == 1) {
                        if (settings.removeForm) {
                            form.remove();
                        }
                        if (settings.resetForm) {
                            form.find('[type="reset"]').click();
                        }
                        if (redirectUrl.length > 0) {
                            setTimeout(function () {
                                window.location.assign(redirectUrl);
                            }, redirectTime);
                        }
                    }
                    btn.removeClass('disabled formLoader').removeAttr('disabled');
                }
            });

            if (typeof settings.submitOnEnter === 'object') {
                settings.submitOnEnter.on('keydown', function (e) {
                    if (e.which == 13 && e.shiftKey) {
                        jQuery(this).val(jQuery(this).val() + "\n\r");

                    } else if (e.which == 13) {
                        if (jQuery(this).val().length > 0)
                            form.trigger('submit');
                        e.preventDefault();
                    }
                });
                settings.submitOnEnter.on('keyup', function (e) {
                    if (e.which == 13) {
                        e.preventDefault();
                    }
                });
            }
            return this;
        });
    };

    $.fn.addAjax = function (callback, beforeSubmitCallback) {
        return this.each(function () {
            beforeSubmitCallback = beforeSubmitCallback || false;
            var btn = jQuery(this);
            btn.on('click', function (e) {
                btn = jQuery(this);
                e.preventDefault();
                if (beforeSubmitCallback !== false) {
                    if (typeof beforeSubmitCallback !== 'function')
                        beforeSubmitCallback = window[beforeSubmitCallback];
                    if (typeof beforeSubmitCallback === 'function')
                        beforeSubmitCallback(btn);
                }
                if (typeof btn.data('alertmsg') !== 'undefined') {
                    if (!confirm(btn.data('alertmsg'))) {
                        return false;
                    }
                }
                btn.addClass('disabled').attr('disabled', 'disabled');
                jQuery.ajax({
                    url: btn.attr('href'),
                    method: 'GET',
                    success: function (data) {
                        btn.removeClass('disabled').removeAttr('disabled');
                        if (typeof callback !== 'function')
                            callback = window[callback];
                        if (typeof callback === 'function')
                            callback(btn, data);
                    },
                    error: function (data) {
                        btn.removeClass('disabled').removeAttr('disabled');
                    }
                });
            });
            return this;
        });
    };

    $.fn.irbScrollToElement = function () {
        var scrollTo = $('.chatMessagesArea').prop('scrollHeight') + 'px';
        $('.chatMessagesArea').slimScroll({scrollTo: scrollTo});
        return this;
    };

    $.fn.scrollBodyTo = function (duration) {

        if (arguments.length == 1) {
            duration = arguments[0];
            duration = parseInt(duration);
            duration = isNaN(duration) ? 100 : duration;
        } else {
            duration = 100;
        }

        var offset = 85;
        if (arguments.length == 2) {
            offset = arguments[1];
            offset = parseInt(offset);
            offset = isNaN(offset) ? 85 : offset;
        }
        return this.each(function () {

            var top_offset = $(this).offset().top;
            top_offset = top_offset - offset;
            $('html, body').animate({
                scrollTop: top_offset
            }, duration);

        });
    };

    $.fn.serializeToObject = function () {

        var serializedObj = {};
        var $this = $(this);

        var serialized_array = $this.serializeArray();

        for (var i in serialized_array) {
            if (serialized_array[i]['name']) {
                var name = serialized_array[i]['name'];
                if (name.toString().indexOf('[]') != -1) {
                    name = name.replace("[]", "");
                    if (!serializedObj[name]) {
                        serializedObj[name] = [];
                    }
                    serializedObj[name].push(serialized_array[i]['value']);

                } else {

                    serializedObj[name] = serialized_array[i]['value'];
                }
            }
        }

        return serializedObj;

    };

    $.fn.show_loading = function () {
        return this.each(function () {
            $(this).css('position', 'relative').prepend('<div class="te-loading"></div>');
        });
    };

    $.fn.hide_loading = function () {
        return this.each(function () {
            jQuery.each($(this).find('.te-loading'), function (index, element) {
                jQuery(element).remove();
            });
        });
    };

    $.fn.te_show_alert = function (msg, type) {

        var auto_hide = 0;
        if (arguments.length >= 3) {
            auto_hide = arguments[2];
            auto_hide = isNaN(auto_hide) ? 5 : auto_hide;
        }

        return this.each(function () {

            $this = $(this);

            var te_alert = te_alert_html(msg, type);

            if (arguments.length >= 4 && arguments[3] == 'append') {
                $this.append(te_alert.html);
            } else {
                $this.prepend(te_alert.html);
            }

            if (auto_hide != 0) {
                $this.find('#te-alert-' + te_alert.id).delay(auto_hide * 1000).fadeOut(350);
            }
        });

    };

    $.fn.tedata = function (data) {

        var dt = String($(this).attr("data-" + data));
        return dt == '' || dt == 'undefined' ? '' : dt;

    };

    $.fn.te_mark_error = function (error) {

        return this.each(function () {

            $this = $(this);
            $this.parents('.form-group').addClass('te-error').append('<span class="te-error-msg">' + error + '</span>');

        });

    };

    $.fn.te_reset_state_dropdown = function (state) {

        return this.each(function () {

            $this = $(this);
            $this.find('li').show();
            $this.find('li[data-state=' + state + ']').hide();

            $this.parent().find('.te-table-status-btn').removeClass('enabled').removeClass('paused').removeClass('archived');
            $this.parent().find('.te-table-status-btn').addClass(state);

        });

    };

    $.fn.te_hide_error_msgs = function () {

        return this.each(function () {

            $this = $(this);
            $this.find('.form-group').removeClass('te-error');
            $this.find('.te-error-msg').remove();

        });

    };


    $.fn.te_hide_alert = function () {

        return this.each(function () {

            $this = $(this);
            $this.find('.te-alert-msg').remove();

        });

    };

    $.fn.te_animate = function (animation, speed) {
        return this.each(function () {
            var $this = $(this);
            var thisSpeed = String(speed);
            var thisAnimatedClass = "animated" + ((thisSpeed == 'fast' || thisSpeed == 'faster') ? '-' + thisSpeed : '');
            $this.addClass(thisAnimatedClass).addClass(animation);
            setTimeout(function () {
                $this.removeClass(thisAnimatedClass).removeClass(animation);
            }, 600);
        })
    };

    $.fn.flyout = function (showOrHide) {

        return this.each(function () {
            var $this = $(this);
            var thisSpeed = String(speed);
            var thisAnimatedClass = "animated" + ((thisSpeed == 'fast' || thisSpeed == 'faster') ? '-' + thisSpeed : '');
            $this.addClass(thisAnimatedClass).addClass(animation);
            setTimeout(function () {
                $this.removeClass(thisAnimatedClass).removeClass(animation);
            }, 600);
        })
    };

    $.fn.te_animate_out = function (animation, speed, callback) {
        return this.each(function () {
            var $this = $(this);
            var thisSpeed = String(speed);
            var thisAnimatedClass = "animated" + ((thisSpeed == 'fast' || thisSpeed == 'faster') ? '-' + thisSpeed : '');
            var animate_out_time = 600;
            if (thisAnimatedClass == 'animated-fast') {
                animate_out_time = 200;
            } else if (thisAnimatedClass == 'animated-faster') {
                animate_out_time = 100;
            }
            $this.addClass(thisAnimatedClass).addClass(animation);
            setTimeout(function () {
                $this.removeClass(thisAnimatedClass).removeClass(animation);
                $this.hide();
                if (callback !== undefined) {
                    callback();
                }
            }, animate_out_time);
        })
    };

}(jQuery));

function auto_hide_alerts() {
    jQuery('.alert-auto-hide').not('.alert-important').delay(3000).fadeOut(350);
}

function make_datepicker() {
    jQuery('.datepicker').datepicker({
        dateFormat: "m/d/y",
        onSelect: function (dateText, inst) {
            jQuery(this).focus();
            jQuery(this).focusout();
        }
    });
}

jQuery(document).ready(function () {

    make_datepicker();

    auto_hide_alerts();
    // jQuery('body').on('click', '.fly-out-close', function (e) {
    //     e.preventDefault();
    //     jQuery(this).parents('.right-fly-out').te_animate_out('slideOutRight', 'fast');
    // });
    // jQuery('div.alert').not('.alert-important').delay(3000).fadeOut(350);
});

function bs_alert(type, msg) {

    // jQuery('.capxmsgalert').remove();
    var html = '<div  id="' + (id) + '"class="capxmsgalert bs-alert alert alert-' + type + ' alert-dismissable fade in te_animated2  te_flipOutX2">';
    html += '<button class="close" data-dismiss="alert">×</button>';
    html += '<span>';
    if (typeof msg === 'object') {
        jQuery.each(msg, function (i, e) {
            html += e + "<br>";
        });
    } else
        html += msg;
    html += '</span>';
    html += '</div>';
    //html += '<script>setTimeout( function(){ jQuery("#' + id + '").remove();} , 5000 );</script>';
    id++;
    return html;
}

function te_alert_html(msg, type) {

    // jQuery('.capxmsgalert').remove();
    var _html = '<div  id="te-alert-' + (te_alert_id) + '"class="capxmsgalert te-alert-msg bs-alert alert alert-' + type + ' alert-dismissable fade in te_animated2  te_flipOutX2">';
    _html += '<button class="close" data-dismiss="alert">×</button>';
    _html += '<span>';
    if (typeof msg === 'object') {
        jQuery.each(msg, function (i, e) {
            _html += e + "<br>";
        });
    } else
        _html += msg;
    _html += '</span>';
    _html += '</div>';
    var te_alert = {html: _html, id: te_alert_id};
    te_alert_id++;
    return te_alert;
}

function te_session_expired_alert(response) {
    swal({
        title: "Session Expired",
        text: "Your session is expired, Please login again!",
        type: "info",
        customClass: 'custom_alert_popup',
        html: true,
        confirmButtonColor: "#18A689",
        confirmButtonText: "OK, Close it",
    }, function () {
        window.location.reload();
    });
}

function show_ams_modal(title, body) {

    jQuery('#ams-modal').find('.modal-title').html(title);
    jQuery('#ams-modal').find('.modal-body').html(body);
    jQuery('#ams-modal').modal('show');

}

(function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function (i, el) {
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('input change', function (e) {
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type == 'keyup' && e.keyCode != 8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);

function te_user_role(role_id) {
    var $roles = {};
    $roles[1] = 'Admin';
    $roles[2] = 'CPCS Analyst';
    $roles[3] = 'CPCS Client - Full Access';
    $roles[4] = 'CPCS Client - View Only';
    $roles[5] = 'External';
    return $roles[role_id] ? $roles[role_id] : '';
}


(function ($) {
    if ($.fn.style) {
        return;
    }

    // Escape regex chars with \
    var escape = function (text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    // For those who need them (< IE 9), add support for CSS functions
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
                    '(\\s*;)?', 'gmi');
                this.cssText =
                    this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        };
        CSSStyleDeclaration.prototype.removeProperty = function (a) {
            return this.removeAttribute(a);
        };
        CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
            var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
                'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }

    // The style function
    $.fn.style = function (styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };
})(jQuery);


function js_current_timestamp() {

    var timeStampInMs = window.performance && window.performance.now && window.performance.timing && window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
    return timeStampInMs;

}

jQuery(document).ready(function () {
    jQuery('body').on('click', '.te-breadcrumb-link .delete-breadcrumb', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var href = jQuery(this).data('href');

        jQuery(this).parent().parent().te_animate_out('zoomOut', 'fast', function () {
        });

        window.location.href = href;
    });
});

function te_check_numeric(num) {
    return !isNaN(num) && num.toString().indexOf('+') == -1;
}

function te_check_positive_numeric(num) {
    var ok = false;
    if (te_check_numeric(num)) {
        if (num >= 0) {
            ok = true;
        }
    }
    return ok;
}

function te_check_nonzero_positive_numeric(num) {
    var ok = false;
    if (te_check_numeric(num)) {
        if (num > 0) {
            ok = true;
        }
    }
    return ok;
}

function te_check_integer(num) {
    return !isNaN(num) && num.toString().indexOf('.') == -1 && num.toString().indexOf('+') == -1;
}

function te_check_positive_integer(num) {
    var ok = false;
    if (te_check_integer(num)) {
        if (num >= 0) {
            ok = true;
        }
    }
    return ok;
}

function te_check_nonzero_positive_integer(num) {
    var ok = false;
    if (te_check_integer(num)) {
        if (num > 0) {
            ok = true;
        }
    }
    return ok;
}

function te_check_float(num) {
    return (!isNaN(num) && num.toString().indexOf('.') != -1 && num.toString().indexOf('+') == -1);
}

function te_check_positive_float(num) {
    var ok = false;
    if (te_check_float(num)) {
        if (num >= 0) {
            ok = true;
        }
    }
    return ok;
}

function te_check_nonzero_positive_float(num) {
    var ok = false;
    if (te_check_float(num)) {
        if (num > 0) {
            ok = true;
        }
    }
    return ok;
}

