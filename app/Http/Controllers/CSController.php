<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use App\Message;
use App\Modules\Traits\HandleCrud;
use App\MyList;
use App\Recipient;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CSController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new Recipient;
        $this->db_table = $this->model->getTable();
        $this->run_main_table_query = false;

        if (!$request->isMethod('get')) {
            $this->run_main_table_query = true;
        }
    }

    public function tableColumns()
    {

        $columns = ['id', 'email',
            'phone', 'active'
        ];
        $show = true;
        $tableCols = $this->setColumns(true, $columns);
        $tableCols['action'] = ['sorting' => false];
//        $tableCols['this.list_id'] = ['label' => "List", 'sorting' => true];

        return $tableCols;
    }

    public function setColumns($show = false, $toShow)
    {
        $columns = $this->model->getTableColumns();
        if ($show == true) {
            $columns = $toShow;
        }
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        if ($show == false) {
            foreach ($toShow as $col) {
                unset($tableCols["this." . $col]);
            }
        }
        return $tableCols;
    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        $search_list = trim($request->get('search_list', ''));
        $viewData['search_list'] = $search_list;

        $search_keyword = trim($request->get('search_keyword', ''));
        //don't show any list if case of page load and empty search list
        if (empty(trim($search_keyword))) {
            $viewData['items'] = new Paginator([], $per_page = 10, $page_no = 1);
        }
        return $viewData;
    }

    public function applyGroupBy(Request $request, $eloquentObj)
    {
        return $eloquentObj->groupBy('phone');
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->orWhere('email', 'LIKE', $search_keyword);
                $q = $q->orWhere('phone', 'LIKE', $search_keyword);
            });
        }
        return $eloquentObj;
    }

    public function bulkDelete(Request $request)
    {
        $itemIds = Input::all();
        $response = [];
        if (isset($itemIds['select']) && count($itemIds['select']) > 0) {

            foreach ($itemIds['select'] as $itemId) {

                $oneItemObj = $this->model->where('id', $itemId)->first();
                if ($oneItemObj) {
                    $this->model->where('phone', $oneItemObj->phone)->delete();
                }
            }

            $response['message'] = 'Items deleted successfully';
        } else {
            $response['message'] = 'Please select the item(s) to delete';
        }

        $response = $this->getItemsData($request, true);

        return $response;
    }

    public function destroy(Request $request)
    {
        $response = [];
        $response['status'] = 'fail';

        $item_id = Input::get('id');
        $itemEloquent = clone($this->model);
        $oneItemObj = $itemEloquent->where('id', $item_id)->first();
        if ($oneItemObj) {
            $this->model->where('phone', $oneItemObj->phone)->delete();
        }
        $response['message'] = 'Item deleted successfully';
        $response = $this->getItemsData($request, true);

        return $response;
    }

    public function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'unsubscribe':
                return $this->unsubscribe($request);
                break;
        }
    }

    public function unsubscribe(Request $request)
    {
        $id = $request->get('item-id');
        $status = '';
        $itemEloquent = clone($this->model);
        $searchIdObject = $itemEloquent->where('id', $id)->first();
        if ($searchIdObject) {
            $allRecipients = $this->model->where('phone', $searchIdObject->phone)->get();
            foreach ($allRecipients as $oneRecipient) {
                if ($oneRecipient->active != 0) {
                    $oneRecipient->active = 0;
                    $oneRecipient->unsubscribed_date = Carbon::now()->toDateTimeString();
                }
                $oneRecipient->save();
            }
        }

        $response = $this->getItemsData($request);
        $response['state'] = $status;
        $response['message'] = 'Item updated successfully';

        return $response;
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }
}
