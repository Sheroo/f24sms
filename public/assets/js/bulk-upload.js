var myDropzone = '';

function show_bulk_upload_first_step_modal(entity, hidden_data) {

    reset_bulk_upload_modal();
    var bulkUploadModal = jQuery('#ams-bulk-upload-modal');

    bulkUploadModal.find('form.dropzone').attr('data-current_slug', hidden_data['current_slug']);

    bulkUploadModal.find('.modal-body #fileForm a').remove();
    bulkUploadModal.find('.modal-dialog').removeClass('wide_model');
    bulkUploadModal.find('.uploadFile').hide();
    bulkUploadModal.find('.selectFile').show().prop('disabled', false);
    if (myDropzone !== '') {
        myDropzone.removeAllFiles(true);
    }

    for (var i in hidden_data) {
        bulkUploadModal.find('form.dropzone').append('<input type="hidden" class="bulk_upload_hidden_data" name="bulk_upload_hidden_data[' + i + ']" value="' + hidden_data[i] + '">');
    }
    bulkUploadModal.find('#summeryDiv').html('');
    bulkUploadModal.find('.modal-body #fileForm').prepend('<a class="download-sample-file-btn" href="' + f24.base_url + "/assets/" + entity + "-bulk-upload-sample-file.csv" + '">Download Sample CSV</a>');
    bulkUploadModal.modal('show');

}

function show_bulk_upload_modal(body) {

    var bulkUploadModal = jQuery('#ams-bulk-upload-modal');
    bulkUploadModal.find('.modal-body #summeryDiv').html(body);
    bulkUploadModal.modal('show');

}

function reset_bulk_upload_modal() {

    var bulkUploadModal = jQuery('#ams-bulk-upload-modal');

    bulkUploadModal.find('.modal-footer .download_error_file').hide();
    bulkUploadModal.find('#fileForm').show();
    bulkUploadModal.find('.customHTML').show();
    bulkUploadModal.find('#summeryDiv').hide();
    bulkUploadModal.find('.send-to-api').hide();
    bulkUploadModal.find('.bulk_upload_done').hide();
    bulkUploadModal.find('.selectFile').show();
    bulkUploadModal.find('.modal-footer p').remove();
    bulkUploadModal.find('.modal-dialog').css('width', '673px');
    bulkUploadModal.find('.modal-body p').remove();
    bulkUploadModal.find('.modal-dialog').removeClass('wide_modal').removeClass('narrow_modal');

}

jQuery(document).ready(function () {

    jQuery('body').on('click', '.show-bulk-upload-modal', function (e) {


        var bulk_upload_entity = jQuery(this).attr('data-bulk_upload_entity');

        var hidden_data = jQuery(this).data();

        show_bulk_upload_first_step_modal(bulk_upload_entity, hidden_data);

    });

    jQuery('body').on('click', '#ams-bulk-upload-modal .selectFile', function (e) {
        jQuery(".dropzone").trigger("click");
    });

    jQuery('body').on('click', '#ams-bulk-upload-modal .bulkUploadFirstStepFileButton', function (e) {
        jQuery("#file-btn").click();

    });

    // Upload data to api
    jQuery(document).on('click', '#ams-bulk-upload-modal .send-to-api', function (e) {
        e.preventDefault();

        var page = jQuery(this).data('page');

        var bulkUploadModal = jQuery('#ams-bulk-upload-modal');

        var temp_table_name = jQuery('#ams-bulk-upload-modal #bulk_upload_temp_table_name').val();
        var serialized_data = bulkUploadModal.find('form.dropzone .bulk_upload_hidden_data').serialize();
        serialized_data += '&temp_table_name=' + temp_table_name;

        bulkUploadModal.find('a.download_error_file').attr('data-summary-file', 'api-response');
        var target_url = prepareAjaxAction(bulkUploadModal.find('form.dropzone').attr('data-current_slug'));

        serialized_data = addAjaxActionToSerializedData(serialized_data, 'ajax_upload_entity_data');
        te_ajax(target_url, serialized_data,
            function (resp) {
                bulkUploadModal.find('.bulk_upload_done').show();
                bulkUploadModal.find('.modal-dialog').removeClass('wide_modal').addClass('narrow_modal');
                bulkUploadModal.find('.send-to-api').hide();

                show_bulk_upload_modal(resp.previewdata);
                if (resp.afterApiCall == 1) {
                    jQuery('#ams-bulk-upload-modal .modal-body a.download_error_file').parent().after('<p style="float: left;">Download skipped entities summary <a href="javascript:void(0)" data-summary-file="download_skipped_error_file" class="download_error_file">here</a>.</p>');
                }

                refresh_items_data(current_slug);

                enable_all_js_plugins();
            },
            function (resp) {
                enable_all_js_plugins();
            },
            function (error_resp) {
            },
            bulkUploadModal.find('.modal-dialog')
        );
    });

    /*
     * Download CSV data validation errors to csv file
     */
    jQuery(document).on('click', '#ams-bulk-upload-modal .download_error_file', function (e) {
        e.preventDefault();
        var bulkUploadModal = jQuery(this).parents('#ams-bulk-upload-modal')
        var current_slug = bulkUploadModal.find('form.dropzone').attr('data-current_slug');
        var serialized_data = bulkUploadModal.find('form.dropzone .bulk_upload_hidden_data').serialize();

        var temp_table_name = jQuery('#ams-bulk-upload-modal #bulk_upload_temp_table_name').val();

        if (jQuery(this).attr('data-summary-file') == 'api-response') {
            serialized_data += "&temp_table_name=" + temp_table_name + "&afterApi=Yes";
        } else if (jQuery(this).attr('data-summary-file') == 'data-validation') {
            serialized_data += "&temp_table_name=" + temp_table_name + "&afterApi=No";
        } else if (jQuery(this).attr('data-summary-file') == 'download_skipped_error_file') {
            serialized_data += "&temp_table_name=" + temp_table_name + "&afterApi=skipped";
        }

        var target_url = prepareAjaxAction(current_slug);
        serialized_data = addAjaxActionToSerializedData(serialized_data, 'ajax_download_bulkupload_summary_csv');
        te_ajax(target_url, serialized_data,
            function (resp) {
                if (resp.path != '') {
                    window.location.href = resp.path;
                }
            },
            function (resp) {
            },
            function (error_resp) {
            },
            jQuery('#ams-bulk-upload-modal .modal-dialog .modal-content')
        );
    });
});

function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

Dropzone.options.myAwesomeDropzone = {

    autoProcessQueue: false,
    uploadMultiple: false,
    parallelUploads: 1,
    maxFiles: 1,
    acceptedFiles: ".csv",

    // Dropzone settings
    init: function () {
        myDropzone = this;

        jQuery('body').on('click', '.uploadFile', function (e) {
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        });

        myDropzone.on("addedfile", function (file) {
            var bulkUploadModal = jQuery('#ams-bulk-upload-modal');
            bulkUploadModal.find('.selectFile').prop('disabled', false).hide();
            bulkUploadModal.find('.uploadFile').show();
        });
        myDropzone.on("sending", function () {
            jQuery('#ams-bulk-upload-modal .dz-filename').append('<span class="uploadingText">Uploading</span>');
            jQuery('#ams-bulk-upload-modal .dz-details').css('margin', '0');
            jQuery('#ams-bulk-upload-modal .selectFile').prop('disabled', true);
        });
        myDropzone.on("successmultiple", function (files, response) {
            console.log(response);
        });
        myDropzone.on("success", function (file, response) {

            jQuery('#ams-bulk-upload-modal .selectFile').prop('disabled', false);
            jQuery('#ams-bulk-upload-modal .dz-details').css('margin-bottom', '10px');
            jQuery('#ams-bulk-upload-modal .modal-dialog').removeClass('narrow_modal').addClass('wide_model');
            jQuery('#ams-bulk-upload-modal #csv_file_path').val(response.csv_file_path);
            jQuery('#ams-bulk-upload-modal #bulk_upload_temp_table_name').val(response.temp_table_name);

            jQuery('#ams-bulk-upload-modal .send-to-api').show();
            jQuery('#ams-bulk-upload-modal .uploadFile').hide();
            jQuery('#ams-bulk-upload-modal #summeryDiv').show();
            jQuery('#ams-bulk-upload-modal #fileForm').hide();
            show_bulk_upload_modal(response.previewdata);

            if (response.csv_is_valid == true) {
                jQuery('.send-to-api').show();
                jQuery('#ams-bulk-upload-modal .modal-body').append('<p style="">Download full upload summary <a href="javascript:void(0)" data-summary-file="data-validation" class="download_error_file">here</a>.</p>');
                jQuery('#bulk_upload_account_ids').val(response.account_ids);
            } else {
                jQuery('#ams-bulk-upload-modal .send-to-api').hide();

            }
            jQuery('#ams-bulk-upload-modal #summeryDiv ul.customtab li:first-child').trigger('click');
        });
        myDropzone.on("error", function (files, response) {

            console.log(response, 'response of bulk upload');
            console.log(files);
            jQuery('#ams-bulk-upload-modal').modal('hide');
            // if (files.status == 'error') {
            var respText = response;
            var statusText = false;
            show_error_modal_in_ajax_callback('bulk_upload', respText, statusText);
            // }
        });
        myDropzone.on("complete", function (file, response) {
            myDropzone.removeFile(file);
        });
    }

};
