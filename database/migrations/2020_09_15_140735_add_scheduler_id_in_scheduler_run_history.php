<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchedulerIdInSchedulerRunHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scheduler_delivery_report', function (Blueprint $table) {
            $table->bigInteger('scheduler_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduler_delivery_report', function (Blueprint $table) {
            $table->dropColumn('scheduler_id');
        });
    }
}
