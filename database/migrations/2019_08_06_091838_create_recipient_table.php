<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipients', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('country_code', 4)->nullable();
            $table->string('phone', 32)->nullable();
            $table->unsignedBigInteger('list_id')->nullable();
            $table->unsignedTinyInteger('active')->default(1);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('list_id')->references('id')->on('lists');
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamps();

            $table->index(['email']);
            $table->index(['phone']);
            $table->index(['list_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipients');
    }
}
