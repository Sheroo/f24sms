<form method="post"
      id="{{$controller_slug}}-search-form"
      data-slug="{{$controller_slug}}"
      class="te-search-form {{$controller_slug}}-search-form clearfix">
    <input type="submit" style="display: none;">
    <div class="col-md-7" style="padding-left:0;">

        <button type="button"
                class="btn btn-primary te-item-action-btn"
                data-action="create"
                data-slug="{{$controller_slug}}"
        ><i class="fa fa-plus"></i> &nbsp;Create Automation
        </button>
    </div>
    <div class="col-md-5 pull-right" style="padding-right:0;">
        <?php
        $search_keyword = empty($search_keyword) ? '' : $search_keyword;
        ?>
        <div class="search-items-input-wrapper pull-right"
             style="max-width:70%;width:200px;">
            <input value="{{$search_keyword}}"
                   data-slug="{{$controller_slug}}"
                   placeholder="Search..."
                   class="te-search-input"
                   name="search_keyword" id="search_keyword"
            />
        </div>
    </div>

    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="order_by"
           id="order_by"
           class="order_by"
           value="{{(isset($order_by)) ? $order_by : ''}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="order"
           id="order"
           class="order"
           value="{{(isset($order_by) && $order == 'desc') ? 'desc' : 'asc'}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="per_page"
           id="per_page"
           class="per_page"
           value="{{$per_page}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="page_no"
           id="page_no"
           class="page_no"
           value="{{$items->currentPage()}}"/>
</form>
<div class="panel">
    <div class="panel-content">
{{--        {{dd($items)}}--}}
        @if(count($items) >= 0)

            <form method="post"
                  data-slug="{{$controller_slug}}"
                  class="te-table-form {{$controller_slug}}-table-form"
                  id="{{$controller_slug}}-table-form"
            >

                <table data-slug="{{$controller_slug}}"
                       class="table table-striped {{$controller_slug}}-table te-item-table"
                       id="{{$controller_slug}}-table">
                    <thead>
                    <tr>
                        @foreach($tableColumns as $colKey => $col)
                            <th class="{{te_table_sorting_class($colKey,$col,$order,$order_by)}}"
                                data-order_by="{{$colKey}}"
                                @if( $colKey == 'action') style="width: 135px;" @endif
                            >
                                {{isset($col['label']) ? $col['label'] : te_wordify(remove_alias($colKey))}}
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            @foreach($tableColumns as $colKey => $col)
                                <?php
                                $colKey = remove_alias($colKey);?>
                                <td>
                                    @if($colKey == 'action')
                                    <div style="display:flex;float: right;">
                                        @if ($item->scheduled_type == 'list')
                                        <button class="te-item-action-btn start run btn-sm btn-warning"
                                                data-action="run"
                                                data-id="{{$item->id}}"
                                                data-slug="{{$controller_slug}}"><i
                                                class="fa fa-play-circle-o"></i></button>
                                        @endif
                                        <button
                                            class="te-item-action-btn edit btn btn-sm btn-default"
                                            data-action="edit"
                                            data-id="{{$item->id}}"
                                            data-slug="{{$controller_slug}}"><i
                                                class="fa fa-edit"></i></button>

                                        <button class="te-item-action-btn edit btn btn-sm btn-danger"
                                                data-action="delete"
                                                data-id="{{$item->id}}"
                                                data-slug="{{$controller_slug}}"><i
                                                class="fa fa-trash-o"></i></button>
                                    </div>

                                    @elseif($colKey == 'status')
                                        {{$item->$colKey==1 ? 'Enabled' : "Paused"}}
                                    @elseif($colKey == 'schedule_interval')
                                        @if($item->scheduled_type == 'list')
                                        {{automation_rule_schedule_html($item)}}
                                        @elseif(@$item->scheduled_type == 'segment')
                                            Run After {{@$item->segment_scheduler_hours}} Hour(s)
                                        @endif
                                    @elseif($colKey == 'scheduled_date' || $colKey == 'start_date' || $colKey == 'end_date')
                                        @if( !is_null($item->$colKey))
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d',$item->$colKey)->toDateString()}}
                                        @endif
                                    @elseif($colKey == 'created_at')
                                        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->$colKey)->toDateString()}}
                                    @elseif($colKey == 'created_at')
                                        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->$colKey)->toDateString()}}
                                    @elseif($colKey == 'gateway')
                                        @if($item->type == 'sms')
                                            {{$item->gateway}}
                                        @else
                                            {{$item->email_service}}
                                        @endif
                                    @elseif($colKey == 'segment')
                                        {{@$item->clicked == 1?"+clicked":"" }}
                                        {{@$item->optin == 1?"+optin":"" }}{{@$item->optin == -1?"-optin":"" }}
                                        {{@$item->abandoned == 1?"+abandoned":"" }}{{@$item->abandoned == -1?"-abandoned":"" }}
                                        {{@$item->memberp1 == 1?"+memberp1":"" }}{{@$item->memberp1 == -1?"-memberp1":"" }}
                                        {{@$item->memberp2 == 1?"+memberp2":"" }}{{@$item->memberp2 == -1?"-memberp2":"" }}
                                    @elseif($colKey == 'country')

                                    <?php
                                        $countries = ['47'=>'Norway',  '46' => 'Sweden','358' => 'finland'];
                                        $country = '';
                                        if($item->scheduled_type == 'segment'){
                                            $country = isset($countries[@$item->country])?$countries[@$item->country]:'All';
                                        }
                                    ?>
                                       {{@$country}}
                                    @else
                                        {{$item->$colKey}}
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </form>
        @endif
    </div>
</div>

@include('includes.pagination',['itemdata'=> $items, 'totalRows' => $totalRows])
