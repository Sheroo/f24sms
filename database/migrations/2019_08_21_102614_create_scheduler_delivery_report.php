<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerDeliveryReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler_delivery_report', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('scheduler_history_id');
            $table->unsignedInteger('recipient_id');
            $table->string('twilio_notification_id', 64)->nullable();
            $table->unsignedTinyInteger('notification_received')->default(0);
            $table->timestamp('notification_received_time')->nullable();

            $table->string('sms_sid', 64)->nullable();
            $table->string('message_sid', 64)->nullable();
            $table->string('sms_status', 12)->nullable();
            $table->string('message_status', 12)->nullable();
            $table->unsignedInteger('error_code')->nullable();


            $table->timestamps();

            $table->index(['scheduler_history_id', 'recipient_id'], 'search_index');
            $table->index(['twilio_notification_id'], 'twilio_notification_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler_delivery_reports');
    }
}
