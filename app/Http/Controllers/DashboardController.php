<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use App\Modules\Traits\HandleCrud;
use App\SchedulerRunHistory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new SchedulerRunHistory;
        $this->db_table = $this->model->getTable();
    }


    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();

        $tableCols = [];
        $tableCols['scheduler_name'] = ['sorting' => true, 'label' => 'Automation'];
        $tableCols['gateway'] = ['sorting' => true, 'label' => 'SMS Gateway'];
        $tableCols['this.total_recipients'] = ['sorting' => true, 'label' => 'Total Recipients'];
        $tableCols['total_delivered'] = ['sorting' => true, 'label' => 'Total Delivered'];
        $tableCols['opened'] = ['sorting' => true, 'label' => 'Open Rate'];
        $tableCols['click'] = ['sorting' => true, 'label' => 'Click Rate'];
        $tableCols['bounced'] = ['sorting' => true, 'label' => 'Bounced Rate'];
        //$tableCols['scheduler_name'] = ['sorting' => true, 'label' => 'Automation'];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
//        $tableCols['action'] = ['sorting' => false];

        unset($tableCols['this.scheduler_id']);
        unset($tableCols['this.succeed']);
        unset($tableCols['this.failed']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.id']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('r.name as scheduler_name');
        $cols[] = DB::raw("CASE WHEN r.type = 'sms' THEN r.gateway  ELSE r.email_service END  as gateway");
        $cols[] = DB::raw("(select count(*) from scheduler_delivery_report sdr1 where `sdr1`.`scheduler_history_id` = `this`.`id`  and `sdr1`.`message_status` in ('delivered','open','click'))  as total_delivered");
        $cols[] = DB::raw("CASE WHEN r.type = 'email' THEN ( select count(*) from scheduler_delivery_report sdr3 where `sdr3`.`scheduler_history_id` = `this`.`id`  and ( `sdr3`.`message_status` in ('open','click')) )  ELSE 0 END  as opened");
        $cols[] = DB::raw("CASE WHEN r.type = 'email' THEN ( select count(*) from scheduler_delivery_report sdr2 where `sdr2`.`scheduler_history_id` = `this`.`id`  and  `sdr2`.`message_status` = 'click' ) ELSE 0 END as click");
        $cols[] = DB::raw("CASE WHEN r.type = 'email' THEN ( select count(*) from scheduler_delivery_report sdr4 where `sdr4`.`scheduler_history_id` = `this`.`id`  and ( `sdr4`.`message_status` = 'bounce' ) )  ELSE 0 END  as bounced");
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->join(DB::raw('scheduler r'), function ($join) {
            $join->on('this.scheduler_id', '=', 'r.id');
        });

        return $eloquentObj;
    }

    public function applyGroupBy(Request $request, $eloquentObj)
    {
        $eloquentObj = $eloquentObj->groupBy('this.id');
        return $eloquentObj;
    }

    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));

        //$eloquentObj->whereNotNull('this.id');

        return $eloquentObj;
    }

    public
    function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'custom':
                break;
            default:
                dd($request->all());
                break;
        }
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }

}
