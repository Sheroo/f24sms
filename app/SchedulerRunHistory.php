<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchedulerRunHistory extends Model
{
    protected $table = 'scheduler_run_history';
    protected $fillable = ['scheduler_id','total_recipients','succeed','failed'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

    }

    public function automation()
    {
        return $this->belongsTo('\App\Automation', 'scheduler_id', 'id');
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
