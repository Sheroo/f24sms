<form method="post"
      id="{{$controller_slug}}-search-form"
      data-slug="{{$controller_slug}}"
      class="te-search-form {{$controller_slug}}-search-form clearfix">
    <input type="submit" style="display: none;">
    <div class="col-md-7" style="padding-left:0;">


    </div>
    <div class="col-md-5 pull-right m-b-10" style="padding-right:0;">
        <?php
        $search_keyword = empty($search_keyword) ? '' : $search_keyword;
        ?>
        <div class="search-items-input-wrapper pull-right"
             style="max-width:70%;width:200px;">
            <input value="{{$search_keyword}}"
                   data-slug="{{$controller_slug}}"
                   placeholder="Search..."
                   class="te-search-input"
                   name="search_keyword" id="search_keyword"
            />
        </div>
    </div>

    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="order_by"
           id="order_by"
           class="order_by"
           value="{{(isset($order_by)) ? $order_by : ''}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="order"
           id="order"
           class="order"
           value="{{(isset($order_by) && $order == 'desc') ? 'desc' : 'asc'}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="per_page"
           id="per_page"
           class="per_page"
           value="{{$per_page}}"/>
    <input type="hidden"
           data-slug="{{$controller_slug}}"
           name="page_no"
           id="page_no"
           class="page_no"
           value="{{$items->currentPage()}}"/>
</form>
<div class="panel">
    <div class="panel-content">
        @if(count($items) >= 0)

            <form method="post"
                  data-slug="{{$controller_slug}}"
                  class="te-table-form {{$controller_slug}}-table-form"
                  id="{{$controller_slug}}-table-form"
            >

                <table data-slug="{{$controller_slug}}"
                       class="table table-striped {{$controller_slug}}-table te-item-table"
                       id="{{$controller_slug}}-table">
                    <thead>
                    <tr>
                        @foreach($tableColumns as $colKey => $col)
                            <th class="{{te_table_sorting_class($colKey,$col,$order,$order_by)}}"
                                data-order_by="{{$colKey}}" @if (in_array( $colKey , ['this.total_recipients','total_delivered','opened','click','bounced'])) style="width: 30px;" @endif>
                                {{isset($col['label']) ? $col['label'] : te_wordify(remove_alias($colKey))}}
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            @foreach($tableColumns as $colKey => $col)
                                <?php
                                $colKey = remove_alias($colKey);
                                ?>
                                <td>
                                    @if($colKey == 'action')
                                        <button
                                            class="te-item-action-btn edit btn btn-sm btn-default"
                                            data-action="edit"
                                            data-id="{{$item->id}}"
                                            data-slug="{{$controller_slug}}"><i
                                                class="fa fa-edit"></i></button>

                                        <button class="te-item-action-btn edit btn btn-sm btn-danger"
                                                data-action="delete"
                                                data-id="{{$item->id}}"
                                                data-slug="{{$controller_slug}}"><i
                                                class="fa fa-trash-o"></i></button>
                                    @elseif($colKey == 'gateway')
                                        {{ ucfirst($item->$colKey)}}
                                    @elseif( $colKey=='click' || $colKey=='opened' || $colKey=='bounced' )
                                        <?php
                                        $result = '0.00';
                                        if( $item->total_recipients != 0 ){

                                            $result = number_format((float) ( $item->$colKey* 100 /  $item->total_recipients ), 2, '.', '');
                                        }
                                        ?>
                                        {{$result}}%
                                    @else
                                        {{$item->$colKey}}
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </form>
        @endif
    </div>
</div>

@include('includes.pagination',['itemdata'=> $items, 'totalRows' => $totalRows])
