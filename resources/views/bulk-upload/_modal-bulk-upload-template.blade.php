<div class="modal inmodal" id="ams-bulk-upload-modal" tabindex="-1" role="dialog" data-backdrop="static"
     data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog narrow_modal" style="width:100%; max-width:950px;">

        <input type="hidden" id="bulk_upload_temp_table_name"
               value=""/>

        <div class="modal-content animated-faster zoomIn">

            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">
                    Bulk Upload
                </h4>
            </div>
            <div class="modal-body item-change-table" style="float: left;margin-bottom: 10px;width: 100%;">
                <div id="fileForm" style="text-align:center;">
                    <form id="my-awesome-dropzone" class="dropzone"
                          data-page="{{te_bulk_upload_page_slug($controller_slug)}}"
                          action="{{ te_route(te_bulk_upload_page_slug($controller_slug))}}"
                          style="margin-top:5px;">
                        <input type="hidden" name="ajax_action" value="ajax_upload_entities">
                        <div class="customHTML">
                            <i class="icon-cloud-upload"
                               style="font-size: 30px;margin: 0 5px 0 0;position: relative;top: 7px;"></i>
                            <span style="font-size: 12px;color: black;">Drag & Drop</span>
                            <span>your file here, or <u style="color: #337ab7">browse</u></span>
                        </div>
                        <div class="dropzone-preview"></div>
                    </form>
                </div>
                <div id="summeryDiv">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-page="{{te_bulk_upload_page_slug($controller_slug)}}"
                        class="btn btn-primary pull-right waves-effect selectFile">Select File
                </button>
                <button type="button" data-page="{{te_bulk_upload_page_slug($controller_slug)}}"
                        class="btn btn-primary pull-right waves-effect uploadFile" style="display: none">
                    Process File
                </button>
                <button type="button" data-page="{{te_bulk_upload_page_slug($controller_slug)}}"
                        class="pull-left model_close_button waves-effect btn btn-default" data-dismiss="modal">Close
                </button>
                <button class="btn btn-primary waves-effect send-to-api"
                        data-page="{{te_bulk_upload_page_slug($controller_slug)}}">Upload
                </button>
                <button class="btn btn-primary waves-effect bulk_upload_done"
                        data-page="{{te_bulk_upload_page_slug($controller_slug)}}" data-dismiss="modal"
                        style="display: none;">
                    Done
                </button>
            </div>

        </div>
    </div>
</div>

