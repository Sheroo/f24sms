<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'mailchimp/unsub/email/webhook/*',
        'twilioipn/*',
        'linkmobilityipn',
        'unsub/email/*',
        'reconsent/email/*',
        'test-sendgrid-event-callback',
        'sendgrid/webhook',
        'mailgun/webhook',
        'linkmobility/stopmessage',
    ];
}
