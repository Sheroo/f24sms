<div class="item-form-section list_message-section"
     data-slug="{{$controller_slug}}"
     data-section="list_message">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            List & Message
        </h3>
    </div>
    <div class="item-form-section-body clearfix">
        <?php
        $lists = isset($lists) ? $lists : [];
        $messages = isset($messages) ? $messages : [];
        $emails = isset($emails) ? $emails : [];
        ?>
        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">List</label>
            <div class="col-sm-9">
                <select name="list_id" id="list_id" class="select-picker form-control form-white te-input te-required">
                    <option value="">Select List</option>
                    @foreach($lists as $list)
                        <option
                            {{@$itemObj->list_id==$list->id ? 'selected="selected"':''}} value="{{$list->id}}">{{$list->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group clearfix sms_msg">
            <label class="col-sm-3 control-label">Message</label>
            <div class="col-sm-9">
                <select name="message_id" id="message_id"
                        class="select-picker form-control form-white te-input te-required">
                    <option value="">Select Message</option>
                    @foreach($messages as $message)
                        <option
                            {{@$itemObj->message_id==$message->id ? 'selected="selected"':''}} value="{{$message->id}}">{{$message->subject}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group clearfix email_msg" style="display: none">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <select name="email_id" id="email_id"
                        class="select-picker form-control form-white te-input te-required">
                    <option value="">Select Email</option>
                    @foreach($emails as $email)
                        <option
                            {{@$itemObj->message_id==$email->id ? 'selected="selected"':''}} value="{{$email->id}}">{{$email->subject}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
</div>
