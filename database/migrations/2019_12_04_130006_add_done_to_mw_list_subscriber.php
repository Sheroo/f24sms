<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDoneToMwListSubscriber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("mailwizz")->table('mw_list_subscriber', function (Blueprint $table) {
            $table->unsignedTinyInteger('sync_with_f24sms')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("mailwizz")->table('mw_list_subscriber', function (Blueprint $table) {
            $table->dropColumn(['sync_with_f24sms']);
            //
        });
    }
}
