<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDifferentFieldsToRecipients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipients', function (Blueprint $table) {
//            $table->string('');

$table->string('emailInsert')->nullable();
$table->string('mobileInsert')->nullable();

$table->string('country')->nullable();
$table->string('domain')->nullable();
$table->string('age')->nullable();

$table->string('maritalStatus')->nullable();
$table->string('housing')->nullable();
$table->string('employment')->nullable();
$table->string('income')->nullable();
$table->string('payrem')->nullable();
$table->unsignedTinyInteger('approved')->nullable();
$table->string('retry')->nullable();
$table->string('rejected')->nullable();
$table->dateTime('optinDate')->nullable();
$table->dateTime('optinSMSDate')->nullable();
$table->dateTime('optinEmailDate')->nullable();
$table->dateTime('optoutDate')->nullable();
$table->dateTime('optoutSMSDate')->nullable();
$table->dateTime('optoutEmailDate')->nullable();
$table->string('emailBounced')->nullable();
$table->string('complained')->nullable();
$table->string('source')->nullable();
$table->string('utm_term')->nullable();
$table->string('score')->nullable();
$table->string('date')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipients', function (Blueprint $table) {
            //
        });
    }
}
