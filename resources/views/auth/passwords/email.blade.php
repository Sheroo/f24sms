@extends('layouts.visitor')

@section('content')

    <!-- BEGIN LOGIN BOX -->

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="logo-img" src="{{te_asset('images/f24-logo-150x150.png')}}">
                <form method="POST" action="{{ te_make_url_scheme_less(route('password.email')) }}">
                    @csrf

                    <div class="form-group row">

                        <div class="col-md-12">
                            <input id="email" placeholder="Email" type="email"
                                   class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </div>
                    <a class="btn btn-link" href="{{ te_route('login') }}">
                        {{ __('Remember Password? Login') }}
                    </a>
                </form>
            </div>
        </div>
    </div>

@endsection
