<?php

use App\Jobs\ProcessSegmentAutomationRuleJob;
use App\Recipient;
use App\SchedulerDeliveryReport;
use App\SchedulerRunHistory;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use SendGrid\Mail\Bcc;
use SendGrid\Mail\Content;
use SendGrid\Mail\From;
use SendGrid\Mail\Personalization;
use SendGrid\Mail\To;


use Mailgun\Mailgun;
use Mailgun\Hydrator\ArrayHydrator;
use Mailgun\HttpClient\HttpClientConfigurator;


define('ROLE_ADMIN', 'admin');
define('ROLE_CS', 'cs');

define('ENTITY_RECIPIENT', 'recipient');

define('Q_RULE_SYNC', 'f24-rule-sync');
define('Q_SEGMENT_RULE_SYNC', 'f24-segment-rule-sync');

define('TAG_CLICKED',2);
define('TAG_OPTIN',3);
define('TAG_ABONDONED',4);
define('TAG_MEMBER_P1',5);
define('TAG_MEMBER_P2',6);

if (!function_exists('asset_url')) {

    function asset_url()
    {
        return asset('/') . 'assets/';
    }
}

function te_url($slug = '', $tool_prefix = '')
{
    $slug = $slug == '/' ? '' : $slug;
    $slug = ltrim($slug, '/');

    return url($tool_prefix . '/' . $slug);
}


function te_schema_less_base_url($slash = true)
{
    $base_url = te_url();
    $base_url = te_make_url_scheme_less($base_url);
    $base_url = rtrim($base_url, '/');
    if ($slash) {
        $base_url = $base_url . '/';
    }

    return $base_url;
}

function te_schema_less_asset_url($slash = true)
{
    $asset_url = url('assets');

    $asset_url = te_make_url_scheme_less(rtrim($asset_url, '/'));
    if ($slash) {
        $asset_url = $asset_url . '/';
    }

    return $asset_url;
}

function te_asset($filePath)
{
    $filePath = ltrim($filePath, '/');
    return te_schema_less_asset_url() . $filePath;
}

function te_route($route)
{
    $route = ltrim($route, '/');
    return te_schema_less_base_url() . $route;
}

function te_make_url_scheme_less($url)
{

    $protocol = strtolower(substr($url, 0, 6));
    if ($protocol == 'https:') {
        $url = substr($url, 6);
    }
    $protocol = strtolower(substr($url, 0, 5));
    if ($protocol == 'http:') {
        $url = substr($url, 5);
    }

    return $url;

}

function style($path, $js_version = '')
{
    $js_version = empty($js_version) ? \Illuminate\Support\Facades\Cache::get('js_version_number') : $js_version;
    if (is_array($path)) {
        foreach ($path as $p) {
            echo '<link rel="stylesheet" href="' . te_asset($p) . '?v=' . $js_version . '"  rel="stylesheet" />
            ';
        }
    } else {
        echo '<link rel="stylesheet" href="' . te_asset($path) . '?v=' . $js_version . '" rel="stylesheet" />
        ';
    }
}

function script($path, $js_version = '')
{
    $js_version = empty($js_version) ? \Illuminate\Support\Facades\Cache::get('js_version_number') : $js_version;
    if (is_array($path)) {
        foreach ($path as $p) {
            echo '<script src="' . te_asset($p) . '?v=' . $js_version . '"></script>
            ';
        }
    } else {
        echo '<script src="' . te_asset($path) . '?v=' . $js_version . '"></script>';
    }
}

function page_script($path, $js_version = '')
{
    $js_version = empty($js_version) ? \Illuminate\Support\Facades\Cache::get('js_version_number') : $js_version;

    if (is_array($path)) {
        foreach ($path as $p) {
            $p = ltrim($p, '/');
            echo '<script src="' . te_asset('js/page-scripts/' . $p . '.js') . '?v=' . $js_version . '"></script>
            ';
        }
    } else {
        $path = ltrim($path, '/');
        echo '<script src="' . te_asset('js/page-scripts/' . $path . '.js') . '?v=' . $js_version . '"></script>
        ';
    }
}

if (!function_exists('is_production_environment')) {
    function is_production_environment()
    {
        return app()->environment('production');
    }
}

if (!function_exists('is_testing_environment')) {
    function is_testing_environment()
    {
        return app()->environment('testing');
    }
}


if (!function_exists('is_staging_environment')) {
    function is_staging_environment()
    {
        return app()->environment('staging');
    }
}

if (!function_exists('is_local_environment')) {
    function is_local_environment()
    {
        return app()->environment('local');
    }
}

function te_segment($index = '')
{
    $segments = [
        'current_slug' => 1,
    ];
    return empty($index) ? $segments : (  isset($segments[$index]) ? $segments[$index] : '' ) ;
}

if (!function_exists('sms_scheduler_rules')) {
    function sms_scheduler_rules($user_id = false, $day = '', $hour = '', $print_query = false , $segment = false )
    {
        $query = sms_scheduler_rules_query($user_id, $day, $hour , $segment );
        Log::info($query);
        if ($print_query) {
            echo '<code>' . $query . '</code><hr/><br/>';
        }
        return collect(DB::select(DB::raw($query)))->pluck('id')->toArray();
    }
}

if (!function_exists('sms_scheduler_rules_query')) {
    function sms_scheduler_rules_query($user_id = false, $day = '', $hour = '' , $segment = false )
    {
        // getting Server time and formatting it.
        $current_time = Carbon::now();

        $temp = $current_time->format('D');
        $current_day = $day == '' ? strtolower($temp) : strtolower($day);
        $current_date_of_month = $current_time->day;
        $current_hour = $hour == '' ? $current_time->hour : $hour;
        //query to get the rules whose time has come to unleash their power through CRON...
        $query = "SELECT id FROM scheduler
        where  status='1' AND ((schedule_interval='daily' AND scheduled_hour = " . $current_hour . "  )

        OR (schedule_interval='weekly' AND scheduled_day LIKE '%" . ucfirst($current_day) . "%'  AND scheduled_hour = " . $current_hour . " )
        OR (schedule_interval='bi-weekly' AND MOD(WEEKOFYEAR(created_at)-WEEKOFYEAR('" . $current_time->toDateTimeString() . "'),2)=0 AND scheduled_day LIKE '%" . ucfirst($current_day) . "%'  AND scheduled_hour = " . $current_hour . " )
        OR (schedule_interval='monthly' AND scheduled_date = " . $current_date_of_month . " AND scheduled_hour = " . $current_hour . "))

        AND (start_date IS NULL  OR start_date <= '" . $current_time->toDateTimeString() . "' ) AND (end_date >= '" . $current_time->toDateTimeString() . "' OR end_date IS NULL OR end_date = ''  )
        AND deleted_at IS NULL
        " . (($user_id) ? " AND user_id={$user_id}" : "");   /*this line is for getting rules of a specific user*/

        if ($segment){
            $query = "SELECT id FROM scheduler
                    where  status='1' AND scheduled_type = 'segment' AND deleted_at IS NULL";

        }

        return $query;
    }
}

if (!function_exists('replace_hyphens_in_text')) {
    function replace_hyphens_in_text($string)
    {
        $html = str_replace("-", " ", ucfirst($string));

        return $html;
    }
}

if (!function_exists('notifyBugsnagError')) {
    function notifyBugsnagError($exception, $errorDetails = [], $type = 'error')
    {

        if (!($exception instanceof \Exception)) {
            $exception = new \Exception($exception);
        }

        $env = $app_env = env('APP_ENV', 'local');
        $errorDetails['Environment'] = $env;
        $type = $type == 'info' ? 'info' : 'error';

        \Bugsnag\BugsnagLaravel\Facades\Bugsnag::notifyException($exception, function ($report) use ($errorDetails, $type) {
            $report->setSeverity($type);
            $report->setMetaData([
                'AMS Error Detail' => $errorDetails
            ]);
            $stacktrace = $report->getStacktrace();
            $frames = $stacktrace->getFrames();
            if (@($frames[0]['method']) == 'notifyBugsnagError') {
                $stacktrace->removeFrame(0);
            }
            if (@($frames[0]['method']) == 'App\Capx\Database\MySqlConnection::__callParentMethod') {
                $stacktrace->removeFrame(0);
                $methods = ['statement', 'delete', 'insert', 'select', 'selectOne', 'update'];
                foreach ($methods as $method) {
                    if (@($frames[1]['method']) == 'App\Capx\Database\MySqlConnection::' . $method) {
                        $stacktrace->removeFrame(1);
                    }
                }
            }
        });

    }
}

if (!function_exists('mysql_escape')) {
    function mysql_escape($inp)
    {
        if (is_array($inp)) return array_map(__METHOD__, $inp);

        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

        return $inp;
    }
}

if (!function_exists('db_esc_like_raw')) {
    /**
     * @param string $str
     * @return string
     */
    function db_esc_like_raw($str)
    {
        return mysql_escape(str_replace(["\\", '%', '_'], ["\\\\", '\%', '\_'], $str));
    }

}

/*
 * to make the search compatible we need to  strtolower($value) because it is case sensitive
 * MYSQL search is case insensitive
 * queryType can be athena
 */
function escape_search_query_value($operator, $value, $queryType = 'sql')
{
    if ($operator == 'is-exactly' || $operator == 'is-in-exact' || $operator == 'is-not-in-exact') {
        if ($queryType == 'athena') {
            $value = DB::raw(athena_escape(strtolower($value)));
        } else {
            $value = DB::raw("'" . mysql_escape(strtolower($value)) . "'");
        }
    } else {
        if ($queryType == 'athena') {
            $value = DB::raw(athena_escape(strtolower($value)));
        } else {
            //convert black slash / to // as db treat black slash as escapse character and escaping apostrophe
            $value = DB::raw("'%" . db_esc_like_raw(strtolower($value)) . "%'");
        }
    }
    return $value;
}

function convertCollectionIntoArray($collection)
{
    $array = collect($collection)->map(function ($x) {
        return (array)$x;
    })->toArray();
    return $array;
}

function truncateFloatingNumber($num, $decimalPoints = 2)
{
    $tens = pow(10, $decimalPoints);
// if you wanted to truncate to nearest 2 decimals, use floor with desired amount of 0.
    $num = floor($num * $tens) / $tens; //
    return $num;
}

function createDateRangeArray($start, $end, $export_dimension)
{
    $array = array();
    $weekNums = [];
    $weekStartDates = [];
    if ($export_dimension == 'date') {
        $array = perDayDateRange($start, $end);
    } else if ($export_dimension == 'week') {
        $dates = perDayDateRange($start, $end);
        $weeks = [];
        foreach ($dates as $date) {
            $yearWeekDate = getWeekNumber($date);
            if (!isset($weeks[$yearWeekDate])) {
                $weeks[$yearWeekDate] = $yearWeekDate;
            }
            $weekNum = intval(explode('-', $yearWeekDate)[1]);
            if (!isset($weekNums[$yearWeekDate])) {
                $weekNums[$yearWeekDate] = $weekNum;
            }
            list($weekStartDate, $weekEndDate) = getStartAndEndDateOFWeek($date);
            if (!isset($weekStartDates[$yearWeekDate])) {
                $weekStartDates[$yearWeekDate] = $weekStartDate;
            }
        }
        $array = array_values($weeks);
//        $weekStartDates = array_values($weekStartDates);
//        $weekNums = array_values($weekNums);
    } else if ($export_dimension == 'month') {
        $start = (new DateTime($start))->modify('first day of this month');
        $end = (new DateTime($end))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        foreach ($period as $dt) {
            $array[] = $dt->format("Y-m");
        }
    }
    return [$array, $weekNums, $weekStartDates];
}

function getWeekNumberIntValue($_date)
{
    $weeknum = collect(DB::select("select week('{$_date}',6) as weeknum;"))->pluck('weeknum');
    $weeknum = $weeknum[0] < 10 ? '0' . $weeknum[0] : $weeknum[0];
    return $weeknum;
}

function getWeekNumber($_date)
{
    $weekNum = getWeekNumberIntValue($_date);
    $weekNumInt = intval($weekNum);
    $month = intval(date('m', strtotime($_date)));
    $year = intval(date('Y', strtotime($_date)));
    if ($month == 1 && $weekNumInt >= 50) {
        $year = $year - 1;
    }
    return $year . '-' . $weekNum;
}


function getStartAndEndDateOFWeek($date)
{
    $date = Carbon::createFromFormat('Y-m-d', $date);
    $date->setWeekStartsAt($date::SUNDAY);
    $date->setWeekEndsAt($date::SATURDAY);
    return [$date->startOfWeek()->format('Y-m-d'), $date->endOfWeek()->format('Y-m-d')];
}


function date_compare($a, $b)
{
    $t1 = strtotime($a['Date']);
    $t2 = strtotime($b['Date']);
    return $t1 - $t2;
}

function insertDataInChunks($table_name, $data, $chunk_size = 1500, $account_id = '')
{
    foreach (array_chunk($data, $chunk_size, true) as $chunk => $chunkHistory) {
        DB::connection(getAccountDedicatedConnection($account_id))->table($table_name)->insert($chunkHistory);
    }
}

/**
 * @param $collection
 * @param string $key
 * @param string $value
 * @return array
 */
function getAssocArray($collection, $key = 'id', $value = 'name')
{
    // get the associative array of object
    $assocArray = [];
    foreach ($collection as $collectionObj) {
        $assocArray[$collectionObj->$key] = $collectionObj->$value;
    }
    return $assocArray;
}

function reconnectDatabaseBeforeJobStarted($connectoinName = 'mysql')
{
    try {

        //try reconnecint mysql database again
        DB::reconnect($connectoinName);

    } catch (\Exception $exceptionOnReConnect) {
        \Illuminate\Support\Facades\Log::info('Job reconnecting fail');
        \Illuminate\Support\Facades\Log::error($exceptionOnReConnect);
    }
}

function disconnectDatabaseAferJobProcessed($connectoinName = 'mysql')
{
    try {

        DB::connection($connectoinName)->disconnect();
    } catch (\Exception $exceptionOnDisConnect) {
        \Illuminate\Support\Facades\Log::info('Job disconnect fail');
        \Illuminate\Support\Facades\Log::error($exceptionOnDisConnect);
    }
}

function getTableSchema($table, $connection = 'mysql')
{
    $describe = DB::connection($connection)->select('SHOW CREATE TABLE ' . $table);
    if (count($describe) > 0) {
        return ltrim(collect($describe[0])['Create Table']) . ";";
    }
    return false;
}

function print_table($data)
{

    $rowCount = 1;
    if (count($data) > 0) {
        ?>


        <?php
        $header = array_keys($data[0]);
        foreach ($header as $headercol) {
            ?>
            <?php echo $headercol; ?>
            <?php
        }
        ?>

        <?php
        foreach ($data as $row) { ?>

            <?php echo $rowCount++; ?>
            <?php
            foreach ($row as $val) {
                ?>
                <?php echo $val; ?>
                <?php
            }
            ?>

            <?php
        }
        ?>

        <?php
    }

}

function cleanString($string)
{
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function te_table_sorting_class($colKey, $col, $order, $order_by)
{
    $sortingClass = '';
    if (isset($col['sorting']) && $col['sorting'] == true) {
        $sortingClass .= 'order_by';


        if ($colKey == $order_by) {
            if (te_compare_strings($order, 'desc')) {
                $sortingClass .= " sorting_desc";
            } else if (te_compare_strings($order, 'asc')) {
                $sortingClass .= " sorting_asc";
            }
        } else {
            $sortingClass .= " sorting";
        }
    }
    return $sortingClass;
}

function getAccountDedicatedConnection($account_id = '', $loadForcefully = false, $config_id = null)
{
    return 'mysql';
}

function prepareExportCSVCommand($rawQuery, $fileName)
{

    $chars_to_be_replaced = array('\\', "\0", "\n", "\t", "\r", '"', "\x1a", '`');
    $chars_replaced_with = array('\\\\', '\\0', '', '', '', '\\"', '\\Z', '\`');
    $rawQuery = str_replace($chars_to_be_replaced, $chars_replaced_with, $rawQuery);
    $sqlCommand = getClientDedicatedMYSQLCommand();
    $command = $sqlCommand . ' -e "' . $rawQuery . '" > \'' . public_path('uploads/' . $fileName) . "'";
    return $command;
}

function getClientDedicatedMYSQLCommand()
{
    return 'mysql -h ' . env('READ_HOST', '127.0.0.1') . ' -u ' . env('DB_USERNAME', 'homestead') . ' -p' . env('DB_PASSWORD') . ' --database=' . env('DB_DATABASE', 'ams') . ' --port=' . env('DB_PORT', 3306);
}

function column_types()
{

    $def = [];
    $def['impressions'] = 'number';
    $def['clicks'] = 'number';
    $def['ctr'] = 'percent';
    $def['avgCpc'] = 'currency';
    $def['cost'] = 'currency';
    $def['attributedConversions30d'] = 'number';
    $def['attributedSales30d'] = 'currency';
    $def['attributedConversions30dSameSKU'] = 'number';
    $def['attributedSales30dSameSKU'] = 'currency';
    $def['roas'] = 'number';
    $def['acos'] = 'percent';
    $def['cpa'] = 'currency';
    $def['conversionRate'] = 'percent';
    $def['camp_daily_budget'] = 'currency';
    $def['daily_budget'] = 'currency';
    $def['default_bid'] = 'currency';
    return $def;
}

function bulkInsertFromTempTableJoin($targetTableName, $tempTableName, $targetTableKeyCol, $inserted_columns = array(), $joinColumns = [], $whereClause = '', $dropTempTable = true, $account_id = '')
{
    $inserted = '';

    $insert_query_cols = [];
    $select_query_cols = [];

    //      DB cols for update query
    foreach ($inserted_columns as $key => $col) {
        $insert_query_cols[] = $key;
        $select_query_cols[] = $col;
    }

    $query = "";
    try {

        $query = "INSERT INTO " . $targetTableName . " (" . implode(",", $insert_query_cols) . ")
                SELECT " . implode(",", $select_query_cols) . " FROM  " . $tempTableName . "  LEFT JOIN " . $targetTableName . "  ON  ";
        $query .= prepareJoinConditions($joinColumns, $targetTableName, $tempTableName);
        $query .= " WHERE " . $targetTableName . " . `" . $targetTableKeyCol . "` IS NULL $whereClause";
        $query = str_replace('{tempTableName}', $tempTableName, $query);
        Log::info("bulk Insert From TempTable Join QUERY");
        Log::info($query);
        DB::connection(getAccountDedicatedConnection($account_id))->statement($query);

    } catch (\Exception $er) {
        $inserted = $er->getMessage();
        Log::info($er);
        notifyBugsnagError(
            $er->getMessage() . ' -- Error in bulkInsertFromTempTableJoin',
            [
                'exception' => $er,
                'Target TableName' => $targetTableName,
                'tempTableName' => $tempTableName,
                'columns to be updated' => $inserted_columns,
                'columns to be Joined' => $joinColumns,
                'query' => $query,
            ],
            'info'
        );
    }

    if ($dropTempTable) {
        if (dropTempTableInEntitiesSyncJob()) {
            $query = "drop table if exists " . $tempTableName . ";";
            DB::connection(getAccountDedicatedConnection($account_id))->statement($query);
        }
    }

    return $inserted;
}

function prepareJoinConditions($joinedColumns, $tableA_prefix, $tableB_prefix)
{
    $joinConditions = [];
    foreach ($joinedColumns as $tempTableCol => $prodTableCol) {
        if (te_compare_strings($prodTableCol, 'null')) {
            $joinConditions[] = $tableA_prefix . ".`" . $tempTableCol . "` IS NULL";
        } else {
            $joinConditions[] = $tableA_prefix . ".`" . $tempTableCol . "`=" . $tableB_prefix . ".`" . $prodTableCol . "`";
        }
    }
    $joinedCondition = implode(' AND ', $joinConditions);
    return $joinedCondition;
}

function te_schedule_intervals()
{
    $schedule_intervals = [
        'daily' => "Daily",
        'weekly' => "Weekly",
        'bi-weekly' => "Bi-Weekly",
        'monthly' => "Monthly"
    ];
    return $schedule_intervals;
}

function te_scheduled_week_days()
{
    $schedule_days = [
        'mon' => "Monday",
        'tue' => "Tuesday",
        'wed' => "Wednesday",
        'thu' => "Thursday",
        'fri' => "Friday",
        'sat' => "Saturday",
        'sun' => "Sunday"
    ];
    return $schedule_days;
}

function te_scheduled_hours()
{
    $schedule_hours = [];
    for ($hour = 0; $hour < 24; $hour++) {
        $schedule_hours[] = $hour;
    }
    return $schedule_hours;
}

function te_scheduled_month_dates()
{
    $schedule_day = [];
    for ($day = 0; $day <= 31; $day++) {
        $schedule_day[] = $day;
    }
    return $schedule_day;
}

function automation_rule_schedule_html($rule)
{

    $html = ucfirst($rule->schedule_interval);
    if ($rule->schedule_interval != 'daily') {
        $html .= ' on ';
    }
    if ($rule->schedule_interval == 'weekly' || $rule->schedule_interval == 'bi-weekly') {
        $html .= ucfirst($rule->scheduled_day);
    } else if ($rule->schedule_interval == 'monthly') {
        $html .= te_month_date($rule->scheduled_date);
    }
    $html .= ' @ ';
    $html .= str_replace(' ', '', strtolower(te_24hours($rule->scheduled_hour, false)));

    return $html;
}

function updateRecipientStatusFromLinkMobility($automation, $ruleRunHistory, $number, $ref, $status = '')
{
    $recipientPhone = $number;
    $recipient = Recipient::where(
        [
            'phone' => $recipientPhone,
            'list_id' => $automation->list_id,
        ]
    )->first();
    if ($recipient) {

        $report = SchedulerDeliveryReport::where([
            'scheduler_history_id' => $ruleRunHistory->id,
            'recipient_id' => $recipient->id,
        ])->first();

        if ($report) {
            $report->sms_sid = $ref;
            $report->message_sid = $ref;
            $report->sms_status = $status;
            $report->message_status = $status;
            $report->save();
        }
    }
}

/**
 * @param $recipient
 * @param \Illuminate\Database\Eloquent\Relations\HasOne $message
 */
function sendLinkMobilitySMSinBulk($numbers, $message,$msgid,$unsub_content, $automation, $ruleRunHistory,$ids)
{
    $smsObj = \App\Libraries\LinkMobilitySMSAPI::getSMSAPIObj();
    $response = $smsObj->sendSMS($numbers, $message,$msgid,$unsub_content, $ruleRunHistory->id,$ids);
    return '';
}
function sendMessenteSMSinBulk($numbers, $message,$msgid,$unsub_content, $automation, $ruleRunHistory,$ids,$SchedulerDeliveryReportids)
{
    $smsObj = \App\Libraries\MessenteSMSAPI::getSMSAPIObj();

    $api = $smsObj::apiInstance();
    $smsObj->sendSMS($api,$numbers,$message, $msgid, $unsub_content,  $ruleRunHistory->id, $ids,$SchedulerDeliveryReportids);
    return '';
}

function sendTwillioSMSinBulk($numbers, $message, $ruleRunHistoryId = '')
{
    $to = [];
    foreach ($numbers as $number) {
        //adding + to the number
        $to[] = '{"binding_type":"sms","address":"+' . trim($number, '+') . '"}';
    }

    $twilioAccountId = env('TWILIO_ACCOUNT_ID', '');
    $twilioToken = env('TWILIO_TOKEN', '');
    $twilioFromNumber = env('TWILIO_FROM', '');
    $services_id = env('TWILIO_SERVICE_ID', '');

    $callbakcUrl = te_url('twilioipn/' . $ruleRunHistoryId);

    if (is_local_environment()) {
        $callbakcUrl = 'https://enfiaq7fmmio5.x.pipedream.net';
    }

    $client = new \Twilio\Rest\Client($twilioAccountId, $twilioToken);
    $notification = $client->notify
        ->services($services_id)
        ->notifications
        ->create(
            [
                'ToBinding' => $to,
                'Body' => $message,
                'Sms' => ['status_callback' => $callbakcUrl],
            ]
        );
    return $notification->sid;
}


function sidebarNavigation($cuser, $currentnav)
{
    $dashboard_item = [
        'name' => 'Dashboard',
        'slug' => 'dashboard',
        'icon' => 'fa fa-home',
        'submenu' => false,
    ];

    $user_item = [
        'name' => 'Users',
        'slug' => 'users',
        'icon' => 'fa fa-users',
        'submenu' => false,
    ];

    $list_item = [
        'name' => 'Lists',
        'slug' => 'lists',
        'icon' => 'fa fa-list',
        'submenu' => false,
    ];

    $message_item = [
        'name' => 'Messages',
        'slug' => 'messages',
        'icon' => 'fa fa-envelope-o',
        'submenu' => false,
    ];
    $message_email = [
        'name' => 'Email Templates',
        'slug' => 'emails',
        'icon' => 'fa fa-envelope-o',
        'submenu' => false,
    ];

    $recipient_item = [
        'name' => 'Recipients',
        'slug' => 'recipients',
        'icon' => 'fa fa-users',
        'submenu' => false,
    ];

    $automation_item = [
        'name' => 'Automation',
        'slug' => 'automation',
        'icon' => 'fa fa-cogs',
        'submenu' => false,
    ];

    $CS_item = [
        'name' => 'Customer Support',
        'slug' => 'cs',
        'icon' => 'fa fa-users',
        'submenu' => false,
    ];
    $segment_item = [
        'name' => 'Segments',
        'slug' => 'segments',
        'icon' => 'fa fa-road',
        'submenu' => false,
    ];

    $sidebarMainNav = [
        'dashboard' => $dashboard_item,
//        'users' => $user_item,
        'lists' => $list_item,
        'recipients' => $recipient_item,
        'messages' => $message_item,
        'emails' => $message_email,
        'automation' => $automation_item,
        'cs' => $CS_item,
        'segments' => $segment_item
    ];

    foreach ($sidebarMainNav as $slug => $menuItem) {
        if (iCheckInArray($slug, nonACLRoutes()) != -1) {
            continue;
        }
        if (!te_check_acl($cuser->role, $slug)) {
            unset($sidebarMainNav[$slug]);
        }
    }

    return $sidebarMainNav;

}

if (!function_exists('app_page_routes')) {
    function app_page_routes()
    {
        $_pages = [
            'dashboard' => 'DashboardController',
            'messages' => 'MessageController',
            'emails' => 'EmailController',
            'recipients' => 'RecipientController',
            'lists' => 'ListController',
            'automation' => 'AutomationController',
            'cs' => 'CSController',
            'segments' => 'SegmentController'
        ];
        return $_pages;
    }
}


function nonACLRoutes()
{
    $slugs = [

    ];
    return $slugs;
}

function loginUserRedirection()
{
    return $userRelatedData = [
        ROLE_ADMIN => ['redirect' => 'dashboard', 'id' => 'id', 'table' => 'users'],
        ROLE_CS => ['redirect' => 'cs', 'id' => 'id', 'table' => 'users'],
    ];
}

function roles_acl($role)
{

    $acl = [
        ROLE_ADMIN => [
            'dashboard',
            'automation',
            'recipients',
            'messages',
            'emails',
            'lists',
            'segments',
        ],
        ROLE_CS => [
            'cs'

        ]
    ];


    return empty($role) ? $acl : (isset($acl[$role]) ? $acl[$role] : $acl);

}

function te_check_acl($role, $feature)
{
    $acl = roles_acl($role);
    return iCheckInArray($feature, $acl) !== -1;
}

function emailHelper($emails, $subject, $content, $template_id = '', $scheduler_history_id = null, $list_id = null)
{
    $count = count($emails);
    try {
        $from = new From(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
        $mail = null;
        for ($i = 0; $i < $count; $i++) {

            if (is_null($mail)) {
                $to = new To($emails[$i], $emails[$i]);
                if (empty($template_id)) {
                    $content = new Content("text/html", $content);
                    try {
                        $mail = new \SendGrid\Mail\Mail($from, $to, $subject, $plainText = null, $content);
                    } catch (\Exception $e) {
                        Log::info("Error occurred  " . $e->getMessage());
                    }
                } else {
                    try {
                        $mail = new \SendGrid\Mail\Mail($from, $to, $subject);
                    } catch (\Exception $e) {
                        Log::info("Error occurred  " . $e->getMessage());
                    }

                }
                continue;
            }

            $personalization = new Personalization();
            try {

                $personalization->addTo(new To($emails[$i], $emails[$i]));
                if (!is_null($scheduler_history_id)) {
                    $personalization->addCustomArg(new \SendGrid\Mail\CustomArg("scheduler_history_id", "" . $scheduler_history_id));

                }
                if (!is_null($list_id)) {
                    $personalization->addCustomArg(new \SendGrid\Mail\CustomArg("list_id", "" . $list_id));
                }

                if (!empty($template_id)) {
                    $personalization->addDynamicTemplateData('subject', $subject);
//                Log::info("Subject set in dynamic template data of personalization = " . $subject);
                }
            } catch (\Exception $e) {
                Log::info("Error occurred  " . $e->getMessage());
            }

            $mail->addPersonalization($personalization);
        }

        if (!empty($template_id)) {
            $mail->setTemplateId($template_id);
            $mail->addDynamicTemplateData("subject", $subject);
            Log::info("Subject set in dynamic template data of mail = " . $subject);
        }

        if (!is_null($scheduler_history_id)) {
            $mail->addCustomArg(new \SendGrid\Mail\CustomArg("scheduler_history_id", "" . $scheduler_history_id));
        }

        if (!is_null($list_id)) {
            $mail->addCustomArg(new \SendGrid\Mail\CustomArg("list_id", "" . $list_id));
        }

        return $mail;
    } catch (\Exception $e) {
        Log::error($e);
        Log::info('Error in creating Email Personalization ' . $e->getMessage());
    }
    return null;
}

function sendEmails($emails, $subject, $content, $template_id = '', $scheduler_history_id = null, $list_id = null)
{
    $apiKey = env('SENDGRID_KEY');
    $sg = new \SendGrid($apiKey);
    $request_body = emailHelper($emails, $subject, $content, $template_id, $scheduler_history_id, $list_id);

    try {
        $response = $sg->client->mail()->send()->post($request_body);
        return $response;
    } catch (Exception $e) {
        Log::error($e);
        Log::info('Error in sending Email  ' . $e->getMessage());
    }

}

//info@wf24.net

function send_email_through_mailgun($emails, $subject, $content, $template_id = null, $scheduler_history_id = null, $list_id = null)
{

    $apiKey = env('MAILGUN_KEY');
    $recipientVariables = [];
    foreach ($emails as $i => $email) {
        $recipientVariables[$email][] = ["id" => $i];
    }
    $data = [
        'from' => env('MAIL_FROM_NAME') . ' ' . env('MAIL_FROM_ADDRESS'),
        'to' => $emails,
        'subject' => $subject,
        'text' => $content,
        'recipient-variables' => json_encode($recipientVariables),
        'h:X-Mailgun-Variables' => '{"list_id": ' . $list_id . ', "scheduler_history_id": ' . $scheduler_history_id . '}'
    ];

    if (!is_null($template_id)) {
        $data['template'] = $template_id;
//        $data['template'] = 'd-964b58aca2bd41889472a32db28182ea';
    }

//    dd($data);

    $mg = Mailgun::create($apiKey); // For US servers

    $response = $mg->messages()->send('info.wf24.net', $data);

//    dd($response->getId());

    return $response;


}
function test_job(){

    $queue_name = Q_RULE_SYNC;
    $job = new ProcessSegmentAutomationRuleJob(249);
    $job->onQueue($queue_name);
    dispatch($job);

}
