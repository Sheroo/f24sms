<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
/*
 * this is temp model and can be used for temporary table like
 * $obj = new TempModel();
 * $obj->setTable($changeHistoryTable);
 */
class TempModel extends Model
{
    public function getTableColumns($tableName = '')
    {
        $tableName = empty($tableName) ? $this->getTable() : $tableName;
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($tableName);
    }
    public function getTableColumnTypes($tableName)
    {
        $columns = [];
        $table_info_columns = DB::select(DB::raw("SHOW COLUMNS FROM " . $tableName));
        foreach ($table_info_columns as $column) {
            $type = $column->Type;
            $pos = stripos($type, '(');
            if ($pos !== false) {
                $type = substr($type, 0, $pos);
            }
            $columns[$column->Field] = $type;
        }
        return $columns;
    }
}
