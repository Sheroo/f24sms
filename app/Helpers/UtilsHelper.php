<?php

function p_rr($data, $exit = 0)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit == 1) {
        exit;
    }
}

if (!function_exists('_dd')) {
    function _dd($arr)
    {
        $bt = debug_backtrace();
        $stackTrace = isset($bt[0]) ? $bt[0] : [];
        $stackTrace = is_array($stackTrace) ? $stackTrace : [];

        $stackTrace2 = isset($bt[1]) ? $bt[1] : [];
        $stackTrace2 = is_array($stackTrace2) ? $stackTrace2 : [];
        $dd_location = [
            'file' => @$stackTrace['file'],
            'function' => @$stackTrace2['function'],
            'line' => @$stackTrace['line']
        ];
        p_rr($dd_location);
        dd($arr);
    }
}

function clear_both()
{
    echo '<div style="clear: both;"></div>';
}

function camelFix($str)
{
    return preg_replace_callback('/(?<!\b)[A-Z][a-z]+|(?<=[a-z])[A-Z]/', function ($match) {
        return ' ' . $match[0];
    }, $str);
}

function te_wordify($str, $camelFix = false)
{
    if ($camelFix) {
        $str = camelFix($str);
    }
    return ucwords(str_replace('_', ' ', trim($str)));
}

if (!function_exists('convert_elequent_to_sql_query')) {
    function convert_elequent_to_sql_query($elequent, $query = null, $binding = null)
    {
        $query = is_null($query) ? $elequent->toSql() : $query;
        $biding = is_null($binding) ? $elequent->getBindings() : $binding;
        if (count($biding) > 0) {
            foreach ($biding as $oneBind) {
                $from = '/' . preg_quote('?', '/') . '/';
                $to = "'" . $oneBind . "'";
                $query = preg_replace($from, $to, $query, 1);
            }
        }
        return $query;
    }
}

function formatDollarValue($value)
{
    $value = str_replace('$', '', $value);
    $value = str_replace(',', '', $value);
    $dollar = '$';
    if ($value < 0) {
        $dollar = '-$';
        $value = $value * -1;
    }
    $value = number_format($value, 2, '.', ',');
    return $dollar . $value;
}


if (!function_exists('remove_alias')) {
    function remove_alias($data)
    {
        if (is_array($data)) {
            $new_data = [];
            foreach ($data as $key => $val) {
                $parts = explode('.', $key);
                $new_data[isset($parts[1]) ? $parts[1] : $parts[0]] = $val;
            }

            return $new_data;
        } else {
            $parts = explode('.', $data);
            return isset($parts[1]) ? $parts[1] : $parts[0];
        }
    }
}

if (!function_exists('date_range')) {
    function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
    {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {

            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }
}

function te_preloader()
{
//    return '<div class="te-preloader preloader"><div class="cssload-speeding-wheel"></div></div>';
    return '<div class="preloader te-preloader"><div class="cssload-speeding-wheel"><svg version="1.1" class="svg-loader" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve"> <image x="0" y="0" width="80" height="80" xlink:href="https://cdn-website.cpcstrategy.com/wp-content/uploads/CPC-Strategy-mark-dark.png" /> <path fill="#0099a8" d="M10,40c0,0,0-0.4,0-1.1c0-0.3,0-0.8,0-1.3c0-0.3,0-0.5,0-0.8c0-0.3,0.1-0.6,0.1-0.9c0.1-0.6,0.1-1.4,0.2-2.1 c0.2-0.8,0.3-1.6,0.5-2.5c0.2-0.9,0.6-1.8,0.8-2.8c0.3-1,0.8-1.9,1.2-3c0.5-1,1.1-2,1.7-3.1c0.7-1,1.4-2.1,2.2-3.1 c1.6-2.1,3.7-3.9,6-5.6c2.3-1.7,5-3,7.9-4.1c0.7-0.2,1.5-0.4,2.2-0.7c0.7-0.3,1.5-0.3,2.3-0.5c0.8-0.2,1.5-0.3,2.3-0.4l1.2-0.1 l0.6-0.1l0.3,0l0.1,0l0.1,0l0,0c0.1,0-0.1,0,0.1,0c1.5,0,2.9-0.1,4.5,0.2c0.8,0.1,1.6,0.1,2.4,0.3c0.8,0.2,1.5,0.3,2.3,0.5 c3,0.8,5.9,2,8.5,3.6c2.6,1.6,4.9,3.4,6.8,5.4c1,1,1.8,2.1,2.7,3.1c0.8,1.1,1.5,2.1,2.1,3.2c0.6,1.1,1.2,2.1,1.6,3.1 c0.4,1,0.9,2,1.2,3c0.3,1,0.6,1.9,0.8,2.7c0.2,0.9,0.3,1.6,0.5,2.4c0.1,0.4,0.1,0.7,0.2,1c0,0.3,0.1,0.6,0.1,0.9 c0.1,0.6,0.1,1,0.1,1.4C74,39.6,74,40,74,40c0.2,2.2-1.5,4.1-3.7,4.3s-4.1-1.5-4.3-3.7c0-0.1,0-0.2,0-0.3l0-0.4c0,0,0-0.3,0-0.9 c0-0.3,0-0.7,0-1.1c0-0.2,0-0.5,0-0.7c0-0.2-0.1-0.5-0.1-0.8c-0.1-0.6-0.1-1.2-0.2-1.9c-0.1-0.7-0.3-1.4-0.4-2.2 c-0.2-0.8-0.5-1.6-0.7-2.4c-0.3-0.8-0.7-1.7-1.1-2.6c-0.5-0.9-0.9-1.8-1.5-2.7c-0.6-0.9-1.2-1.8-1.9-2.7c-1.4-1.8-3.2-3.4-5.2-4.9 c-2-1.5-4.4-2.7-6.9-3.6c-0.6-0.2-1.3-0.4-1.9-0.6c-0.7-0.2-1.3-0.3-1.9-0.4c-1.2-0.3-2.8-0.4-4.2-0.5l-2,0c-0.7,0-1.4,0.1-2.1,0.1 c-0.7,0.1-1.4,0.1-2,0.3c-0.7,0.1-1.3,0.3-2,0.4c-2.6,0.7-5.2,1.7-7.5,3.1c-2.2,1.4-4.3,2.9-6,4.7c-0.9,0.8-1.6,1.8-2.4,2.7 c-0.7,0.9-1.3,1.9-1.9,2.8c-0.5,1-1,1.9-1.4,2.8c-0.4,0.9-0.8,1.8-1,2.6c-0.3,0.9-0.5,1.6-0.7,2.4c-0.2,0.7-0.3,1.4-0.4,2.1 c-0.1,0.3-0.1,0.6-0.2,0.9c0,0.3-0.1,0.6-0.1,0.8c0,0.5-0.1,0.9-0.1,1.3C10,39.6,10,40,10,40z" > <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 40 40" to="360 40 40" dur="1.3s" repeatCount="indefinite" /> </path> </svg></div></div>';
}


function te_get_spinner_svg()
{
    ob_start(); ?>
    <!-- SPINNER -->
    <svg version="1.1"
         class="svg-loader"
         xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         x="0px"
         y="0px"
         viewBox="0 0 80 80"
         xml:space="preserve">



<path
    fill="#0099a8"
    d="M10,40c0,0,0-0.4,0-1.1c0-0.3,0-0.8,0-1.3c0-0.3,0-0.5,0-0.8c0-0.3,0.1-0.6,0.1-0.9c0.1-0.6,0.1-1.4,0.2-2.1
	c0.2-0.8,0.3-1.6,0.5-2.5c0.2-0.9,0.6-1.8,0.8-2.8c0.3-1,0.8-1.9,1.2-3c0.5-1,1.1-2,1.7-3.1c0.7-1,1.4-2.1,2.2-3.1
	c1.6-2.1,3.7-3.9,6-5.6c2.3-1.7,5-3,7.9-4.1c0.7-0.2,1.5-0.4,2.2-0.7c0.7-0.3,1.5-0.3,2.3-0.5c0.8-0.2,1.5-0.3,2.3-0.4l1.2-0.1
	l0.6-0.1l0.3,0l0.1,0l0.1,0l0,0c0.1,0-0.1,0,0.1,0c1.5,0,2.9-0.1,4.5,0.2c0.8,0.1,1.6,0.1,2.4,0.3c0.8,0.2,1.5,0.3,2.3,0.5
	c3,0.8,5.9,2,8.5,3.6c2.6,1.6,4.9,3.4,6.8,5.4c1,1,1.8,2.1,2.7,3.1c0.8,1.1,1.5,2.1,2.1,3.2c0.6,1.1,1.2,2.1,1.6,3.1
	c0.4,1,0.9,2,1.2,3c0.3,1,0.6,1.9,0.8,2.7c0.2,0.9,0.3,1.6,0.5,2.4c0.1,0.4,0.1,0.7,0.2,1c0,0.3,0.1,0.6,0.1,0.9
	c0.1,0.6,0.1,1,0.1,1.4C74,39.6,74,40,74,40c0.2,2.2-1.5,4.1-3.7,4.3s-4.1-1.5-4.3-3.7c0-0.1,0-0.2,0-0.3l0-0.4c0,0,0-0.3,0-0.9
	c0-0.3,0-0.7,0-1.1c0-0.2,0-0.5,0-0.7c0-0.2-0.1-0.5-0.1-0.8c-0.1-0.6-0.1-1.2-0.2-1.9c-0.1-0.7-0.3-1.4-0.4-2.2
	c-0.2-0.8-0.5-1.6-0.7-2.4c-0.3-0.8-0.7-1.7-1.1-2.6c-0.5-0.9-0.9-1.8-1.5-2.7c-0.6-0.9-1.2-1.8-1.9-2.7c-1.4-1.8-3.2-3.4-5.2-4.9
	c-2-1.5-4.4-2.7-6.9-3.6c-0.6-0.2-1.3-0.4-1.9-0.6c-0.7-0.2-1.3-0.3-1.9-0.4c-1.2-0.3-2.8-0.4-4.2-0.5l-2,0c-0.7,0-1.4,0.1-2.1,0.1
	c-0.7,0.1-1.4,0.1-2,0.3c-0.7,0.1-1.3,0.3-2,0.4c-2.6,0.7-5.2,1.7-7.5,3.1c-2.2,1.4-4.3,2.9-6,4.7c-0.9,0.8-1.6,1.8-2.4,2.7
	c-0.7,0.9-1.3,1.9-1.9,2.8c-0.5,1-1,1.9-1.4,2.8c-0.4,0.9-0.8,1.8-1,2.6c-0.3,0.9-0.5,1.6-0.7,2.4c-0.2,0.7-0.3,1.4-0.4,2.1
	c-0.1,0.3-0.1,0.6-0.2,0.9c0,0.3-0.1,0.6-0.1,0.8c0,0.5-0.1,0.9-0.1,1.3C10,39.6,10,40,10,40z"
>

    <animateTransform
        attributeType="xml"
        attributeName="transform"
        type="rotate"
        from="0 40 40"
        to="360 40 40"
        dur="0.9s"
        repeatCount="indefinite"
    />
</path>
</svg>
    <?php
    return ob_get_clean();
}


function te_get_name_initials($name)
{
    $name_parts = explode(' ', $name);
    $first_initial = strtoupper(substr(trim($name_parts[0]), 0, 1));
    $last_initial = isset($name_parts[0]) ? strtoupper(substr(trim($name_parts[1]), 0, 1)) : '';

    return $first_initial . $last_initial;

}

function te_intval($number)
{
    $number = str_replace('$', '', trim($number));
    $number = str_replace(',', '', $number);
    $number = intval($number);
    $number = number_format($number, 0, '.', ',');

    return $number;
}


if (!function_exists('te_compare_strings')) {
    function te_compare_strings($str1, $str2, $ignore_case = true)
    {
        if ($ignore_case) {
            $str1 = str_replace(' ', '', strtolower($str1));
            $str2 = str_replace(' ', '', strtolower($str2));
        }
        $str1 = trim($str1);
        $str2 = trim($str2);

        return $str1 == $str2;
    }
}

if (!function_exists('removeStringSpaces')) {
    function removeStringSpacesAndLowerCase($str1)
    {
        $str1 = str_replace(' ', '', strtolower($str1));
        $str1 = trim($str1);

        return $str1;
    }
}


function sksort(&$array, $subkey = "id", $sort_ascending = false)
{

    if (count($array)) {
        $temp_array[key($array)] = array_shift($array);
    }

    foreach ($array as $key => $val) {
        $offset = 0;
        $found = false;
        foreach ($temp_array as $tmp_key => $tmp_val) {
            if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                $temp_array = array_merge((array)array_slice($temp_array, 0, $offset),
                    array($key => $val),
                    array_slice($temp_array, $offset)
                );
                $found = true;
            }
            $offset++;
        }
        if (!$found) {
            $temp_array = array_merge($temp_array, array($key => $val));
        }
    }

    if ($sort_ascending) {
        $array = array_reverse($temp_array);
    } else {
        $array = $temp_array;
    }
}


function te_24hours($h = -1, $zeroes = true)
{
    $hours = [];
    $hours[] = '12 AM';
    for ($i = 1; $i < 24; $i++) {
        $hour = ($i % 12);
        $hour = $hour == 0 ? 12 : $hour;
        $hours[] = ($zeroes ? ($hour < 10 ? '0' : '') : '') . ($hour) . ($i < 12 ? ' AM' : ' PM');
    }

    return $h == -1 ? $hours : @$hours[$h];
}

function te_week_days()
{
    return ['Sun' => 'Sun', 'Mon' => 'Mon', 'Tue' => 'Tue', 'Wed' => 'Wed', 'Thu' => 'Thu', 'Fri' => 'Fri', 'Sat' => 'Sat'];

}


function te_validate_date($date_str)
{

    $match = false;
    if (preg_match("/^(0[1-9]|1[0-2]|[1-9])\/(0[1-9]|[1-2][0-9]|3[0-1]|[1-9])\/([0-9]{4}|[0-9]{2})$/", $date_str)) {
        $match = date('m/d/Y', strtotime($date_str));
    }

    return $match;
}


function days_array_between_dates($dateStart, $dateEnd, $format = 'Y-m-d')
{

    $dateStart = date($format, strtotime($dateStart));
    $dateEnd = date($format, strtotime($dateEnd));

    $days = [];

    while (strtotime($dateStart) <= strtotime($dateEnd)) {
        $days[] = $dateStart;
        $dateStart = date('Y-m-d', strtotime($dateStart . ' +1 day'));
    }

    return $days;
}

function dates_pseudo_table($dateStart, $dateEnd, $dateColName = 'report_date')
{

    $query = "select * from
    (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) {$dateColName} from
(select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
(select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
(select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
(select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
(select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
WHERE {$dateColName} between '{$dateStart}' and '{$dateEnd}'";

    return $query;
}


function check_table_exists($table_name, $account_id = '')
{

    $db = env('DB_DATABASE', 'ams');
    $res = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$db' AND table_name = '$table_name'";
    if (!empty($account_id)) {
        $res = \Illuminate\Support\Facades\DB::connection(getAccountDedicatedConnection($account_id))->select($res);
    } else {
        $res = \Illuminate\Support\Facades\DB::select($res);
    }


    $table_exists = false;

    if ($res) {
        $table_exists = true;
    }

    return $table_exists;
}


if (!function_exists('array_insert_after')) {
    function array_insert_after(&$array, $position, $insert)
    {
        if (is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos = array_search($position, array_keys($array));
            if ($pos !== false) {
                $array = array_merge(
                    array_slice($array, 0, $pos + 1),
                    $insert,
                    array_slice($array, $pos + 1)
                );
            }

        }
    }
}


function te_month_date($date)
{
    $postfix = 'th';
    if ($date < 10 || $date > 20) {
        if ($date % 10 == 1) {
            $postfix = 'st';
        } else if ($date % 10 == 2) {
            $postfix = 'nd';
        } else if ($date % 10 == 3) {
            $postfix = 'rd';
        }
    }
    return $date . $postfix;
}


function iCheckInArray($needle, $array)
{
    $matchedValueInArray = -1;
    foreach ($array as $value) {
        if (te_compare_strings($value, $needle)) {
            $matchedValueInArray = $value;
            break;
        }
    }
    return $matchedValueInArray;

}

function iMatchInArray($needle, $array)
{
    $matchedValueInArray = -1;
    /*
     * Special case for Entity as this is conflicting with Target Entity
     */

    foreach ($array as $value) {
        if (stripos($needle, $value) != false) {
            $matchedValueInArray = $value;
            break;
        }
    }
    return $matchedValueInArray;

}

function iCheckCommaValuesInArray($commaSepValues, $array, $sep = ',', &$matchedValues = [], &$unMatchedValues = [])
{
    $matchedValueInArray = -1;
    $finalMatchedArray = [];
    if (!is_array($commaSepValues)) {
        $commaSeparatedValuesArray = explode($sep, $commaSepValues);
    } else {
        $commaSeparatedValuesArray = $commaSepValues;
    }
    // trim spaces
    foreach ($commaSeparatedValuesArray as $index => $value) {
        $commaSeparatedValuesArray[$index] = trim($value);
    }
    $mismatchFound = false;
    $matchesValues = [];
    foreach ($commaSeparatedValuesArray as $value) {
        $found = iCheckInArray($value, $array);
        if ($found != -1) {
            $matchesValues[] = $found;
            $matchedValues[] = $found;
        } else {
            $unMatchedValues[] = $value;
//            $mismatchFound = true;
//            break;
        }
    }
    if (count($unMatchedValues) > 0) {
        $mismatchFound = true;
    }
    if (!$mismatchFound) {
        foreach ($array as $value) {
            $found = iCheckInArray($value, $matchesValues);
            if ($found != -1) {
                $finalMatchedArray[] = $value;
            }
        }
        $matchedValues = $finalMatchedArray;
        $matchedValueInArray = implode($sep, $finalMatchedArray);
    }
    return $matchedValueInArray;
}

if (!function_exists('merge_response_array_into_one')) {
    function merge_response_array_into_one($responses)
    {
        $apiResponseToArray = [];
        foreach ($responses as $Aresponse) {
            $apiResponseToArray = array_merge($apiResponseToArray, $Aresponse);
        }
        return $apiResponseToArray;
    }
}


if (!function_exists('createChunksOfData')) {
    function createChunksOfData($ApiData, $chunkSize)
    {
        $arrayChunks = [];
        if (count($ApiData) > $chunkSize) {
            $arrayChunks = array_chunk($ApiData, $chunkSize);
        } else {
            $arrayChunks[] = $ApiData;
        }

        return $arrayChunks;

    }
}


function get_key_value_by_value_from_array($array, $value)
{
    $found = array_search($value, $array);
    return $found === false ? '' : $found;
}


function textEncodingisInvalid($text)
{
    return !mb_detect_encoding($text, 'UTF-8', true);
}

function amazonSpecialCharactersWindows1252HTMLEncoded()
{
    return ['&amp;' => '&', '&reg;' => '®', '&Aacute;' => 'Á', '&Eacute;' => 'É', '&Iacute;' => 'Í', '&Ntilde;' => 'Ñ', '&Oacute;' => 'Ó', '&Uacute;' => 'Ú', '&Uuml;' => 'Ü', '&aacute;' => 'á', '&eacute;' => 'é', '&iacute;' => 'í', '&ntilde;' => 'ñ', '&oacute;' => 'ó', '&uacute;' => 'ú', '&uuml;' => 'ü', '&Auml;' => 'Ä', '&Ouml;' => 'Ö', '&OElig;' => 'Œ', '&szlig;' => 'ß', '&auml;' => 'ä', '&ouml;' => 'ö', '&Agrave;' => 'À', '&Acirc;' => 'Â', '&AElig;' => 'Æ', '&Ccedil;' => 'Ç', '&Egrave;' => 'È', '&Ecirc;' => 'Ê', '&Euml;' => 'Ë', '&Icirc;' => 'Î', '&Iuml;' => 'Ï', '&Ocirc;' => 'Ô', '&Ugrave;' => 'Ù', '&Ucirc;' => 'Û', '&Yuml;' => 'Ÿ', '&agrave;' => 'à', '&acirc;' => 'â', '&aelig;' => 'æ', '&ccedil;' => 'ç', '&egrave;' => 'è', '&ecirc;' => 'ê', '&euml;' => 'ë', '&icirc;' => 'î', '&iuml;' => 'ï', '&ocirc;' => 'ô', '&ugrave;' => 'ù', '&ucirc;' => 'û', '&yuml;' => 'ÿ', '&oelig;' => 'œ',];
}

function amazonSpecialCharactersEncodeHtmlEntities($encoding = 'cp1252')
{
    $specialChars = ['-', '&', '\b', '+', '\t', '\n', '\r', '®', 'Á', 'É', 'Í', 'Ñ', 'Ó', 'Ú', 'Ü', 'á', 'é', 'í', 'ñ', 'ó', 'ú', 'ü', 'Ä', 'Ö', 'Œ', 'ß', 'ä', 'ö', 'À', 'Â', 'Æ', 'Ç', 'È', 'Ê', 'Ë', 'Î', 'Ï', 'Ô', 'Ù', 'Û', 'Ÿ', 'à', 'â', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'î', 'ï', 'ô', 'ù', 'û', 'ÿ', 'œ', '-', '!', '$', '"', "'", '#', '%', '&', '(', ')', '*', '+', ',', '.', '/', ':', ';', '\<', '=', '\>', '?', '@', '\ ', '\\', '[', ']', '_', '`', '|', '{', '}', '~', '®', 'Á', 'É', 'Í', 'Ñ', 'Ó', 'Ú', 'Ü', 'á', 'é', 'í', 'ñ', 'ó', 'ú', 'ü', 'Ä', 'Ö', 'Ü', 'ß', 'ä', 'ö', 'ü', 'À', 'Â', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Î', 'Ï', 'Ô', 'Ù', 'Û', 'Ü', 'Œ', 'Ÿ', 'à', 'â', 'æ', 'ç', 'è', 'ê', 'ë', 'î', 'ï', 'ô', 'ù', 'û', 'ü', 'ÿ', 'œ', '\b'];
    $output = [];
    foreach ($specialChars as $_temp) {
        $encodedText = htmlentities($_temp, ENT_QUOTES, $encoding);
        if ($encodedText != $_temp) {
            $output[$_temp] = $encodedText;
        }
    }
    return $output;
}

function amazonSpecialCharactersDecodeHtmlEntities($string, $encoding = 'cp1252')
{
    return html_entity_decode($string, ENT_QUOTES, $encoding);
}

function convertIfEncodingIsNotUTF8(&$text)
{
    $specialCharLeft = false;
    $allChars = amazonSpecialCharactersWindows1252HTMLEncoded();
    if (mb_detect_encoding($text, ['UTF-8'], true) == false) {
        //default to Windows-1252 encoding if not utf8
        $encodedText = htmlentities($text, ENT_QUOTES, "cp1252");
        foreach ($allChars as $htmlEntity => $utf8) {
            if (strpos($encodedText, $htmlEntity) !== false) {
                $encodedText = str_replace($htmlEntity, $utf8, $encodedText);
            }
        }

        if (mb_detect_encoding($encodedText, ['UTF-8'], true) == false) {
            $specialCharLeft = true;
        }
        $text = $encodedText;
    }
    return $specialCharLeft;
}

function validateHexColor($colorCode)
{
    $colorIsValid = false;
    $validLengths = [3, 4, 6, 8];
    foreach ($validLengths as $length) {
        $regex = '/^#(?:[0-9a-fA-F]{' . $length . '})$/';
        if (preg_match($regex, $colorCode)) {
            $colorIsValid = true;
            break;
        }
    }
    return $colorIsValid;
}

if (!function_exists('csv_to_array')) {
    function csv_to_array($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();
        ini_set('auto_detect_line_endings', true);
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }
}

if (!function_exists('database_lost_connection_messages')) {
    function database_lost_connection_messages()
    {
        return [
            'server has gone away',
            'no connection to the server',
            'Lost connection',
            'is dead or not enabled',
            'Error while sending',
            'decryption failed or bad record mac',
            'server closed the connection unexpectedly',
            'SSL connection has been closed unexpectedly',
            'Error writing data to the connection',
            'Resource deadlock avoided',
            'Transaction() on null',
            'child connection forced to terminate due to client_idle_limit',
            'query_wait_timeout',
            'reset by peer',
            'Physical connection is not usable',
            'TCP Provider: Error code 0x68',
            'Name or service not known',
            'ORA-03114',
            'Packets out of order. Expected',
            'Connection refused',
            'Communication link failure',
            'Connection timed out',
        ];
    }
}


function memory_get_usage_mb()
{
    $size = memory_get_usage(true);
    return (($size / 1024) / 1024);
}
