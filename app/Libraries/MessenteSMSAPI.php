<?php

namespace App\Libraries;

use App\SchedulerDeliveryReport;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use Messente\Api\Api\OmnimessageApi;
use Messente\Api\Configuration;
use Messente\Api\Model\Omnimessage;
use Messente\Api\Model\SMS;

class MessenteSMSAPI
{

    static $callBackURL ;
    protected $userName;
    protected $password;
    protected $sender;

    public function __construct()
    {
    }

    public static function getSMSAPIObj()
    {

        return new MessenteSMSAPI();
    }


    public static function apiInstance()
    {
        $userName = env('MESSENTE_USERNAME');
        $password = env('MESSENTE_PASSWORD');
        $sender = env('MESSENTE_SENDER');
        $config = Configuration::getDefaultConfiguration()
            ->setUsername($userName)
            ->setPassword($password);

        $apiInstance = new OmnimessageApi(
            new Client(),
            $config
        );
        return $apiInstance;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }



    public function sendSMS($api, $numbers, $textContent, $msgid, $unsub_content, $messageId, $ids,$SchedulerDeliveryReportids)
    {
        $callback = env('MESSENTE_CALLBACK'); //change the callback url here

        foreach ($numbers as $i => $number) {
            $id = $ids[$i];
            $SchedulerDeliveryReportid = $SchedulerDeliveryReportids[$i];
            $omnimessage = new Omnimessage([
                'to' => '+'.$number,
                'dlrUrl' => $callback,
            ]);

            $sms = new SMS(
                [
                    'text' => $this->createMsg($number, $textContent, $msgid, $unsub_content, $messageId, $id),
                    'sender' => $this->sender,
                ]
            );

            $omnimessage->setMessages([$sms]);
            try {
                $result = $api->sendOmnimessage($omnimessage);
                $omnimessageid = $result['omnimessageId'];
                print_r($result['omnimessageId']);
                $SchedulerDeliveryReport = SchedulerDeliveryReport::where('id',$id);
                $SchedulerDeliveryReport->message_sid = $omnimessageid;
                $SchedulerDeliveryReport->save();
                exit;
            } catch (Exception $e) {
                $result = 'Exception when calling sendOmnimessage: ' . $e->getMessage() . PHP_EOL;
//                print_r($result);
            }
        }
        return $result;
    }

    public function createMsg($number, $textContent, $msgid, $unsub_content, $messageId, $id)
    {
        $messages = [];
        if (is_null($unsub_content)) {
            $unsub_content = '';
        }

        $url = " \nhttps://f-365.net/{$id}/{$msgid}";
        $unsub_content = "\n" . $unsub_content;
        $contentToSend = $textContent . $url . $unsub_content;
//            $contentToSend = nl2br($contentToSend);
        return $contentToSend;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $gateId
     */
    public function setSender($sender): void
    {
        $this->sender = $sender;
    }


}
