<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use App\Mail\SendEmail;
use App\Recipient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Message;
use App\Modules\Traits\HandleCrud;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use SendGrid\Mail\Bcc;
use SendGrid\Mail\Content;
use SendGrid\Mail\From;
use SendGrid\Mail\Personalization;
use SendGrid\Mail\To;

class EmailController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->controller_slug = 'emails';
        $this->setDefaultCrudConfiguration();
        $this->model = new Message;
        $this->db_table = $this->model->getTable();
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = ['sorting' => false];
        unset($tableCols['this.id']);
        unset($tableCols['this.type']);
        unset($tableCols['this.content']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.deleted_at']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('u.name as created_by');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $eloquentObj = $eloquentObj->where('type', 'LIKE', 'email');
        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('subject', 'LIKE', $search_keyword);
                $q = $q->orWhere('content', 'LIKE', $search_keyword);
            });
        }
        $eloquentObj->whereNull('this.deleted_at');
        return $eloquentObj;
    }

    public function customAjaxActions(Request $request)
    {
        // TODO: Implement customAjaxActions() method.
    }

    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
        $item_form_data['type'] = 'email';
//        dd($item_form_data);

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);


        return $response;
    }

    public function edit_form(Request $request)
    {
        $item_id = $request->get("item_id", 0);
        $itemEloquent = clone($this->model);
        $item = $itemEloquent->where('id', $item_id)->first();
        $formData = [];
        $response = [];
        $formData['itemObj'] = $item;
        $formData['form_action'] = 'edit';
        $formData['item_id'] = $item_id;
        $formData['loginUser'] = Auth::User();
        $formData['controller_slug'] = $this->controller_slug;

        $this->appendCreateEditFormData($request, $item, $formData);
        $response['form_html'] = (String)view($this->controller_slug . '._form')->with($formData);
        $response['status'] = 'success';

        return $response;
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }




}
