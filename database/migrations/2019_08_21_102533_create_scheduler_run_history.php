<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerRunHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler_run_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('scheduler_id');
            $table->unsignedInteger('total_recipients')->default(0);
            $table->unsignedInteger('succeed')->default(0);
            $table->unsignedInteger('failed')->default(0);
            $table->timestamps();

            $table->index(['scheduler_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler_run_history');
    }
}
