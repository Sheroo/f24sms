<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login','ApiController@login');
Route::post('/optin', 'TwilioIPN@post_fundivia_store')->name('optin');
Route::post('/followup', 'TwilioIPN@updateForFollowup')->name('followup');
Route::post('/followup_sms', 'TwilioIPN@updateForFollowup_sms')->name('followup_sms');
Route::post('/messenteipn', 'MessenteIPN@MessenteIPN'); // added the messente IPN

//Route::get('/save_stop', 'TwilioIPN@save_stop')->name('save_stop');
