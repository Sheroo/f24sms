@extends('layouts.master')

@section('head')

    {{--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">--}}
    {{--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">--}}
@endsection

@section('content')

    <div class="header">
        <h2>Manage <strong>Emails</strong></h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{te_route('dashboard')}}">Home</a>
                </li>
                <li class="active">Emails</li>
            </ol>
        </div>
    </div>

    <div class="row">

        <div class="portlets">
            <div class="{{$controller_slug}}-table-wrapper items-table-wrapper"
                 id="{{$controller_slug.'-table-wrapper'}}"
                 data-slug="{{$controller_slug}}">
                @include($controller_slug.'._list')
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include($controller_slug.'._form-modal')
@append


@section('scripts')
    <?php page_script([$controller_slug . '/index']);?>
    <?php page_script([$controller_slug . '/ckeditor/ckeditor']);?>
    {{--<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>--}}
    {{--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>--}}

    {{--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>--}}
@append

