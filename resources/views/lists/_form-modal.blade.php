<div class="modal inmodal item-form-panel" data-slug="{{$controller_slug}}" id="item-form-panel" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated-faster zoomIn" id="item-form-modal-content">
            @include($controller_slug.'._form')
        </div>
    </div>
</div>
