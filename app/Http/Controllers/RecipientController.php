<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use App\Message;
use App\Modules\Traits\HandleCrud;
use App\MyList;
use App\Recipient;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class RecipientController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new Recipient;
        $this->db_table = $this->model->getTable();
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = ['sorting' => false];
        $tableCols['this.list_id'] = ['label' => "List", 'sorting' => true];
        unset($tableCols['this.id']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.country_code']);
        unset($tableCols['this.first_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        unset($tableCols['this.last_name']);
        return $tableCols;
    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        $search_list = trim($request->get('search_list', ''));

        $viewData['search_list'] = $search_list;
        $viewData['listData'] = MyList::all();
        return $viewData;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('u.name as created_by');
        $cols[] = DB::raw('l.name as list_id');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->join(DB::raw('lists l'), function ($join) {
            $join->on('this.list_id', '=', 'l.id');
        });
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {

        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('first_name', 'LIKE', $search_keyword);
                $q = $q->orWhere('last_name', 'LIKE', $search_keyword);
                $q = $q->orWhere('this.email', 'LIKE', $search_keyword);
                $q = $q->orWhere('phone', 'LIKE', $search_keyword);
            });
        }

        $search_list = trim($request->get('search_list', ''));
        if (!empty($search_list)) {
            $eloquentObj = $eloquentObj->where('list_id', '=', $search_list);
        }

        return $eloquentObj;
    }

    public function customAjaxActions(Request $request)
    {
        // TODO: Implement customAjaxActions() method.

    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {
        $formData['lists'] = DB::table('lists')->orderBy('name', 'asc')->get();
    }

    /**
     * Store a newly created resource in storage.
     * Update an already existing resource in database
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response| array
     */
    public function post_fundivia_store(Request $request)
    {

//        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
//        dd($item_form_data);

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
//        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);
        return $response;
    }



}
