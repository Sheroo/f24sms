<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    $user = Auth::user();
    if ($user) {

        $userRelatedData = loginUserRedirection();
        return redirect('/' . $userRelatedData[$user->role]['redirect']);
    }
    return redirect('/dashboard');
})->name('landing_page');

/**
 * Login Route(s)
 */

Route::get('/cs', 'RecipientController@cs')->name('recipients.cs');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
/**
 * Register Route(s)
 */
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');
/**
 * Password Reset Route(S)
 */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
/**
 * Email Verification Route(s)
 */
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::group(['as' => 'main::', 'middleware' => ['auth', 'check-acl']], function () {

    foreach (app_page_routes() as $page_slug => $pageController) {
        Route::get($page_slug, $pageController . '@index')->name($page_slug);
        Route::post($page_slug, $pageController . '@ajaxActions')->name($page_slug . '_ajax_actions');
    }

});


Route::get('login/{role?}', 'Auth\LoginController@showLoginForm')->name('login');


Route::post('/twilioipn/usermessage', 'TwilioIPN@userMessage');
Route::post('/linkmobility/stopmessage', 'TwilioIPN@linkMobilityStopMessage');
Route::post('/twilioipn/{schedulerHistoryId}', 'TwilioIPN@IPN');
Route::post('/linkmobilityipn', 'TwilioIPN@LinkMoblityIPN');
//Route::post('/messenteipn', 'MessenteIPN@MessenteIPN'); // added the messente IPN
Route::get('/unsub/email/{email}', 'TwilioIPN@UnsubEmail');
Route::get('/delete/email/{email}', 'TwilioIPN@DeleteEmail');
Route::post('/mailchimp/unsub/email/webhook/{listid}', 'TwilioIPN@UnsubMailChimpEmailHook');
    Route::get('/mailchimp/unsub/email/webhook/{listid}', 'TwilioIPN@UnsubMailChimpEmailHook');
Route::post('/sendgrid/webhook', 'TwilioIPN@sendGridEmailWebhook');
Route::post('/mailgun/webhook', 'TwilioIPN@mailGunEmailWebhook');

Route::get('/test-msg/{phoneNumber}', function ($phoneNumber) {
    sendTwillioSMSinBulk([$phoneNumber], 'Hello Message fom Hassan @ ' . date('d/m/Y H:i:s'));
});

Route::get('/reconsent/email/{email}', 'TwilioIPN@reConsent');
Route::get('/gdpr/email/{email}', 'TwilioIPN@recipientsData');
Route::get('/updaterecipients', 'TwilioIPN@updateRecipientsForValidEmails');
Route::get('/addrequest', 'RequestController@store');
Route::get('/requests', 'RequestController@requests');
Route::get('/sendEmail', 'RequestController@email');
Route::get('/img', 'ImgUploadController@showImgUpload')->name('upload');
Route::post('/upload', 'ImgUploadController@Upload')->name('post');

Route::get('/{id}/{msgid}', 'TwilioIPN@clickrecipientbyid');
Route::get('/fundivia/{email}/{phone}/{domain}', 'TwilioIPN@tag_fundivia_optin');
Route::get('/f24/{email}/{phone}/{domain}/{product}', 'TwilioIPN@tag_f24_member');



//Login Routes

Route::get('login/{role?}', 'Auth\LoginController@showLoginForm')->name('login');
//Route::get('/sendEmail', 'EmailController@sendHelloEmail');
