<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>F24 SMS</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description"/>
    <meta content="themes-lab" name="author"/>
    <link rel="shortcut icon" href="{{te_asset('global/images/favicon.png')}}" type="image/png">

    <?php
    $styles = [
        'global/css/style.css',
        'global/css/ui.css',
        'global/plugins/bootstrap-loading/lada.min.css',
        'css/custom.css'
    ];
    style($styles);
    ?>
    @yield('styles')
    @yield('head')
</head>
<body class="account separate-inputs" data-page="login">
<div class="container" id="login-block">
    @yield('content')
    <p class="account-copyright">
        <span>Copyright © 2019 </span><span>F24 Media</span>.<span> All rights reserved.</span>
    </p>
</div>
<!-- END LOGIN BOX -->
@include('includes.js-variables')
<?php
$scripts = [
    'global/plugins/jquery/jquery-3.1.0.min.js',
    'global/plugins/jquery/jquery-migrate-3.0.0.min.js',
    'global/plugins/gsap/main-gsap.min.js',
    'global/plugins/tether/js/tether.min.js',
    'global/plugins/bootstrap/js/bootstrap.min.js',
    'global/plugins/backstretch/backstretch.min.js',
    'global/plugins/bootstrap-loading/lada.min.js',
    'global/plugins/jquery-validation/jquery.validate.min.js',
    'global/plugins/jquery-validation/additional-methods.min.js',
//    'global/js/pages/login-v1.js',
];
script($scripts);
?>
@yield('scripts')
</body>
</html>
