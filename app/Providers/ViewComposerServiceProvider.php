<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.master'], function ($view) {
            $cuser = Auth::User();
            $currentnav = \Illuminate\Support\Facades\Request::segment(te_segment('current_slug'));

            $sidebarMainNav = sidebarNavigation($cuser, $currentnav);

            $view->with('currentuser', $cuser);
            $view->with('sidebarMainNav', $sidebarMainNav);
            $view->with('current_navitem_slug', @$currentnav);

            JavaScriptFacade::put([
                'base_url' => te_schema_less_base_url(),
                'asset_url' => te_asset('/'),
                'current_user_id' => @$cuser->id,
                'loginUser' => @$cuser,
                'current_slug' => $currentnav,
                'app_env' => env('APP_ENV', 'local'),
            ]);
        });
    }
}
