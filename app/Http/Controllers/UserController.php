<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use App\Modules\Traits\HandleCrud;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new User;
        $this->db_table = $this->model->getTable();
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = ['sorting' => false];
        unset($tableCols['this.id']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.email_verified_at']);
        unset($tableCols['this.password']);
        unset($tableCols['this.remember_token']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.id as id');
        $cols[] = DB::raw('this.name as name');
        $cols[] = DB::raw('this.email as email');
        $cols[] = DB::raw('this.created_at as created_at');
        $cols[] = DB::raw('u.name as created_by');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        return $eloquentObj;
    }

    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));

        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('name', 'LIKE', $search_keyword);
                $q = $q->orWhere('email', 'LIKE', $search_keyword);
            });
        }
        return $eloquentObj;
    }

    public
    function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'custom':
                break;
            default:
                dd($request->all());
                break;
        }
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }

}
