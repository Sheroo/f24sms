<!-- imageupload.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Laravel Multiple Images Upload Using Dropzone</title>
    <meta name="_token" content="{{csrf_token()}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
</head>
<body>
<div class="container">

    <h3 class="jumbotron">Laravel Multiple Images Upload Using Dropzone</h3>
{{--    <form method="post" action="{{url('image/upload/store')}}" enctype="multipart/form-data"--}}
{{--          class="dropzone" id="dropzone">--}}
{{--        @csrf--}}
{{--    </form>--}}
    <form method="post" action="{{url('/upload')}}" enctype="multipart/form-data"
          class="dropzone" id="dropzone">
        @csrf
        <input type="file" name="img" />
        <input type="submit" name="submit" value="Submit" />

    </form>
</body>
</html>
