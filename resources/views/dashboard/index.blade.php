@extends('layouts.master')

@section('content')

    <div class="header">
        <h2><strong>Dashboard</strong></h2>
    </div>

    <div class="row">

        <div class="portlets">
            <div class="{{$controller_slug}}-table-wrapper items-table-wrapper"
                 id="{{$controller_slug.'-table-wrapper'}}"
                 data-slug="{{$controller_slug}}">
                @include($controller_slug.'._list')
            </div>
        </div>
    </div>

@endsection

@section('modals')
@append

@section('scripts')
@append
