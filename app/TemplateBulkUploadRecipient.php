<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateBulkUploadRecipient extends Model
{
    protected $table = 'template_bulk_upload_recipient';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
