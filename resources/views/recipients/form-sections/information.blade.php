<div class="item-form-section message_information-section"
     data-slug="{{$controller_slug}}"
     data-section="message_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Message Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">First Name</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="first_name"
                       value="{{@$itemObj->first_name}}"
                       type="text"
                       placeholder="First Name"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Last Name</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="last_name"
                       value="{{@$itemObj->last_name}}"
                       type="text"
                       placeholder="Last Name"/>
            </div>
        </div>


        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-validate-email"
                       name="email"
                       value="{{@$itemObj->email}}"
                       type="text"
                       placeholder="Email"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Phone</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="phone"
                       value="{{@$itemObj->phone}}"
                       type="text"
                       placeholder="Phone"/>
            </div>
        </div>

    </div>
</div>
