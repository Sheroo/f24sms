<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchedulerDeliveryReport extends Model
{
    protected $table = 'scheduler_delivery_report';
    protected $fillable = ['scheduler_history_id', 'recipient_id', 'twilio_notification_id','notification_received'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

    }

    public function automation_history()
    {
        return $this->belongsTo('\App\SchedulerRunHistory', 'scheduler_history_id', 'id');
    }

    public function recipient()
    {
        return $this->belongsTo('\App\Recipient', 'recipient_id', 'id');
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
