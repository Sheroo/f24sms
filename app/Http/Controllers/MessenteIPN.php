<?php

namespace App\Http\Controllers;

use App\Recipient;
use App\SchedulerDeliveryReport;
use App\SchedulerRunHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MessenteIPN extends Controller
{

    public function MessenteIPN(Request $request)
    {
        Log::info('Received IPN for MessenteIPN ');
//        $rawBody = $request->getContent();
//
//        $data = [];
//        parse_str($rawBody, $data);
//        print_r($data);
        Log::info($request->getContent());
        exit;
/*
        array (
            'to' => '+923017420727',
            'sender' => 'F24',
            'status' => 'NACK',
            'omnimessage_id' => 'be8b9c34-38c0-4740-8a76-64ba71e3c177',
            'message_id' => '34cb7c20-d296-4df7-93cc-228a9b29a200',
            'channel' => 'sms',
            'err' => 6,
            'error' => 'Too many messages sent to this number during 1 hour',
            'timestamp' => '2020-09-24T12:39:58.872947',
        )
*/
//
//        {
//            "to": "+923017420727",
//  "sender": "F24",
//  "status": "ACK",
//  "omnimessage_id": "6e210495-b276-4e67-a902-255d03e15433",
//  "message_id": "5028f6a9-ffc1-40f2-83f8-431beb57fcdb",
//  "channel": "sms",
//  "err": 0,
//  "error": "The message was accepted by the operator",
//  "timestamp": "2020-09-24T10:41:44.468297"
//}
//
//        {
//            "to": "+923017420727",
//  "sender": "F24",
//  "status": "DELIVRD",
//  "omnimessage_id": "6e210495-b276-4e67-a902-255d03e15433",
//  "message_id": "5028f6a9-ffc1-40f2-83f8-431beb57fcdb",
//  "channel": "sms",
//  "err": 0,
//  "error": null,
//  "timestamp": "2020-09-24T10:41:49.811023"
//}




        if (isset($data['ID'])) {
            Log::info('Received IPN for MessenteIPN '.$data['ID']);
            $schedulerHistoryId = @$data['ID'];
            $ref = @$data['REF'];
            $number = @$data['RCV'];
            $state = @$data['STATE'];
            if (strtolower('DELIVRD') == strtolower($state)) {
                $state = 'delivered';
            }
            $deliveryTime = '';
            try {
                $deliveryTime = Carbon::createFromFormat('Y.m.d H:i:s', $data['DELIVERYTIME']);
            } catch (\Exception $e) {

            }
            $this->updateRecipientStatusFromMessente($schedulerHistoryId, $number, $ref, $state);

        } else {
            Log::info('Linkmobility IPN is empty ');
            Log::info($rawBody);
        }
    }

    function updateRecipientStatusFromMessente($schedulerHistoryId, $number, $ref, $status = '')
    {

        $history = SchedulerRunHistory::where('id', $schedulerHistoryId)->first();
        if ($history) {
            $automation = $history->automation;
            if ($automation) {
                $recipientPhone = $number;
                $recipient = Recipient::where(
                    [
                        'phone' => $recipientPhone,
                        'list_id' => $automation->list_id,
                    ]
                )->first();
                if ($recipient) {

                    $report = SchedulerDeliveryReport::where([
                        'scheduler_history_id' => $schedulerHistoryId,
                        'recipient_id' => $recipient->id,
                    ])->first();

                    if ($report) {
                        $report->sms_sid = $ref;
                        $report->message_sid = $ref;
                        $report->sms_status = strtolower($status);
                        $report->message_status = strtolower($status);
                        $report->notification_received = 1;
                        $report->notification_received_time = Carbon::now()->toDateTimeString();
                        $report->save();
                    } else {
                        Log::info('SchedulerDeliveryReport not found for recipient' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                    }
                } else {
                    Log::info('Recipient not found ' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                }
            } else {
                Log::info('Automation rule not found for schedulerHistoryId ' . $schedulerHistoryId);
            }
        } else {
            Log::info('schedulerHistoryId not found ' . $schedulerHistoryId);
        }
    }


}
