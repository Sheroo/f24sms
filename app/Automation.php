<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Automation extends Model
{
    use SoftDeletes;
    protected $table = 'scheduler';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function mylist()
    {
        return $this->belongsTo('App\MyList', 'list_id', 'id');
    }

    public function message()
    {
        return $this->belongsTo('App\Message', 'message_id', 'id');
    }
}
