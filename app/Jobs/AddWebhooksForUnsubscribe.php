<?php

namespace App\Jobs;

use App\MailChimpContact;
use DrewM\MailChimp\MailChimp;
use DrewM\MailChimp\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class AddWebhooksForUnsubscribe implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Rune account api key
        $apiKey = env('MAILCHIMP_KEY');
        $mailChimp = new MailChimp($apiKey);
        $lists = $mailChimp->get('lists', ['count' => 100]);
        if (isset($lists['lists'])) {

            foreach ($lists['lists'] as $listCol => $listValue) {
                $list_id = $listValue['id'];
                try {


                    echo "List Id " . $list_id . PHP_EOL;
                    echo "Web List Id " . $listValue['web_id'] . PHP_EOL;
                    echo "lists/$list_id/webhooks/" . PHP_EOL;
                    $result = $mailChimp->post("lists/$list_id/webhooks", [
                        'events' => ['unsubscribe' => true],
                        'sources' => ['user' => true, 'admin' => true, 'api' => true,],
                        'url' => 'https://f-365.net/mailchimp/unsub/email/webhook/' . $list_id,
                    ]);

                    if (isset($result['id'])) {
                        echo "Id <" . $result['id'] . "> " . PHP_EOL;
                        echo "Id <" . $result['url'] . "> " . PHP_EOL;
                        echo "Id <" . json_encode($result['events']) . "> " . PHP_EOL;
                    } else {
                        $result = $mailChimp->put("lists/$list_id/webhooks/" . $result[''], [
                            'events' => ['unsubscribe' => true],
                            'sources' => ['user' => true, 'admin' => true, 'api' => true,],
                            'url' => 'https://f-365.net/mailchimp/unsub/email/webhook/' . $list_id,
                        ]);
                        p_rr($result);
                        echo "Webhook for " . $list_id . " not created" . PHP_EOL;
                    }

//                    $webhooks = $mailChimp->get('lists/' . $list_id . '/webhooks', ['count' => 100]);
//                    foreach ($webhooks as $webhk) {
//                        foreach ($webhk as $webhook) {
//
//                            p_rr($webhook);
//                            echo "URL is "."lists/$list_id/webhooks/" . $webhook['id'].PHP_EOL;
//                            $result = $mailChimp->patch("lists/$list_id/webhooks/" . $webhook['id'], [
//                                'events' => ['unsubscribe' => true],
//                                'sources' => ['user' => true, 'admin' => true, 'api' => true,],
//                            ]);
//                            p_rr($result);
//                            echo "Webhook for " . $list_id . " is  updated" . PHP_EOL;
//                        }
//                    }

                } catch (\Exception $e) {
                    Log::info($e->getMessage());
                    Log::info('---------------------------');
                }

            }
        }
    }
}
