<?php

namespace App\Jobs;

use App\Recipient;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateRecipientsForValidEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue(Q_RULE_SYNC);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->updateRecipientsEmails();
    }


    public function updateRecipientsEmails()
    {

        Log::info('Getting unsubscribed user from mailwizz');
        $blackEmails = DB::connection("mailwizz")->table('mw_list_subscriber')
            ->where('status', '!=', 'confirmed')
            ->whereNull('sync_with_f24sms')
            ->pluck('email')
            ->toArray();

        $recipients = Recipient::wherein('email', $blackEmails)
            ->get();

        $ids = [];
        foreach ($recipients as $recip) {
            $recip->active = 0;
            $recip->unsubscribed_date = Carbon::now()->toDateTimeString();
            $recip->save();
            $ids[] = $recip->email;
        }

        Log::info('Updating user in mailwizz');
        $blackEmails = DB::connection('mailwizz')->table('mw_list_subscriber')
            ->where('status', '!=', 'confirmed')//this check is required because there are duplicated email for unsub and confirmed, so we don't want to mark 'yes' for confirmed
            ->whereIn('email', $ids)
            ->update(['sync_with_f24sms' => 1]);
    }


}
