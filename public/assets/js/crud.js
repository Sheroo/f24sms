function reset_item_form(slug) {
    jQuery('.te-item-form[data-slug=' + slug + ']').reset_form().find('input').each(function (index, elem) {
        jQuery(elem).val('');
    });
    if (window['reset_item_form__' + slug] !== undefined) {
        window['reset_item_form__' + slug]();
    }
    validate_all_sections(slug, false);
}

function _get_serialized_item_form_data(slug) {

    var serialized_data = jQuery('.te-item-form[data-slug=' + slug + ']').serializeToObject();
    serialized_data['itemSearchFormData'] = jQuery('.te-search-form[data-slug=' + slug + ']').serializeToObject();
    if (window['_get_serialized_item_form_data__' + slug] !== undefined) {
        serialized_data = window['_get_serialized_item_form_data__' + slug](serialized_data);
    }
    return serialized_data;

}

function refresh_item_form_controls() {
    make_datepicker();
}

function validate_all_sections(slug, show_errors) {

    if (show_errors === undefined) {
        show_errors = false;
    }

    var item_form_panel = jQuery('.item-form-panel[data-slug=' + slug + ']');

    var valid = true;
    item_form_panel.find('.item-form-section').each(function (index, sec) {
        var section_name = jQuery(sec).data('section');
        if (window['validate_form_' + slug + '_section_' + section_name] !== undefined) {
            if (!window['validate_form_' + slug + '_section_' + section_name](slug, section_name)) {
                valid = false;
            }
        } else {
            if (!jQuery(sec).validate_form_section(show_errors)) {
                valid = false;
            }
        }
    });

    if (valid) {
        item_form_panel.find('.item-form-action-btn[data-action=save]').addClass('active');
    } else {
        item_form_panel.find('.item-form-action-btn[data-action=save]').removeClass('active');
    }

    return valid;
}


function validate_email($email) {
    if ($email == '') return false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($email)) {
        return false;
    } else {
        return true;
    }
}

function validate_password(password) {

    if (password == '') {
        return false;
    } else if (password.length < 4) {
        return false;
    }
    return true;

}

jQuery(document).ready(function () {

    jQuery('body').on('change input', '.item-form-panel .item-form-section .te-input', function (e) {
        e.preventDefault();
        var itsForm = jQuery(this).parents('.te-item-form');
        var itsSlug = itsForm.data('slug');
        validate_all_sections(itsSlug);
    });

    jQuery('body').on('blur', '.item-form-panel .item-form-section .te-input.datepicker', function (e) {
        e.preventDefault();
        var itsForm = jQuery(this).parents('.te-item-form');
        var itsSlug = itsForm.data('slug');
        validate_all_sections(itsSlug);
    });
});

jQuery.fn.extend({

    reset_form: function () {
        return this.each(function () {
            $(this).find('.bs-alert').remove();
            $(this).find('.help-block').remove();
            $(this).find('.form-group').removeClass('has-error');
            $(this)[0].reset();
        });
    },
    validate_form_section: function (show_errors) {

        if (show_errors === undefined) {
            show_errors = false;
        }

        var thisSection = jQuery(this);

        var okey = true;

        jQuery(this).find('.form-group').each(function (index, element) {
            jQuery(element).removeClass('has-error').find('.help-block').each(function (ind, err) {
                jQuery(err).remove();
            });
        });

        jQuery.each(jQuery(this).find('input, textarea, select'), function (index, element) {

            if (!jQuery(element).is(':disabled')) {

                var elementHidden = jQuery(element).parents('.form-group').css('display') == 'none';
                if (!elementHidden) {

                    var inputValue = jQuery(element).val();

                    if (jQuery(element).hasClass('te-required')) {


                        // if (element.tagName == 'SELECT' || element.tagName == 'select') {
                        //     inputValue = element.options[element.selectedIndex].value;
                        // inputValue = jQuery(element).find('option:selected').val();
                        // }
                        // console.log(element.tagName, 'Tag of ' + element.name);
                        // console.log(inputValue, 'Value of ' + element.name);
                        if (inputValue == '') {
                            if (show_errors) {
                                var error = '<span class="help-block">Requied field</span>';
                                jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                jQuery(element).focus();
                            }
                            okey = false;

                        } else {

                            if (element.name == 'credit_card_expiration_month' || element.name == 'credit_card_expiration_year') {

                                var d = new Date();
                                var month = d.getMonth() + 1;
                                var year = d.getFullYear();

                                var cc_month = parseInt(jQuery('#credit_card_expiration_month').val())
                                var cc_year = parseInt(jQuery('#credit_card_expiration_year').val())

                                if ((year > cc_year) || (year == cc_year && month > cc_month)) {
                                    if (show_errors) {
                                        var error = '<span class="help-block">Invalid Date</span>';
                                        jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                        jQuery(element).focus();
                                    }

                                    okey = false;
                                }
                            }
                        }
                    } else if (jQuery(element).hasClass('te-validate-email')) {
                        if (!validate_email(inputValue)) {
                            if (show_errors) {
                                var error = '<span class="help-block">Invalid Email Address.</span>';
                                jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                jQuery(element).focus();
                            }

                            okey = false;
                        }
                    } else if (jQuery(element).hasClass('te-validate-optional-email')) {
                        if (inputValue != '') {
                            if (!validate_email(inputValue)) {
                                if (show_errors) {
                                    var error = '<span class="help-block">Invalid Email Address.</span>';
                                    jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                    jQuery(element).focus();
                                }

                                okey = false;
                            }
                        }
                    } else if (jQuery(element).hasClass('te-validate-password')) {
                        if (!validate_password(inputValue)) {
                            if (show_errors) {
                                var error = '<span class="help-block">Invalid Password.</span>';
                                jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                jQuery(element).focus();
                            }

                            okey = false;
                        }
                    } else if (jQuery(element).hasClass('te-validate-confirm-password')) {
                        var confirm_pass = inputValue;
                        var pass = jQuery('#' + jQuery(element).data('match')).val();
                        if (pass != confirm_pass) {
                            if (show_errors) {
                                var error = '<span class="help-block">Password does not match.</span>';
                                jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                jQuery(element).focus();
                            }

                            okey = false;
                        }
                    } else if (jQuery(element).hasClass('te-validate-card')) {
                        jQuery('#credit_card_number').validateCreditCard(function (result) {
                            if (result.card_type == null) {
                                if (show_errors) {
                                    var error = "Invalid Card.";
                                    jQuery(element).parents('.form-group').addClass('has-error').append(error);
                                    jQuery(element).focus();
                                }

                                jQuery('#card_span').html('');
                                jQuery('#card_name').val('');
                                return;
                            }
                            if (result.length_valid) {
                                if (show_errors) {
                                    jQuery(element).parents('.form-group').addClass('has-error');
                                }
                                jQuery('#card_span').html(result.card_type.name);
                                jQuery('#card_name').val(result.card_type.name);
                            } else {
                                if (show_errors) {
                                    jQuery(this).parents('.form-group').addClass('has-error');
                                }
                                jQuery('#card_span').html('');
                                jQuery('#card_name').val('');
                            }
                        });
                    }
                }
            }
        });

        if (okey) {
            thisSection.addClass('br-ok');
        } else {
            thisSection.removeClass('br-ok');
        }

        return okey;
    }
});

jQuery(document).ready(function () {

    jQuery('body').on('submit', 'form.te-item-form', function (e) {
        e.preventDefault();
        jQuery(this).find('.item-form-action-btn[data-action=save]').click();
        return false;
    });

    jQuery('body').on('click', '.item-form-action-btn', function (e) {
        e.preventDefault();
        var action = jQuery(this).data('action');
        var slug = jQuery(this).data('slug');

        if (action == 'close') {

            reset_item_form(slug);
            jQuery('.item-form-panel[data-slug=' + slug + ']').modal('hide');

            if (window['_close_form_callback__' + slug] !== undefined) {
                window['_close_form_callback__' + slug](slug);
            }

        } else if (action == 'save') {

            if (jQuery(this).hasClass('active')) {

                var _action = prepareAjaxAction(slug);
                var serialized_data = _get_serialized_item_form_data(slug);


                if(CKEDITOR.instances.editor1 != undefined)
                {
                    var content = CKEDITOR.instances.editor1.getData();
                    serialized_data['content'] = content;

                }
                serialized_data = addAjaxActionToSerializedData(serialized_data, 'ajax_save_item');

                te_ajax(
                    _action,
                    serialized_data,
                    function (resp) {
                        //trigger script to fetch latest data for strategy_info div
                        jQuery('.item-form-panel[data-slug=' + slug + ']').modal('hide');
                        jQuery('.' + slug + '-table-wrapper').html(resp.table_html);
                        jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert('Item Saved successfully', 'success', 3);

                        if (window['_save_item_callback__' + slug] !== undefined) {
                            window['_save_item_callback__' + slug](resp, slug);
                        }

                        enable_all_js_plugins();

                    },
                    function (resp) {
                        jQuery('.item-form-panel[data-slug=' + slug + '] .modal-content').te_hide_alert().te_show_alert(resp.message, 'danger', 5);
                    },
                    function (error_resp) {
                    },
                    jQuery('.item-form-panel[data-slug=' + slug + '] .modal-content')
                );
            }
        }
    });
});

jQuery(document).ready(function () {

    jQuery('body').on('click', '.te-item-action-btn', function (e) {

        e.preventDefault();
        var action = jQuery(this).data('action');
        var slug = jQuery(this).data('slug');

        if (action == 'create') {

            reset_item_form(slug);
            jQuery('.item-form-panel[data-slug=' + slug + ']').modal({backdrop: 'static', keyboard: false});

            var _action = prepareAjaxAction(slug);

            var serialized_data = 'content=' + content;


            serialized_data = addAjaxActionToSerializedData(serialized_data, 'create_form');

            te_ajax(
                _action,
                serialized_data,
                function (success_resp) {

                    jQuery('.item-form-panel[data-slug=' + slug + '] #item-form-modal-content').html(success_resp.form_html);
                    refresh_item_form_controls();
                    jQuery('.te-item-form[data-slug=' + slug + '] .dropdown-backdrop').hide();
                    validate_all_sections(slug);
                    if (window['_create_form_callback__' + slug] !== undefined) {
                        window['_create_form_callback__' + slug](success_resp, slug);
                    }

                },
                function (fail_resp) {
                },
                function (error_resp) {
                },
                jQuery('.item-form-panel[data-slug=' + slug + '] .modal-content')
            );

        } else if (action == 'edit') {

            var $id = jQuery(this).data('id');
            reset_item_form(slug);
            jQuery('.item-form-panel[data-slug=' + slug + ']').modal({backdrop: 'static', keyboard: false});

            var _action = prepareAjaxAction(slug);
            var serialized_data = 'item_id=' + $id;
            serialized_data = addAjaxActionToSerializedData(serialized_data, 'edit_form');

            te_ajax(
                _action,
                serialized_data,
                function (success_resp) {

                    jQuery('.item-form-panel[data-slug=' + slug + '] #item-form-modal-content').html(success_resp.form_html);
                    refresh_item_form_controls();
                    jQuery('.te-item-form[data-slug=' + slug + '] .dropdown-backdrop').hide();
                    validate_all_sections(slug);
                    if (window['_edit_form_callback__' + slug] !== undefined) {
                        window['_edit_form_callback__' + slug](success_resp, slug);
                    }
                    if(slug == "automation") {
                        setTimeout(function () {
                            jQuery('.scheduled_type').change();
                        }, 10);
                    }
                },
                function (fail_resp) {
                },
                function (error_resp) {
                },
                jQuery('.item-form-panel[data-slug=' + slug + '] .modal-content')
            );

        } else if (action == 'delete') {

            var $id = jQuery(this).data('id');
            swal({
                    title: "",
                    text: "Are you sure you want to delete?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete!",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        var serialized_data = jQuery('.' + slug + '-search-form').serializeToObject();
                        serialized_data['id'] = $id;
                        serialized_data['method'] = 'delete';
                        var delete_action = prepareAjaxAction(slug);
                        serialized_data = addAjaxActionToSerializedData(serialized_data, 'delete');
                        te_ajax(
                            delete_action,
                            serialized_data,
                            function (resp) {
                                jQuery('.' + slug + '-table-wrapper').html(resp.table_html);
                                enable_all_js_plugins();
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert('Item deleted successfully', 'success', 3);

                                if (window['_delete_callback__' + slug] !== undefined) {
                                    window['_delete_callback__' + slug](resp, slug);
                                }
                            },
                            function (resp) {
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert(resp.message, 'danger');
                                enable_all_js_plugins();
                            },
                            function (error_resp) {
                            },
                            jQuery('.items-table-wrapper[data-slug=' + slug + '] .panel-content')
                        );

                    }
                }
            );

        } else if (action == 'unsubscribe') {

            var id = jQuery(this).data('id');
            swal({
                    title: "",
                    text: "Are you sure you want to unsbscribe?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, UnSubscribe!",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        var serialized_data = jQuery('.' + slug + '-search-form').serializeToObject();
                        serialized_data['item-id'] = id;
                        serialized_data['method'] = 'unsubscribe';
                        var delete_action = prepareAjaxAction(slug);
                        serialized_data = addAjaxActionToSerializedData(serialized_data, 'unsubscribe');
                        te_ajax(
                            delete_action,
                            serialized_data,
                            function (resp) {
                                jQuery('.' + slug + '-table-wrapper').html(resp.table_html);
                                enable_all_js_plugins();
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert('Item UnSubscribed successfully', 'success', 3);

                                if (window['_delete_callback__' + slug] !== undefined) {
                                    window['_delete_callback__' + slug](resp, slug);
                                }
                            },
                            function (resp) {
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert(resp.message, 'danger');
                                enable_all_js_plugins();
                            },
                            function (error_resp) {
                            },
                            jQuery('.items-table-wrapper[data-slug=' + slug + '] .panel-content')
                        );

                    }
                }
            );
        } else if (action == 'bulk_delete') {

            var id = jQuery(this).data('id');
            var ids = [];
            var select = $("input[name='select']");
            select = $.each((select), function () {
                ids.push($(this).val());
            });

            swal({
                    title: "",
                    text: "Are you sure you want to delete all checked?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete All!",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        var serialized_data = jQuery('.' + slug + '-search-form').serializeToObject();
                        serialized_data['item-id'] = id;
                        serialized_data['method'] = 'bulkDelete';
                        serialized_data['select'] = ids;

                        var delete_action = prepareAjaxAction(slug);
                        serialized_data = addAjaxActionToSerializedData(serialized_data, 'bulkDelete');
                        te_ajax(
                            delete_action,
                            serialized_data,
                            function (resp) {
                                jQuery('.' + slug + '-table-wrapper').html(resp.table_html);
                                enable_all_js_plugins();
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert('Items Deleted successfully', 'success', 3);

                                if (window['_delete_callback__' + slug] !== undefined) {
                                    window['_delete_callback__' + slug](resp, slug);
                                }
                            },
                            function (resp) {
                                jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert(resp.message, 'danger');
                                enable_all_js_plugins();
                            },
                            function (error_resp) {
                            },
                            jQuery('.items-table-wrapper[data-slug=' + slug + '] .panel-content')
                        );

                    }
                }
            );
        } else {

            var $custom_item_action_function = 'custom_item_action_' + slug + '_' + action;
            if (window[$custom_item_action_function] !== undefined) {
                window[$custom_item_action_function](this);
            } else {
                swal('Error', 'function ' + $custom_item_action_function + "() NOT FOUND.");
            }

        }
    });

});


function enable_all_js_plugins() {
}

jQuery(document).ready(function () {

    jQuery(document).on('submit', '.te-search-form', function (e) {

        e.preventDefault();

        var slug = jQuery(this).data('slug');
        refresh_items_data(slug);

        return false;

    });

    jQuery(document).on('change', '.te-search-form .select-picker', function (e) {

        e.preventDefault();

        var slug = jQuery(this).parents('.te-search-form').attr('data-slug');
        refresh_items_data(slug);

        return false;

    });

    jQuery(document).on('change', 'select.te-per-page', function (e) {

        e.preventDefault();
        var slug = jQuery(this).data('slug');
        var searchForm = jQuery('.te-search-form[data-slug=' + slug + ']');
        searchForm.find('.page_no').val(1);
        searchForm.find('.per_page').val(jQuery(this).val());
        refresh_items_data(slug);

    });

    jQuery(document).on('click', '.order_by', function (e) {

        e.preventDefault();
        var slug = jQuery(this).parents('table.te-item-table').data('slug');

        if (jQuery(this).parent().find('th.doing-sort').length == 0) {

            jQuery(this).addClass('doing-sort');

            var order_by = jQuery(this).data('order_by');
            var class_name = jQuery(this).data('class');
            var order = jQuery(this).hasClass('sorting_desc') ? 'asc' : 'desc';

            var this_search_form = jQuery('.te-search-form[data-slug=' + slug + ']');

            this_search_form.find('.order_by').val(order_by);
            this_search_form.find('.order').val(order);
            this_search_form.find('.page_no').val(1);
            refresh_items_data(slug);
        }
    });

    jQuery(document).on('click', '.te-pagination-link', function (e) {
        e.preventDefault();
        var slug = jQuery(this).data('slug');

        if (!jQuery(this).hasClass('te-current-page')) {
            var page_no = jQuery(this).data('page_no');
            jQuery('.te-search-form[data-slug=' + slug + ']').find('.page_no').val(page_no);
            refresh_items_data(slug);
        }
    });

});

function refresh_items_data(slug) {
    var its_form = jQuery('.te-search-form[data-slug=' + slug + ']');
    // console.log(its_form);
    var serialized_data = its_form.serializeToObject();

    var page_no = its_form.find('.page_no').val();
    var action = prepareAjaxAction(slug);

    action = appendPageNoToAjaxAction(action, page_no);
    serialized_data = addAjaxActionToSerializedData(serialized_data, 'refresh_table');


    te_ajax(
        action,
        serialized_data,
        function (resp) {
            jQuery('.items-table-wrapper[data-slug=' + slug + ']').html(resp.table_html);
            if (window['_refresh_table_callback__' + slug] !== undefined) {
                window['_refresh_table_callback__' + slug](resp, slug);
            }
            enable_all_js_plugins();
        },
        function (resp) {
        },
        function (resp) {
        },
        jQuery('.items-table-wrapper[data-slug=' + slug + '] .panel-content')
    );
}

function prepareAjaxAction(controller_slug) {

    return controller_slug;
}

function appendPageNoToAjaxAction(action, page_no) {

    page_no = isNaN(page_no) ? '' : page_no;
    if (page_no != '') {
        action = action + '/?page=' + page_no;
    }
    return action;
}

function addAjaxActionToSerializedData(serializedData, action) {
    if (typeof serializedData == 'object') {
        serializedData['ajax_action'] = action;
    } else {
        serializedData = 'ajax_action=' + action + '&' + serializedData;
    }
    return serializedData;
}


