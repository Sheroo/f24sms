@if($csvIsValid && count($uploadedEntitiesValidation) > 0)
    <ul class="nav customtab nav-tabs" role="tablist">
        <?php $tabsCount = 0;?>
        @foreach($uploadedEntitiesValidation as $entity => $entityData)
            @if(count($entityData) > 0)
                <li role="presentation" class="{{$tabsCount++ == 0 ? "active" : ''}}">
                    <a href="#{{$entity}}-tab-content"
                       aria-controls="{{$entity}}-tab-content"
                       role="tab"
                       data-toggle="tab"
                       aria-expanded="false">{{bulkUploadEntityNamesPlural($entity)}}
                        ({{isset($entities_count[$entity]) ? $entities_count[$entity] : count($entityData)}})</a>
                </li>
            @endif
        @endforeach
    </ul>
    <?php $tabsCount = 0;?>

    <div class="tab-content {{$controller_slug}}">
        @foreach($uploadedEntitiesValidation as $entity => $entityData)
            @if(count($entityData) > 0)
                <?php

                $campaignCols = bulkUploadEntityColsEmptyValCheck($entity, $controller_slug);
                if ($entity == 'account') {
                }

                ?>

                <div role="tabpanel" class="tab-pane fade in {{$tabsCount++ == 0 ? "active" : ''}}"
                     id="{{$entity}}-tab-content">
                    <div class="bookmarks-table-header table-responsive">
                        <table class="table table-striped bulk-upload-data-summary-table">
                            <thead>
                            <tr>
                                @foreach($campaignCols as $col)
                                    <th>{{$col}}</th>
                                @endforeach
                                <th style="width:120px;">Upload Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $itemIndex = 0;
                            $row_count_limit = env('BULK_UPLOAD_SUMMARY_ROW_LIMIT', 200);

                            foreach($entityData as $item ) {
                            $itemIndex++;
                            ?>

                            <tr style="{{isset($item['Error']) && $item['Error']==1 ? 'background-color: rgb(255, 237, 138)' : ''}}">

                                @foreach($campaignCols as $col)
                                    @if($col=='Campaign Labels' && isset($item[$col]))
                                        <?php
                                        $labels_arr = str_getcsv($item[$col]);
                                        $html = '';
                                        $html .= '<td class="campaign-custom_labels-col">';
                                        $html .= '<div class="te-tags ">';
                                        foreach ($labels_arr as $label) {
                                            $label = trim($label);
                                            if (empty($label)) {
                                                continue;
                                            }
                                            $lb_n_colr = explode(":", $label);
                                            $c = "#999999";
                                            if (isset($lb_n_colr[1]) && !empty($lb_n_colr[1])) {
                                                $c = $lb_n_colr[1];
                                            }
                                            $html .= '<div class="colored-tags te-tag no-wrap">';
                                            $html .= $lb_n_colr[0];
                                            $html .= '<span class="tag-style" style="background:' . $c . '"></span>';
                                            $html .= '</div>';
                                        }
                                        $html .= '</div>';
                                        $html .= '</td>';
                                        echo $html;
                                        ?>
                                    @else
                                        <td>{{isset($item[$col]) ? ($item[$col]) : ''}}</td>
                                    @endif
                                @endforeach
                                <td class="upload-status">
                                    <?php
                                    $upload_status = $item['Upload Status'];
                                    $upload_status = is_array($upload_status) ? $upload_status : json_decode($upload_status, true);
                                    ?>
                                    @if(count($upload_status) > 0)
                                        @if(count($upload_status) > 1)
                                            <ul class="upload-status-ul">
                                                @foreach($upload_status as $error)
                                                    <li>{!! $error !!}</li>
                                                @endforeach
                                            </ul>
                                        @else
                                            @foreach($upload_status as $error)
                                                {!! $error !!}
                                            @endforeach
                                        @endif
                                    @else
                                        Upload successful
                                    @endif
                                </td>
                            </tr>
                            <?php } ?>
                            <?php
                            $total_entities = isset($entities_count[$entity]) ? $entities_count[$entity] : count($entityData);
                            $entityDataCount = count($entityData);
                            if ($total_entities > $entityDataCount && $entity != 'account') {
                            ?>

                            <tr>
                                <td colspan="<?php echo(count($campaignCols) + 1)?>">
                                    <em>Displaying only
                                        first <?php echo $row_count_limit;?> <?php echo bulkUploadEntityNamesPlural($entity)?></em>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        @endforeach

    </div>

@else

    <p>Your CSV file does not contain the required columns.You can download the Sample CSV file <a
            href="{{te_bulk_upload_csv_url($controller_slug)}}">here</a> for guide. </p>
    <?php if(isset($csv_parse_error) && !empty($csv_parse_error)) { ?>
    {{$csv_parse_error}}
    <?php } else { ?>
    <p>Entities create operation should be in following order</p>
    <p>Following columns are required to be in CSV file.</p>
    <table class="table table-bordered">
        <tr>
            <th>Required Columns</th>
            <th>Uploaded CSV Columns</th>
            <th>Missing Columns in CSV File</th>
            <th>Extra Columns in CSV File</th>
        </tr>
        <tr>
            <td style="vertical-align: top">
                <ol style="padding-left: 0; list-style-position: inside;">
                    <?php
                    foreach($uploadedEntitiesValidation['requiredColumn'] as $column){
                    ?>
                    <li>{{$column}}</li>
                    <?php
                    }
                    ?>
                </ol>
            </td>
            <td style="vertical-align: top">
                <ol style="padding-left: 0; list-style-position: inside;">
                    <?php
                    foreach($uploadedEntitiesValidation['userFileColumn'] as $column){
                    ?>
                    <li>{{$column}}</li>
                    <?php
                    }
                    ?>
                </ol>
            </td>
            <td style="vertical-align: top">
                <ol style="padding-left: 0; list-style-position: inside;">
                    <?php
                    if(isset($uploadedEntitiesValidation['missingCols'])){
                    foreach($uploadedEntitiesValidation['missingCols'] as $column){
                    ?>
                    <li>{{$column}}</li>
                    <?php
                    }
                    }
                    ?>
                </ol>
            </td>
            <td style="vertical-align: top">
                <ol style="padding-left: 0; list-style-position: inside;">
                    <?php
                    if(isset($uploadedEntitiesValidation['extraCols'])){
                    foreach($uploadedEntitiesValidation['extraCols'] as $column){
                    ?>
                    <li>{{$column}}</li>
                    <?php
                    }
                    }
                    ?>
                </ol>
            </td>
        </tr>
    </table>
    <?php } ?>

@endif
