<?php

namespace App\Modules\Traits;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;


use App\MyList;

trait HandleCrud
{
    public $controller_slug;
    public $per_page = 10;
    public $per_page_list = [10, 25, 50, 100];
    public $default_per_page = 10;
    public $default_order_by = 'desc';
    public $default_order = 'this.id';
    public $model;
    public $db_table = '';
    public $run_main_table_query = true;

    public function setDefaultCrudConfiguration()
    {
        $this->controller_slug = \Illuminate\Support\Facades\Request::segment(te_segment('current_slug'));
        $this->per_page = 10;
        $this->default_per_page = 10;
        $this->per_page_list = [10, 25, 50, 100];
        $this->default_order = 'desc';
        $this->default_order_by = 'this.id';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $viewData = $this->getItemsData($request, $ajax = false);
        $viewData['breadcrumbs'] = '';
        return view($this->controller_slug . '.index', $viewData);
    }

    public function getItemsData(Request $request, $ajax = true)
    {

        list($order, $order_by) = $this->getOrderByParams($request);
        list($page_no, $per_page) = $this->getPaginationParams($request);

        $viewData = [];
        $logInUser = Auth::User();

        $query = DB::table(DB::raw($this->db_table . ' as this'));

        if (!empty($order_by)) {
            $query = $query->orderBy($order_by, $order);
        }

        $query = $this->applyJoins($request, $query);
        $query = $this->applyGroupBy($request, $query);
        $query = $this->selectColumns($request, $query);
        $search_keyword = trim($request->get('search_keyword', ''));

        $query = $this->searchItemsDataByKeyword($request, $query);
        $queryRequired = $request->get('queryRequired');

        if ($queryRequired) {
            $viewData['query'] = convert_elequent_to_sql_query($query);
            $viewData['queryObj'] = $query;
            return $viewData;
        }


        $count = 0;
        if ($this->run_main_table_query) {


            if (stripos(convert_elequent_to_sql_query($query), 'group by') !== false) {

                $tempQuery = "SELECT count(*) as total from (" . convert_elequent_to_sql_query($query) . ") temp";
                $count = @DB::select($tempQuery)[0]->total;
            } else {

                $count = $query->count();
            }
        }


        if ($this->run_main_table_query) {

            $viewData['items'] = $query->simplePaginate($per_page, $columns = ['*'], $pageName = 'page', $page_no);
        } else {

            $viewData['items'] = new Paginator([],$per_page ,  $page_no );
        }

        $viewData['tableColumns'] = $this->tableColumns();
        $viewData['search_keyword'] = $search_keyword;
        $viewData['order_by'] = $order_by;
        $viewData['order'] = $order;
        $viewData['per_page'] = $per_page;
        $viewData['perpagelist'] = $this->per_page_list;
        $viewData['loginUser'] = $logInUser;
        $viewData['controller_slug'] = $this->controller_slug;
        $viewData['totalRows'] = $count;

        $viewData = $this->appendViewDataInGetItemData($request, $viewData);


        if ($ajax) {

            $response['tableColumns'] = $this->tableColumns();
            $response['search_keyword'] = $search_keyword;
            $response['order_by'] = $order_by;
            $response['order'] = $order;
            $response['per_page'] = $per_page;
            $response['perpagelist'] = $this->per_page_list;
            $response['loginUser'] = $logInUser;
            $response['controller_slug'] = $this->controller_slug;
            $response['table_html'] = (String)view($this->controller_slug . '._list')->with($viewData);
            $response['status'] = 'success';
            return $response;

        } else {

            return $viewData;

        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public
    function getOrderByParams(
        Request $request
    )
    {
        $order = $request->get('order');
        $order_by = $request->get('order_by');

        //if no order provided then order desc
        $order = empty($order) ? $this->default_order : $order;
        //if no order by provided then Name by default
        $order_by = empty($order_by) ? $this->default_order_by : $order_by;

        return array($order, $order_by);
    }

    /**
     * @param Request $request
     * @return array
     */
    public
    function getPaginationParams(
        Request $request
    )
    {
        $page_no = $request->get('page_no');
        $page_no = empty($page_no) ? 1 : $page_no;
        $per_page = $request->get('per_page');
        $per_page = is_null($per_page) || empty($per_page) ? $this->default_per_page : $per_page;

        return array($page_no, $per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function applyGroupBy(Request $request, $eloquentObj)
    {
        return $eloquentObj;
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = ['sorting' => false];
        return $tableCols;
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        //
    }

    public
    function ajax_get_items(
        Request $request
    )
    {
        $response = $this->getItemsData($request);
        return $response;
    }

    public
    function update(
        Request $request
    )
    {
        $item_form_data = Input::all();
        $response = [];
        $response['status'] = 'fail';

        return $response;
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|Response
     */
    public
    function ajaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'refresh_table':
                return $this->getItemsView($request);
                break;
            case 'ajax_save_item':
                return $this->store($request);
                break;
            case 'edit_form':
            case 'create_form':
                return $this->edit_form($request);
                break;
            case 'inline_update':
                return $this->inline_update($request);
                break;
            case 'bulkDelete':
                return $this->bulkDelete($request);
                break;
            case 'delete':
                return $this->destroy($request);
                break;
            default:
                return $this->customAjaxActions($request);
                break;
        }
    }

    public function getItemsView(Request $request)
    {

        $viewData = $this->getItemsData($request, $ajax = false);
        $userData['loginUser'] = Auth::user();
        $viewData['breadcrumbs'] = '';
        $viewData['table_html'] = (String)view($this->controller_slug . '._list', $viewData + $userData);

        $viewData['status'] = 'success';

        return Response::json($viewData);
    }

    /**
     * Store a newly created resource in storage.
     * Update an already existing resource in database
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response| array
     */
    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
//        dd($item_form_data);

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);


        return $response;
    }

    public function _prepare_item_eloquent_obj($itemEloquentObj, $data)
    {
        $itemDBCols = $itemEloquentObj->getTableColumns();
        foreach ($data as $key => $value) {
            if (in_array($key, $itemDBCols)) {
                if ($key == 'password') {
                    $value = Hash::make($value);
                }
                $itemEloquentObj->$key = $value;
            }
        }
        return $itemEloquentObj;
    }

    /**
     * called after store function is complete
     * @return \Illuminate\Http\Response| array
     */
    public function _post_store_action(Request $request, $itemEloquentObj, &$response)
    {

    }

    public function edit_form(Request $request)
    {
        $item_id = $request->get("item_id", 0);
        $itemEloquent = clone($this->model);
        $item = $itemEloquent->where('id', $item_id)->first();
        $formData = [];
        $response = [];
        $formData['itemObj'] = $item;
        $formData['form_action'] = 'edit';
        $formData['item_id'] = $item_id;
        $formData['loginUser'] = Auth::User();
        $formData['controller_slug'] = $this->controller_slug;

        $this->appendCreateEditFormData($request, $item, $formData);
        $response['form_html'] = (String)view($this->controller_slug . '._form')->with($formData);
        $response['status'] = 'success';

        return $response;
    }

    public function inline_update(Request $request)
    {

        $status = '';
        $post_data = Input::all();
        $itemEloquent = clone($this->model);
        $itemObj = $itemEloquent->where('id', $post_data['itemId'])->first();

        if ($itemObj) {

            if (isset($post_data['status'])) {
                $itemObj->status = $post_data['status'];
                if ($post_data['status'] == 1) {
                    $status = 'Enable';
                } else {
                    $status = 'Pause';
                }
            }

            $itemObj->save();
        }

        $response = $this->getItemsData($request);
        $response['state'] = $status;
        $response['message'] = 'Item updated successfully';

        return $response;
    }

    /**
     * this function will delete biddingRules in bulk
     * @return Response | array
     */
    public function bulkDelete(Request $request)
    {
        $itemIds = Input::all();
        $response = [];
        if (isset($itemIds['itemid']) && count($itemIds['itemid']) > 0) {
            $itemEloquent = clone($this->model);
            $itemEloquent->destroy($itemIds['itemid']);
            $response['message'] = 'Items deleted successfully';
        } else {
            $response['message'] = 'Please select the item(s) to delete';
        }

        $response = $this->getItemsData($request, true);

        return $response;
    }

    public
    function destroy(
        Request $request
    )
    {
        $response = [];
        $response['status'] = 'fail';
        $item_id = Input::get('id');
        $itemEloquent = clone($this->model);

        $item = $itemEloquent->where('id', $item_id)->first();
        if ($item) {
            $item->delete();
        }

        $response = $this->getItemsData($request, true);
        return $response;

    }

}
