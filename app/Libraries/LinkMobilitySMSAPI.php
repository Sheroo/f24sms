<?php

namespace App\Libraries;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class LinkMobilitySMSAPI
{

    protected $userName;
    protected $password;
    protected $smsGateId;
    protected $sender;
    static $baseURL = 'https://xml.pswin.com';


    public function __construct($userName, $password, $sender, $smsGateId = '')
    {
        $this->userName = $userName;
        $this->password = $password;
        $this->sender = $sender;
        $this->smsgateId = $smsGateId;

    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSMSGateId()
    {
        return $this->smsgateId;
    }

    /**
     * @param mixed $gateId
     */
    public function setGateId($gateId): void
    {
        $this->gateId = $gateId;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $gateId
     */
    public function setSender($sender): void
    {
        $this->sender = $sender;
    }

    public static function getSMSAPIObj()
    {
        $userName = env('LINK_USERNAME');
        $password = env('LINK_PASSWORD');
        $sender = env('LINK_SENDER');
        $smsGateId = env('LINK_SMSGATEWAYID');


        return new LinkMobilitySMSAPI($userName, $password, $sender, $smsGateId);
    }

    private function createXML($numbers, $textContent,$msgid,$unsub_content, $messageId,$ids)
    {

        $messages = [];
        if (is_null($unsub_content)){
            $unsub_content = '';
        }


        foreach ($numbers as $i => $number) {
            $id = $ids[$i];
            $url = " \nhttps://f-365.net/{$id}/{$msgid}";
            $unsub_content = "\n".$unsub_content;
            $contentToSend = $textContent .  $url . $unsub_content;
            $contentToSend = nl2br($contentToSend);
            $messages[] = "<MSG><ID>" . ($messageId) . "</ID><TEXT>" . $contentToSend . "</TEXT><SND>" . $this->getSender() . "</SND><RCV>" . trim($number, '+') . "</RCV></MSG>";
        }

        $finalXML = "<?xml version=\"1.0\"?><SESSION><CLIENT>" . $this->userName . "</CLIENT><PW>" . $this->password . "</PW><SD>".$messageId."</SD><MSGLST> " . implode(" ", $messages) . " </MSGLST></SESSION>";

        return $finalXML;
    }

    /*
     * $messageId of runHistory id so we can retreive it in delivery report
     */
    public function sendSMS($numbers, $textContent,$msgid,$unsub_content, $messageId,$ids)
    {

        $xml = $this->createXML($numbers, $textContent,$msgid,$unsub_content, $messageId,$ids);
        $requestMethod = 'POST';
        $URL = self::$baseURL;
        $client = new Client();
        $ret = [];
        try {
            $response = $client->post(
                $URL,
                [
                    'headers' => [
                        'Content-Type' => 'application/xml;charset=UTF-8'
                    ],
                    'body' => $xml,
                    //'debug' => true,
                ]
            );
            $resBody = $response->getBody();
            // Load the XML
            $xmlResponse = simplexml_load_string($resBody);
            // JSON encode the XML, and then JSON decode to an array.
            $responseArray = json_decode(json_encode($xmlResponse), true);

            $ret = [];
            $succeeded = [];
            $failed = [];

            $ret['success'] = 0;
            if (isset($responseArray['LOGON'])) {
                if ($responseArray['LOGON'] == 'OK') {
                    if (isset($responseArray['MSGLST'])) {
                        foreach ($responseArray['MSGLST'] as $msg) {
                            $messageId = @$msg['ID'];
                            $messageRef = @$msg['REF'];
                            if ($msg['STATUS'] == 'OK') {
                                $succeeded[$messageId] = $messageRef;
                            } else {
                                $failed[$messageId] = $messageRef;
                            }
                        }
                        $ret['succeed'] = $succeeded;
                        $ret['failed'] = $failed;
                        $ret['success'] = 1;
                    } else {
                        $ret['error_msg'] = 'Empty message list';
                    }
                } else {
                    $ret['error_msg'] = @$responseArray['REASON'];
                }
            } else {
                $ret['error_msg'] = 'No response from LinkMobility';
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $ret['error_msg'] = $e->getMessage();
        }
        return $ret;
    }

}
