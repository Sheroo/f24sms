#!/usr/bin/env bash
branch_prefix=F24-;
project_slug=F24

if [[ $# > 0 ]]
	then

	branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
	branch_name="(unnamed branch)"     # detached HEAD
	branch_name=${branch_name##refs/heads/}

    if [[ $branch_name == *"$branch_prefix"* ]]
        then
        replace_with=''
        branch_number="${branch_name/$branch_prefix/$replace_with}"
        git add . && git commit -m "$1 __ #$branch_number"

         if [ -z "$2" ]
         then
         git push origin $branch_prefix$branch_number
         fi
    else
        git add . && git commit -m "$1"
        if [ -z "$2" ]
         then
         git push origin $branch_name
         fi
    fi

fi
#file end
