<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckACL
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $cuser = Auth::User();
        if( $cuser ){
            $fearture_slug = \Illuminate\Support\Facades\Request::segment(te_segment('current_slug'));
            $nonACLRoutes = nonACLRoutes();
            if (iCheckInArray($fearture_slug, $nonACLRoutes) == -1) {

                if (!te_check_acl($cuser->role, $fearture_slug)) {

                    return redirect()->route('landing_page');

                }

            }
        }

        return $next($request);

    }
}
