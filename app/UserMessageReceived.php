<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMessageReceived extends Model
{
    protected $table = 'user_message_received';
    protected $fillable = ['recipient_id', 'body', 'from_phone','to_phone','from_country','to_country','message_sid', 'created_at', 'updated_at'];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
