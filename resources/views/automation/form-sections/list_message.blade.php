<div class="item-form-section list_message-section"
     data-slug="{{$controller_slug}}"
     data-section="list_message">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            List/Segment & Message
        </h3>
    </div>
    <div class="item-form-section-body clearfix">
        <?php
        $lists = isset($lists) ? $lists : [];
        $messages = isset($messages) ? $messages : [];
        $emails = isset($emails) ? $emails : [];
        $segments = isset($segments) ? $segments : [];

        ?>
            <div class="form-group clearfix">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select name="scheduled_type" id="scheduled_type"
                            class="select-picker form-control form-white te-input te-required scheduled_type">
                        <option {{@$itemObj->scheduled_type =='list' ? 'selected="selected"':''}} value="list">List</option>
                        <option {{@$itemObj->scheduled_type =='segment' ? 'selected="selected"':''}} value="segment">Segment</option>
                    </select>
                </div>
            </div>
        <div class="form-group clearfix list_sec" <?php echo e(is_null(@$itemObj->scheduled_type) ? '' : (@$itemObj->scheduled_type == 'list' ? '':'style=display:none')) ?>>
            <label class="col-sm-3 control-label">List</label>
            <div class="col-sm-9">
                <select name="list_id" id="list_id" class="select-picker form-control form-white te-input <?php echo e(is_null(@$itemObj->scheduled_type) ? 'te-required' :''); ?> <?php echo e(@$itemObj->scheduled_type == 'list' ? 'te-required':'')?>">
                    <option value="">Select List</option>
                    @foreach($lists as $list)
                        <option
                            {{@$itemObj->list_id==$list->id ? 'selected="selected"':''}} value="{{$list->id}}">{{$list->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

            <div class="form-group clearfix seg_sec" <?php echo e(@$itemObj->scheduled_type == 'segment' ? '':'style=display:none') ?>>
                <label class="col-sm-3 control-label">Segment</label>
                <div class="col-sm-9">
                    <select name="segment_id" id="segment_id" class="select-picker form-control form-white te-input ">
                        <option value="">Select Segment</option>

                        {{@$itemObj->segment_id}}

                        @foreach($segments as $segment)
                            <option
                                {{@$itemObj->segment_id == $segment->id ? 'selected="selected"':''}} value="{{$segment->id}}">
                                {{@$segment->clicked == 1?"+clicked":"" }}
                                {{@$segment->optin == 1?"+optin":"" }}{{@$segment->optin == -1?"-optin":"" }}
                                {{@$segment->abandoned == 1?"+abandoned":"" }}{{@$segment->abandoned == -1?"-abandoned":"" }}
                                {{@$segment->memberp1 == 1?"+memberp1":"" }}{{@$segment->memberp1 == -1?"-memberp1":"" }}
                                {{@$segment->memberp2 == 1?"+memberp2":"" }}{{@$segment->memberp2 == -1?"-memberp2":"" }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group clearfix seg_sec" <?php echo e(@$itemObj->scheduled_type == 'segment' ? '':'style=display:none') ?>>
                <label class="col-sm-3 control-label">Country</label>
                <div class="col-sm-9">
                    <select name="country" id="country" class="select-picker form-control form-white te-input ">
                        <option value="">All Countries</option>

                        {{@$itemObj->country}}

                        @foreach(['47'=>'Norway',  '46' => 'Sweden','358' => 'finland'] as $value => $country)
                            <option
                                {{@$itemObj->country == $value ? 'selected="selected"':''}} value="{{$value}}">
                                {{@$country }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group clearfix sms_msg">
            <label class="col-sm-3 control-label">Message</label>
            <div class="col-sm-9">
                <select name="message_id" id="message_id"
                        class="select-picker form-control form-white te-input te-required">
                    <option value="">Select Message</option>
                    @foreach($messages as $message)
                        <option
                            {{@$itemObj->message_id==$message->id ? 'selected="selected"':''}} value="{{$message->id}}">{{$message->subject}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group clearfix email_msg" style="display: none">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <select name="email_id" id="email_id"
                        class="select-picker form-control form-white te-input ">
                    <option value="">Select Email</option>
                    @foreach($emails as $email)
                        <option
                            {{@$itemObj->message_id==$email->id ? 'selected="selected"':''}} value="{{$email->id}}">{{$email->subject}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
</div>
