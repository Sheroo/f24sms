<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BouncedEmail extends Model
{
    protected $table = 'bounced_email';
    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }


}
