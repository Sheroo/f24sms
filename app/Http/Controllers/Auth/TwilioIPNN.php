<?php

namespace App\Http\Controllers;


use App\BouncedEmail;
use App\Recipient;
use App\SchedulerDeliveryReport;
use App\SchedulerRunHistory;
use App\UserMessageReceived;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class TwilioIPNN extends Controller
{

    public function __construct()
    {
    }


    public function UnsubMailChimpEmailHook($listId, Request $request)
    {

        Log::info('Received IPN for UnsubMailChimpEmailHook listid ' . $listId);
        $postData = $request->all();
        $formData = @$postData['data'];
        Log::info($formData);
        $email = @$formData['email'];
        $recipient = Recipient::where('email', $email)->first();
        if ($recipient) {
            $data = $this->UnsubEmail($email);
            Log::info('UnsubMailChimpEmailHook Email found ' . json_encode($data));

        } else {
            Log::info('UnsubMailChimpEmailHook Email not found ' . $email);
        }
        return ['status' => 'success'];
    }

    public function userMessage(Request $request)
    {
        Log::info('Received IPN for userMessage');
        $rawBody = $request->getContent();
        parse_str($rawBody, $data);

        $keywords = [
            "F24stop",
            "f24webstop",
            "stopf24",
            "Stopsms",
            "stop"
        ];

        $from_phone = ltrim(@$data['From'], '+');
        $to_phone = ltrim(@$data['To'], '+');
        $from_country = @$data['FromCountry'];
        $to_country = @$data['ToCountry'];
        $message_sid = @$data['SmsMessageSid'];
        $body = @$data['Body'];

        $userMessage = UserMessageReceived::create([
            'body' => $body,
            'from_phone' => $from_phone,
            'to_phone' => $to_phone,
            'from_country' => $from_country,
            'to_country' => $to_country,
            'message_sid' => $message_sid,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);

        $recipients = Recipient::where('phone', $from_phone)->get();
        if ($recipients) {
            foreach ($recipients as $recipient) {

                $userMessage->recipient_id = $recipient->id;
                $userMessage->save();

                if (iCheckInArray($body, $keywords)) {
                    $recipient->active = 0;
                    $recipient->unsubscribed_date = Carbon::now()->toDateTimeString();
                    $recipient->save();
                }
            }

        }


    }

    public function linkMobilityStopMessage(Request $request)
    {
        Log::info('Received IPN for linkMobilityStopMessage');
        //$rawBody = file_get_contents("php://input");
        //$data = json_decode($rawBody, true);
//        $rawBody = $request->getContent();
//        parse_str($rawBody, $data);
//
//        if ($data) {
//            $keywords = [
//                "F24stop",
//                "f24webstop",
//                "stopf24",
//                "Stopsms",
//                "stop",
//                "stopp",
//                "Stopp miinto",
//                "wfstop",
//                "WFSTOPP",
//            ];
//
//            $from_phone = ltrim(@$data['SND'], '+');
//            $to_phone = ltrim(@$data['RCV'], '+');
//
//            $body = $data['TXT'];
//            $message_sid = @$data['ID'];
//            if (iCheckInArray($body, $keywords) !== -1) {
//                $isStopMessage = true;
//            }
//
//
//            $userMessage = UserMessageReceived::create([
//                'body' => $body,
//                'from_phone' => $from_phone,
//                'to_phone' => $to_phone,
//                'message_sid' => $message_sid,
//                'created_at' => Carbon::now()->toDateTimeString(),
//                'updated_at' => Carbon::now()->toDateTimeString(),
//            ]);
//
//            if ($isStopMessage) {
//
//                $recipients = Recipient::where('phone', $from_phone)->get();
//                if ($recipients) {
//                    foreach ($recipients as $recipient) {
//
//                        $userMessage->recipient_id = $recipient->id;
//                        $userMessage->save();
//
//                        if (iCheckInArray($body, $keywords)) {
//                            $recipient->active = 0;
//                            $recipient->unsubscribed_date = Carbon::now()->toDateTimeString();
//                            $recipient->save();
//
//                        }
//                    }
//
//                }
//
//            }
//        }
//

    }

    public function IPN($schedulerHistoryId, Request $request)
    {

        Log::info('Received IPN for schedulerHistoryId ' . $schedulerHistoryId);
        $rawBody = $request->getContent();
        parse_str($rawBody, $data);
        if ($data) {

            $history = SchedulerRunHistory::where('id', $schedulerHistoryId)->first();
            if ($history) {
                $automation = $history->automation;
                if ($automation) {

                    $recipientPhone = trim($data['To'], '+');
                    $recipient = Recipient::where(
                        [
                            'phone' => $recipientPhone,
                            'list_id' => $automation->list_id,
                        ]
                    )->first();
                    if ($recipient) {

                        $report = SchedulerDeliveryReport::where([
                            'scheduler_history_id' => $schedulerHistoryId,
                            'recipient_id' => $recipient->id,
                        ])->first();
                        if ($report) {

                            $report->sms_sid = @$data['SmsSid'];
                            $report->message_sid = @$data['MessageSid'];
                            $report->sms_status = @$data['SmsStatus'];
                            $report->message_status = @$data['MessageStatus'];
                            $report->error_code = @$data['ErrorCode'];
                            $report->notification_received = 1;
                            $report->notification_received_time = Carbon::now()->toDateTimeString();
                            $report->save();
                        } else {

                            Log::info('SchedulerDeliveryReport not found for receipient' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                        }
                    } else {
                        Log::info('Recipient not found ' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                    }
                } else {
                    Log::info('Automation rule not found for schedulerHistoryId ' . $schedulerHistoryId);
                }
            } else {
                Log::info('schedulerHistoryId not found ' . $schedulerHistoryId);
            }
        } else {
            Log::info('No Data received in schedulerHistoryId ' . $schedulerHistoryId);
        }


    }

    public function LinkMoblityIPN(Request $request)
    {

        $rawBody = $request->getContent();
        $data = [];
        parse_str($rawBody, $data);

        if (isset($data['ID'])) {

            $schedulerHistoryId = @$data['ID'];
            $ref = @$data['REF'];
            $number = @$data['RCV'];
            $state = @$data['STATE'];
            if (strtolower('DELIVRD') == strtolower($state)) {
                $state = 'delivered';
            }
            $deliveryTime = '';
            try {
                $deliveryTime = Carbon::createFromFormat('Y.m.d H:i:s', $data['DELIVERYTIME']);
            } catch (\Exception $e) {

            }
            $this->updateRecipientStatusFromLinkMobility($schedulerHistoryId, $number, $ref, $state);

        } else {
            Log::info('Linkmobility IPN is empty ');
            Log::info($rawBody);
        }
    }

    function updateRecipientStatusFromLinkMobility($schedulerHistoryId, $number, $ref, $status = '')
    {

        $history = SchedulerRunHistory::where('id', $schedulerHistoryId)->first();
        if ($history) {
            $automation = $history->automation;
            if ($automation) {
                $recipientPhone = $number;
                $recipient = Recipient::where(
                    [
                        'phone' => $recipientPhone,
                        'list_id' => $automation->list_id,
                    ]
                )->first();
                if ($recipient) {

                    $report = SchedulerDeliveryReport::where([
                        'scheduler_history_id' => $schedulerHistoryId,
                        'recipient_id' => $recipient->id,
                    ])->first();

                    if ($report) {
                        $report->sms_sid = $ref;
                        $report->message_sid = $ref;
                        $report->sms_status = strtolower($status);
                        $report->message_status = strtolower($status);
                        $report->notification_received = 1;
                        $report->notification_received_time = Carbon::now()->toDateTimeString();
                        $report->save();
                    } else {
                        Log::info('SchedulerDeliveryReport not found for recipient' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                    }
                } else {
                    Log::info('Recipient not found ' . $recipientPhone . ' for schedulerHistoryId ' . $schedulerHistoryId);
                }
            } else {
                Log::info('Automation rule not found for schedulerHistoryId ' . $schedulerHistoryId);
            }
        } else {
            Log::info('schedulerHistoryId not found ' . $schedulerHistoryId);
        }
    }

    public function UnsubEmail($email)
    {
        $recipients = Recipient::where(['email' => $email])->get();
        if ($recipients) {

            foreach ($recipients as $recipient) {

                $recipient->active = 0;
                $recipient->unsubscribed_date = Carbon::now()->toDateTimeString();
                $recipient->save();
            }

        }

        return ['status' => 'success'];
    }


    public function DeleteEmail($email)
    {
        $recipient = Recipient::where('email', $email);

        if ($recipient) {

            $recipient->delete();
        }
        return ['status' => 'success'];
    }


    public function reConsent($email)
    {
        $recipients = Recipient::where('email', $email)->get();
        if ($recipients) {
            foreach ($recipients as $recipient) {

                $recipient->reconsent_date = Carbon::now()->toDateTimeString();
                $recipient->save();
            }
        } else {
            return ['status' => 'failure'];
        }
        return ['status' => 'success'];
    }

    public function recipientsData($email)
    {
        $recipients = Recipient::where(['email' => $email])->get();

        $recipients = json_encode($recipients);
        return $recipients;

    }

    public function post_fundivia_unique(Request $request)
    {
                $recipient = new Recipient;
                $recipient->email = $request->email;
                $recipient->phone = $request->phone;
                $recipient->first_name = $request->first_name;
                $recipient->last_name = $request->last_name;
                $recipient->domain = $request->domain;
                $recipient->list_id = 188;
                $recipient->save();
                return "record has been saved successfully";

    }
    public function post_fundivia_store(Request $request)
    {
        $email = $request->email;
        $followup = $request->followup;
        $recipients = Recipient::where(['email' => $email])->get();

        $numbers = $recipients->count();
        if($numbers > 0 ){
            return "duplicate";
        }else{
                $recipient = new Recipient;
            $recipient->email = $request->email;
            $recipient->phone = $request->phone;
            $recipient->first_name = $request->first_name;
            $recipient->last_name = $request->last_name;
            $recipient->domain = $request->domain;
            $recipient->list_id = 188;
            $recipient->save();
            return "record has been saved successfully";

        }
//

    }

    public function updateRecipientsForValidEmails()
    {

        $blackEmails = DB::table('mw_list_subscriber')
            ->where('status', '!=', 'confirmed')
            ->whereNull('sync_with_f24sms')
//            ->get()
            ->pluck('email')
            ->toArray();
        //  print_r($blackEmails);
//        $recipients = DB::table('recipients')
//            ->wherein('email', $blackEmails)
////                ->update(['active' => 0])
//            ->get()
//        ;

        $recipients = Recipient::wherein('email', $blackEmails)
//                ->update(['active' => 0])
            ->get();
        $emails = [];

        foreach ($recipients as $recip) {
            $recip->active = 0;
            $recip->save();
            $emails[] = $recip->email;
        }

        $blackEmails = DB::table('mw_list_subscriber')
            ->where('status', '!=', 'confirmed')//this check is required because there are duplicated email for unsub and confirmed, so we don't want to mark 'yes' for confirmed
//            ->whereNull('sync_with_f24sms')
            ->whereIn('email', $emails)
            //           ->get();
            ->update(['sync_with_f24sms' => 'yes']);

//        print_r($blackEmails);


    }

public function updateForFollowup(Request $request){
        $email = $request->email;
        $followup = $request->followup;
    $recipients = Recipient::where(['email' => $email])->get();
    if ($recipients) {
        foreach ($recipients as $recipient) {
            $recipient->followup = $followup;
//            $recipient->reconsent_date = Carbon::now()->toDateTimeString();
            $recipient->optinDate = Carbon::now()->toDateTimeString();

            $recipient->save();
        }
    } else {
        return ['status' => 'failure'];
    }
    return ['status' => 'success'];

}
    public function updateForFollowup_sms(Request $request){
        $phone = $request->phone;
        $followup = $request->followup;
        $recipients = Recipient::where(['phone' => $phone])->get();
        if ($recipients) {
            foreach ($recipients as $recipient) {
                $recipient->followup_sms = $followup;
//            $recipient->reconsent_date = Carbon::now()->toDateTimeString();
                $recipient->optinDate = Carbon::now()->toDateTimeString();

                $recipient->save();
            }
        } else {
            return ['status' => 'failure'];
        }
        return ['status' => 'success'];

    }


    public function sendGridEmailWebhook()
    {

        Log::info('Received SendGrid IPN ');
        $request = Request::capture();
        $postData = $request->all();
        //just sleep for few second in case we are still inserting data in scheduler_delivery_report table
        sleep(2);
        if ($postData) {

            foreach ($postData as $data) {

                $list_id = @$data['list_id'];
                $email = @$data['email'];

                $recipient = Recipient::where([
                    'list_id' => $list_id,
                    'email' => $email
                ])->first();

                if ($recipient) {

                    $scheduler_history_id = $data['scheduler_history_id'];
                    $sg_message_Id = @$data['sg_message_id'];
                    $sg_message_Id = explode('.', $sg_message_Id);
                    $sg_message_Id = @$sg_message_Id[0];

                    $report = SchedulerDeliveryReport::where([
                        'scheduler_history_id' => $scheduler_history_id,
                        'recipient_id' => $recipient->id,

                    ])->first();


                    if ($report) {
                        //if (in_array($data['event'], ['delivered', 'bounce' ,'deferred' , 'dropped' , 'processed' , 'open' , 'click' , 'spamreport' , 'unsubscribe' , 'group_unsubscribe' , 'group_resubscribe' ])) {
                        //if (in_array($data['event'], ['delivered', 'bounce', 'processed', 'open', 'click'])) {

                        $report->message_status = $data['event'];
                        $report->notification_received = 1;
                        $report->notification_received_time = Carbon::now()->toDateTimeString();
                        $report->save();
                        //}
                    } else {

                        Log::info('SchedulerDeliveryReport not found for sg_message_id' . $sg_message_Id);
                    }

                    //if email is bounced then save in separate table
                    //if (in_array($data['event'], ['bounce', 'dropped', 'spamreport', 'unsubscribe'])) {
                    if (in_array($data['event'], ['bounce', 'dropped', 'spamreport', 'unsubscribe'])) {

                        $bouncedData = [];
                        $bouncedData['email'] = $email;
                        $bouncedData['type'] = $data['event'];
                        $bouncedData['created_at'] = Carbon::now()->toDateTimeString();
                        $bouncedData['updated_at'] = Carbon::now()->toDateTimeString();

                        try {

                            BouncedEmail::insert($bouncedData);
                        } catch (\Exception $e) {
                            Log::info('Bounced Email insertion');
                            Log::info($e->getMessage());
                        }

                        //make user inactive
                        $recipient->active = 0;
                        //$recipient->unsubscribed_date = Carbon::now()->toDateTimeString();
                        $recipient->save();
                    }

                } else {
                    Log::info('SchedulerDeliveryReport recipient not found for email ' . $email);
                }
            }

        } else {
            Log::info('No Data received in SendGrid ');
        }
    }


    public function mailGunEmailWebhook()
    {

        Log::info('Received Mailgun IPN ');
        $request = Request::capture();
        $postData = $request->all();
        //just sleep for few second in case we are still inserting data in scheduler_delivery_report table
        sleep(2);
        if ($postData) {


            $list_id = @$postData['event-data']['user-variables']['list_id'];
            $email = @$postData['event-data']['recipient'];

            $recipient = Recipient::where([
                'list_id' => $list_id,
                'email' => $email
            ])->first();

            if ($recipient) {

                $scheduler_history_id = @$postData['event-data']['user-variables']['scheduler_history_id'];
                $sg_message_Id = @$postData['event-data']['message']['headers']['message-id'];
                $sg_message_Id = explode('.', $sg_message_Id);
                $sg_message_Id = @$sg_message_Id[0];

                $report = SchedulerDeliveryReport::where([
                    'scheduler_history_id' => $scheduler_history_id,
                    'recipient_id' => $recipient->id,

                ])->first();


                if ($report) {
                    //if (in_array($data['event'], ['delivered', 'bounce' ,'deferred' , 'dropped' , 'processed' , 'open' , 'click' , 'spamreport' , 'unsubscribe' , 'group_unsubscribe' , 'group_resubscribe' ])) {
                    if (in_array($postData['event-data']['event'], ['delivered', 'failed', 'opened', 'clicked'])) {

                        if ($postData['event-data']['event'] == 'opened') {
                            $postData['event-data']['event'] = 'open';
                        }

                        if ($postData['event-data']['event'] == 'clicked') {
                            $postData['event-data']['event'] = 'click';
                        }


                        $report->message_status = $postData['event-data']['event'];
                        $report->notification_received = 1;
                        $report->notification_received_time = Carbon::now()->toDateTimeString();
                        $report->save();
                    }
                } else {

                    Log::info('SchedulerDeliveryReport not found for sg_message_id' . $sg_message_Id);
                }

                //if email is bounced then save in separate table
                if (in_array($postData['event-data']['event'], ['complained', 'unsubscribed'])) {

                    $bouncedData = [];
                    $bouncedData['email'] = $email;
                    if ($postData['event-data']['event'] == 'unsubscribed') {
                        $postData['event-data']['event'] = 'unsubscribe';
                    }
                    $bouncedData['type'] = $postData['event-data']['event'];
                    $bouncedData['created_at'] = Carbon::now()->toDateTimeString();
                    $bouncedData['updated_at'] = Carbon::now()->toDateTimeString();

                    try {

                        BouncedEmail::insert($bouncedData);
                    } catch (\Exception $e) {
                        Log::info('Bounced Email insertion');
                        Log::info($e->getMessage());
                    }

                    //make user inactive
                    $recipient->active = 0;
                    //$recipient->unsubscribed_date = Carbon::now()->toDateTimeString();
                    $recipient->save();
                }

            } else {
                Log::info('SchedulerDeliveryReport recipient not found for email' . $email);
            }


        } else {
            Log::info('No Data received in SendGrid ');
        }
    }
}
