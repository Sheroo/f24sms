<?php

namespace App\Modules\Traits;

use App\MyList;
use App\TemplateBulkUploadRecipient;
use Carbon\Carbon;
use Goodby\CSV\Export\Standard\CsvFileObject;
use Goodby\CSV\Export\Standard\Exception\StrictViolationException;
use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Goodby\CSV\Import\Standard\Lexer;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

trait HandleBulkUpload
{
    public $bulk_upload_entity;
    public $bulk_upload_hidden_data;

    public function getEntitiesToBeUploaded()
    {
        $entities = [
            ENTITY_RECIPIENT
        ];
        return $entities;
    }

    public function getRequiredColumnsForCSV()
    {
        $reqiued_columns = [
            ENTITY_RECIPIENT => [
                'mobile',
                'countryCode',
            ]
        ];
        return isset($reqiued_columns[$this->bulk_upload_entity]) ? $reqiued_columns[$this->bulk_upload_entity] : [];
    }

    public function getOptionalColumnsForCSV()
    {
        $reqiued_columns = [
            ENTITY_RECIPIENT => [
                'firstName',
                'lastName',
                'email'
            ]
        ];
        return isset($reqiued_columns[$this->bulk_upload_entity]) ? $reqiued_columns[$this->bulk_upload_entity] : [];
    }

    public function getAllCSVColumns()
    {

        return array_merge($this->getRequiredColumnsForCSV(), $this->getOptionalColumnsForCSV());

    }

    public function uploadCSVFile()
    {
        set_time_limit(0);

        $cuser = Auth::user();
        $file = Input::all();

        $error = '';
        $filePath = '';

        if (isset($file['file']) && !array_key_exists('testCase', $file)) {
            $extension = $file['file']->getClientOriginalExtension();
        } else {
            $extension = 'csv';
        }

        if (isset($extension) && strtolower($extension) != 'csv') {
            $error = 'Only CSV file allowed!';
        } else if (!isset($file['file']) && empty($file['file'])) {
            $error = 'Please select file to upload!';
        } else {

            if (!array_key_exists('testCase', $file)) {
                try {
                    $destinationPath = public_path('uploads'); // upload path
                    $fileName = 'bulkUpload_entities_' . $cuser->id . '_' . date('Ymd') . '_' . time() . '_' . rand() . '.' . $extension; // renaming file
                    $file['file']->move($destinationPath, $fileName); // uploading file to given path
                    $filePath = $destinationPath . '/' . $fileName;
                } catch (Exception $e) {
                    $filePath = '';
                    $error = $e->getMessage();
                    if (stripos($error, 'upload_max_filesize') !== false) {
                        $error = "File size should be less than " . env('BULK_UPLOAD_MAX_FILESIZE', 16) . " MB.";
                    }
                    notifyBugsnagError('Error in BulkUploadFile [' . $e->getMessage() . ']', [], 'info');
                }
            } else {
                $filePath = $file['file'];
            }
        }
        return [$filePath, $error];
    }

    public function validateCSVHeader($csvHeader)
    {
        $requiredColumns = $this->getRequiredColumnsForCSV();
        $optionalColumns = $this->getOptionalColumnsForCSV();
        $allCSVColumns = $this->getAllCSVColumns();
        $columns_are_matched = true;
        foreach ($requiredColumns as $reqCol) {
            if (iCheckInArray($reqCol, $csvHeader) == -1) {
                $columns_are_matched = false;
                break;
            }
        }
        if ($columns_are_matched) {

            foreach ($csvHeader as $csvHeaderValue) {
                if (iCheckInArray($csvHeaderValue, $allCSVColumns) == -1) {
                    $columns_are_matched = false;
                    break;
                }
            }

        }

        return $columns_are_matched;
    }

    public function parseCSVHeader($filePath)
    {

        $csv_is_valid = true;
        $csvHeader = null;
        $response = [
            'csv_is_valid' => true,
            'error_msg' => '',
            'csvHeader' => $csvHeader
        ];
        $interpreter = new Interpreter();
        $lexerConfig = new LexerConfig();
        $lexerConfig->setDelimiter(";");
        $lexer = new Lexer($lexerConfig);
        $interpreter->addObserver(function (array $row) use (&$csvHeader, &$response) {

            if ($csvHeader == null) {
                $csvHeader = $this->checkCaseSenstiveInCSVHeaders($row);
                $response['csvHeader'] = $csvHeader;
                return;
            }

        });

        try {

            $lexer->parse($filePath, $interpreter);

        } catch (StrictViolationException $ex) {

            $csv_is_valid = false;
            $response['csv_is_valid'] = false;
            $response['error_msg'] = $ex->getMessage();

        } catch (\Exception $ex) {

            $csv_is_valid = false;
            $response['csv_is_valid'] = false;
            $response['error_msg'] = $ex->getMessage();

        }

        if ($csv_is_valid) {
            $response['csv_is_valid'] = $this->validateCSVHeader($csvHeader);
        }

        return $response;

    }

    public function parseCSVAndDumpIntoDatabase($filePath, $tempTableName)
    {

        $currentUser = Auth::user();
        $csvHeader = null;

        $entities = [];

        $response = [
            'error_msg' => '',
        ];

        /*
         * initializing of arrays
         */
        $entities_count = [];
        $preview_rows = [];
        foreach ($this->getEntitiesToBeUploaded() as $tempEntity) {
            $entities_count[$tempEntity] = 0;
            $preview_rows[$tempEntity] = [];
        }

        $insert_chunk_size = 5000;//env('BULK_UPLOAD_INSERT_CHUNK_SIZE', 1500);
        $interpreter = new Interpreter();
        $lexerConfig = new LexerConfig();
        $lexerConfig->setDelimiter(";");
        $lexer = new Lexer($lexerConfig);

        $config = new ExporterConfig();
        $config->setFileMode(CsvFileObject::FILE_MODE_APPEND);
        $exporter = new Exporter($config);

        $destinationPath = public_path('uploads'); // upload path
        $fileName = 'dump_bu_entities_' . $this->bulk_upload_entity . '_' . $currentUser->id . '_' . date('Ymd') . '_' . time() . '_' . rand() . '.csv'; // renaming file
        $exportedFilePath = $destinationPath . '/' . $fileName;

        $csvHeaderExported = false;
        $exportedHeader = [];

        $listObj = null;
        if (te_compare_strings($this->bulk_upload_entity, ENTITY_RECIPIENT)) {
            $listObj = MyList::where('id', '=', @$this->bulk_upload_hidden_data['list_id'])->first();
            $listObj = $listObj ? $listObj : null;
        }

        $interpreter->addObserver(function (array $row) use (
            $tempTableName
            , &$csvHeader
            , &$csvHeaderExported
            , &$exportedHeader
            , &$entities
            , $insert_chunk_size
            , &$channelTypes
            , $exportedFilePath
            , $exporter
            , $currentUser
            , &$entities_count
            , &$preview_rows
            , $listObj

        ) {

            if ($csvHeader == null) {

                $csvHeader = $this->checkCaseSenstiveInCSVHeaders($row);
                $response['csvHeader'] = $csvHeader;

            } else {

                $row = array_combine($csvHeader, $row);
                foreach ($row as $row_index => $row_value) {
                    $row[$row_index] = trim($row_value);
                }

                $row['Error'] = 0;
                $row['Upload Status'] = [];

                if (te_compare_strings($this->bulk_upload_entity, ENTITY_RECIPIENT)) {
                    $row['List Id'] = @$this->bulk_upload_hidden_data['list_id'];
                    $row['List Name'] = @$listObj->name;
                    $row['Entity'] = ENTITY_RECIPIENT;
                    $this->processRecipientValidation($row, $csvHeader);
                }

                if (!empty($row['Entity']) && in_array($row['Entity'], $this->getEntitiesToBeUploaded())) {
                    $entities[] = $row;
                    /*
                     * update entities count
                     */
                    $entities_count[$row['Entity']]++;
                    if (count($preview_rows[$row['Entity']]) < env('BULK_UPLOAD_SUMMARY_ROW_LIMIT', 200)) {
                        $preview_rows[$row['Entity']][] = $row;
                    }
                }

                if (count($entities) >= $insert_chunk_size) {
                    try {
                        if (!$csvHeaderExported) {
                            $csvHeaderExported = true;
                            $_exportedHeader = array_keys($entities[0]);
                            $exportedHeader = $this->checkCaseSenstiveInCSVHeaders($_exportedHeader);
                            $exporter->export($exportedFilePath, [$exportedHeader]);
                        }

                        $exporter->export($exportedFilePath, $entities);

                    } catch (Exception $e) {
                        notifyBugsnagError($e, [
                            'user Id' => $currentUser->id,
                            'temp table name' => @$tempTableName
                        ]);
                    }
                    $entities = [];

                }
            }

        });

        try {
            $lexer->parse($filePath, $interpreter);
            if (count($entities) > 0) {
                if (!$csvHeaderExported) {
                    $_exportedHeader = array_keys($entities[0]);
                    $exportedHeader = $this->checkCaseSenstiveInCSVHeaders($_exportedHeader);
                    $exporter->export($exportedFilePath, [$exportedHeader]);
                }

                $exporter->export($exportedFilePath, $entities);

            }

            $csvColumnsMapping = [];
            foreach ($exportedHeader as $headerKey => $headerValue) {
                $csvColumnsMapping[$headerValue] = $headerValue;
            }

        } catch (StrictViolationException $ex) {

            $response['error_msg'] = $ex->getMessage();
            notifyBugsnagError($ex, [
                'user Id' => $currentUser->id,
                'error_msg' => $ex->getMessage(),
                'temp table name' => @$tempTableName,
                'queryTemplate' => ''
            ]);

        }

//        list($accountAdminConfigs, $adminConfigAccountIds) = adminConfigOfProvidedAccounts($accountIds);
        // this query will run on each separate DB to dump all bulk upload data, later we will delete the unwanted data
        $queryTemplate = prepare_load_infile_query_for_bulk_upload_temp_table($csvColumnsMapping, $exportedHeader, $tempTableName, $exportedFilePath);

//        /*
//         * create main table in database that will hold stats across all tables
//         */
//        $queryTemplate_main = '';
//        try {
//            // loading the same CSV file into main table in main DB ( used to show summary modal data for all channels )
//            $bulkUpload_Temp_Main_TableName = getBulkUploadTempMainTableName($tempTableName);
//            createBulkUploadTempTable($this->bulk_upload_entity, $bulkUpload_Temp_Main_TableName);
//            $queryTemplate_main = prepare_load_infile_query_for_bulk_upload_temp_table($csvColumnsMapping, $exportedHeader, $bulkUpload_Temp_Main_TableName, $exportedFilePath);
//            DB::getPdo()->exec($queryTemplate_main);
//        } catch (\Exception $ex) {
//            $response['error_msg'] = $ex->getMessage();
//            notifyBugsnagError($ex, [
//                'user Id' => $currentUser->id,
//                'error_msg' => $ex->getMessage(),
//                'temp table name' => $bulkUpload_Temp_Main_TableName,
//                'queryTemplate' => $queryTemplate_main
//            ]);
//        }

        // this loop get all DB layers,
        // create temp tables in each db layer
        // insert all bulk upload data in each db layer
        // delete irrelevant data from each db layer which do not belong to its accounts

        // create bulk upload temp table in separate DB (get the first account of DB layer, just to get the connection)
        createBulkUploadTempTable($this->bulk_upload_entity, $tempTableName);

        // dump all the csv data into temp table
        DB::getPdo()->exec($queryTemplate);


        if ($currentUser->debug == 'no') {
            if (file_exists($exportedFilePath)) {
                @unlink($exportedFilePath);
            }
        }
        $response['entities_count'] = $entities_count;
        $response['preview_rows'] = $preview_rows;
        return $response;

    }

    public function uploadEntities(Request $request)
    {
        set_time_limit(0);
        $currentUser = Auth::user();

        $response['status'] = 'fail';
        $response['temp_table_name'] = '';
        $response['entities_count'] = [];

        $bulk_upload_hidden_data = $request->get('bulk_upload_hidden_data', []);
        $this->bulk_upload_hidden_data = $bulk_upload_hidden_data;

        $bulk_upload_entity = @$bulk_upload_hidden_data['bulk_upload_entity'];

        if (empty($bulk_upload_entity) || !iCheckInArray($bulk_upload_entity, $this->getEntitiesToBeUploaded())) {
            $response['message'] = 'Invalid Entity ' . $bulk_upload_entity;
            return response()->json($response);
        } else {
            $this->bulk_upload_entity = $bulk_upload_entity;
        }

        try {
            list($filePath, $error) = $this->uploadCSVFile();
        } catch (Exception $e) {
            $filePath = '';
            $error = $e->getMessage();
            if (stripos($error, 'upload_max_filesize') !== false) {
                $error = "File size should be less than 16 MB.";
            }
            notifyBugsnagError('Error in BulkUploadFile [' . $e->getMessage() . ']', [], 'info');
        }
        $csvHeaders = null;
        $entities = [];

        if (!empty($error)) {

            $response['message'] = $error;
            return response()->json($response);

        } else {

            $CSVHeaderResponse = $this->parseCSVHeader($filePath);
            $csvHeaders = $CSVHeaderResponse['csvHeader'];

            $csv_is_valid = $CSVHeaderResponse['csv_is_valid'];

            $entityDataForValidationModel = [];
            $parseCSVResponse = [];
            if ($csv_is_valid) {

                $bulkUpload_TempTableName = getBulkUploadTempTableName($currentUser->id, $this->bulk_upload_entity);
                $response['temp_table_name'] = $bulkUpload_TempTableName;

                // prepare preview data in memory ( to avoid query from database )
                // also calculate entities count in memory
                // we also get channels array in memory while reading csv file
                $parseCSVResponse = $this->parseCSVAndDumpIntoDatabase($filePath, $bulkUpload_TempTableName);
                @unlink($filePath);

                $entityDataForValidationModel = $parseCSVResponse['preview_rows'];
                $response['entities_count'] = $parseCSVResponse['entities_count'];

            } else {

                $csvHeaders = $csvHeaders ? $csvHeaders : [];
                $requiredColumns = $this->getRequiredColumnsForCSV();

                $allColumns = $this->getAllCSVColumns();
                $entityDataForValidationModel['requiredColumn'] = $requiredColumns;
                $entityDataForValidationModel['userFileColumn'] = $csvHeaders;
                foreach ($requiredColumns as $key => $col) {
                    if (iCheckInArray($col, $csvHeaders) == -1) {
                        $entityDataForValidationModel['missingCols'][] = $col;
                    }
                }

                foreach ($csvHeaders as $key => $col) {
                    if (iCheckInArray($col, $allColumns) == -1) {
                        $entityDataForValidationModel['extraCols'][] = $col;
                    }
                }

                $response['csv_parse_error'] = @$CSVHeaderResponse['error_msg'];
            }

            $response['csv_is_valid'] = $csv_is_valid;
            $dataForCSVFilePreview = array(
                'uploadedEntitiesValidation' => $entityDataForValidationModel,
                'csvIsValid' => $csv_is_valid,
                'csv_parse_error' => @$response['error_msg'],
                'entities_count' => $response['entities_count'],
                'bulk_upload_entity' => $this->bulk_upload_entity,
                'controller_slug' => $this->controller_slug
            );
            $response['previewdata'] = (String)view('bulk-upload._preview-bulk-upload-data-validation', $dataForCSVFilePreview);
            $response['status'] = 'success';
            $response['bulk_upload_entity'] = $this->bulk_upload_entity;
            $response['afterApiCall'] = 0;
            return response()->json($response);
        }
    }

    public
    function downloadBulkUploadSummaryCSV(Request $request)
    {
        $response['status'] = 'fail';
        $response['msg'] = 'File is not ready.';
        $bulk_upload_hidden_data = $request->get('bulk_upload_hidden_data', []);

        $this->bulk_upload_hidden_data = $bulk_upload_hidden_data;
        $this->bulk_upload_entity = @$bulk_upload_hidden_data['bulk_upload_entity'];

        $event = $request->get('afterApi');
        $tempTableName = $request->get('temp_table_name');
        if ($event == 'Yes') {
            $tempTableName .= "_apiresponse";
        }
        $response = $this->writeBulkUploadSummaryFile($tempTableName, $event);
        return $response;

    }

    public
    function uploadEntityData(Request $request)
    {
        set_time_limit(0);

        $bulk_upload_hidden_data = $request->get('bulk_upload_hidden_data', []);
        $this->bulk_upload_hidden_data = $bulk_upload_hidden_data;

        $this->bulk_upload_entity = @$bulk_upload_hidden_data['bulk_upload_entity'];

        $temp_table = $request->get('temp_table_name');

        //this will hold Value TEMPORARY if we wanted to use TEMPORARY keyword while creating temp tables
        $destinationPath = public_path('uploads'); // upload path

        //this table hold stats from across different databases
//        $main_table_for_overall_stats = getBulkUploadTempMainTableName($temp_table);

//        $main_table_for_overall_stats_fileName = $main_table_for_overall_stats . "_dump" . ".csv";
//        $main_table_for_overall_stats_filePath = $destinationPath . '/' . $main_table_for_overall_stats_fileName;

//        $main_api_table_for_overall_stats = getBulkUploadTempMainTableName($temp_table . '_apiresponse');
//        $main_api_table_for_overall_stats_fileName = $main_api_table_for_overall_stats . "_dump" . ".csv";
//        $main_api_table_for_overall_stats_filePath = $destinationPath . '/' . $main_api_table_for_overall_stats_fileName;

//        $main_table_for_overall_stats_columns = [];
//        if (te_compare_strings(ENTITY_RECIPIENT, $this->bulk_upload_entity)) {
//            $main_table_for_overall_stats_columns = (new TemplateBulkUploadRecipient())->getTableColumns();
//        }
        //remove id column as wel don't want to import that
//        $main_table_for_overall_stats_columns = collect($main_table_for_overall_stats_columns)->filter(function ($value) {
//            return $value != 'id';
//        })->toArray();

//        $main_table_for_overall_stats_columns_raw = collect($main_table_for_overall_stats_columns)->map(function ($value) {
//            return "`$value`";
//        })->implode(",");

        $template_table_name = strtolower('template_bulk_upload_' . $this->bulk_upload_entity);
//        DB::statement('DROP TABLE IF EXISTS ' . $main_api_table_for_overall_stats . ';');
        // creat temp table in main database as well if we are only uploading data for a account residing in different database
        // to make sure that this table always exists in main DB ( to avoid any error )
//        DB::statement('CREATE TABLE IF NOT EXISTS ' . $temp_table . ' LIKE ' . $template_table_name);
//        DB::statement('CREATE TABLE IF NOT EXISTS ' . $main_api_table_for_overall_stats . ' LIKE ' . $temp_table . ';');
//        DB::statement('TRUNCATE TABLE ' . $main_table_for_overall_stats . ';');

        $entitiesCount = DB::table($temp_table)
            ->where('Error', 0)
            ->count();

        if ($entitiesCount > 0) {
            $api_response_temp_table = $temp_table . '_apiresponse';

            DB::statement('DROP TABLE IF EXISTS ' . $api_response_temp_table . ';');
            DB::statement('CREATE TABLE IF NOT EXISTS ' . $api_response_temp_table . ' LIKE ' . $temp_table . ';');
            $this->storeBulkDataInDatabase($temp_table, $api_response_temp_table);

            /*
             * populate main table and main api table so we can present user with stats
             */
            //export data in a csv file and then we will upload to main table and api table

            // insert data into api_response_main table
//            $rawQuery = " SELECT " . $main_table_for_overall_stats_columns_raw . " FROM $api_response_temp_table ";
//            $command = prepareExportCSVCommand($rawQuery, $main_api_table_for_overall_stats_fileName);
//            $worked = exec($command, $output, $worked);
//            $queryTemplate = prepare_load_infile_query_for_bulk_upload_temp_table($main_table_for_overall_stats_columns, $main_table_for_overall_stats_columns, $main_api_table_for_overall_stats, $main_api_table_for_overall_stats_filePath, "\t", "\n", 'latin1');
//            DB::connection()->getPdo()->exec($queryTemplate);

            // insert data into temp_bulkUPload_main table
//            $rawQuery = " SELECT " . $main_table_for_overall_stats_columns_raw . " FROM $temp_table ";
//            $command = prepareExportCSVCommand($rawQuery, $main_table_for_overall_stats_fileName);
//            $worked = exec($command, $output, $worked);
//            $queryTemplate = prepare_load_infile_query_for_bulk_upload_temp_table($main_table_for_overall_stats_columns, $main_table_for_overall_stats_columns, $main_table_for_overall_stats, $main_table_for_overall_stats_filePath, "\t", "\n", 'latin1');
//            DB::connection()->getPdo()->exec($queryTemplate);
        }


        $countAfterAPIResponse = $this->bulkUploadedEntitiesFinalCount($template_table_name, $api_response_temp_table);

        $response['previewdata'] = (String)view('bulk-upload._preview-bulk-upload-count', ['uploadedEntitiesCount' => $countAfterAPIResponse]);
        $response['error'] = 0;
        $response['status'] = 'success';
        $response['count'] = $countAfterAPIResponse;
        $response['afterApiCall'] = 1;
        return response()->json($response);

    }

    function writeBulkUploadSummaryFile($tempTableName, $afterAPICall = 'No')
    {
        set_time_limit(0);
//        $tempTableName = $tempTableName . '_main';
        if ($afterAPICall == 'skipped') {
            $rawQuery = "select * from " . $tempTableName . " where Error = 1";
        } else {
            $rawQuery = "select * from " . $tempTableName;
        }
        $filePathCopy = "data_" . $this->bulk_upload_entity . "_" . Carbon::now()->toDateString() . "_dump" . ".csv";
        $command = prepareExportCSVCommand($rawQuery, $filePathCopy);
        $worked = exec($command, $output, $worked);
        if (!$worked) {
            if ($afterAPICall == "Yes") {
                $filename = "ApiResponse_" . $this->bulk_upload_entity . "_" . date('Ymd') . "_" . time() . "_" . ".csv";
            } elseif ($afterAPICall == "skipped") {
                $filename = "Skipped_" . $this->bulk_upload_entity . "_" . date('Ymd') . "_" . time() . "_" . ".csv";
            } else {
                $filename = "ProcessingSummary_" . $this->bulk_upload_entity . "_" . date('Ymd') . "_" . time() . "_" . ".csv";
            }
            $handle = fopen(public_path('uploads/' . $filename), 'w+');

            $config = new LexerConfig();
            $config->setIgnoreHeaderLine(false);
            $config->setDelimiter(";");
            $lexer = new Lexer($config);

            $aggregate_types = $this->aggregate_columns_for_bulk();
            foreach ($aggregate_types as $key => $value) {
                $aggregate_types[$key] = remove_alias($value);
            }

            $selected_csv_columns_with_keys = [];
            $entity_custom_labels = [];
            $choosed_columns = $this->getChooseableColumnsForBulk();
            if ($afterAPICall == 'Yes') {
                $choosed_columns['Api Response'] = "Api Response";
            }
            foreach ($choosed_columns as $key => $value) {
                $selected_csv_columns_with_keys[remove_alias($key)] = $value;
            }
//            $index = array_search('Title', array_keys($selected_csv_columns_with_keys));
//            if ($index !== false) {
//                $selected_csv_columns_with_keys = array_merge(array_slice($selected_csv_columns_with_keys, 0, $index + 1), ['URL' => 'URL'], array_slice($selected_csv_columns_with_keys, $index + 1));
//            }

            fputcsv($handle, $selected_csv_columns_with_keys);  /*add header to csv*/
            $interpreter = assignKeysToCsvRowsForBulk($selected_csv_columns_with_keys, $isCompared = false, $aggregate_types, $target_csv_file_handle = $handle, 'bulk', column_types(), $entity_custom_labels, $afterAPICall);
            $lexer->parse(public_path('uploads/') . $filePathCopy, $interpreter);

            fclose($handle);
            $response['path'] = url('uploads/' . $filename);
            $response['status'] = 'success';
            @unlink($filePathCopy);
            return response()->json($response);
        } else {
            $response['status'] = 'failure';
            return response()->json($response);
        }
    }

    public
    function checkCaseSenstiveInCSVHeaders($csvHeaders)
    {
        $requiredColumns = $this->getAllCSVColumns();

        $newCSVHeader = [];
        foreach ($csvHeaders as $csvHeaderValue) {
            $check = iMatchInArray($csvHeaderValue, $requiredColumns);
            $newCSVHeader[] = $check != -1 ? $check : $csvHeaderValue;
        }
        return $newCSVHeader;
    }

    public
    function getEntitiesToBeCreatedAndUpdated($bulkUploadData, $entityName)
    {
        $created = [];
        $updated = [];

        $createdIndex = [];
        $updatedIndex = [];

        foreach ($bulkUploadData as $entityIndex => $entity) {
            $functionName = "prepare_" . $entityName . "_for_api_call";
            $created[] = $functionName($entity, 'Create');
            $createdIndex[] = $entityIndex;
        }

        return [
            $created, $createdIndex, $updated, $updatedIndex
        ];

    }

    public
    function bulkUploadedEntitiesFinalCount($temp_table, $api_response_temp_table)
    {
        $countAfterAPIResponse = [];

        $entities = $this->getEntitiesToBeUploaded();
        foreach ($entities as $entity) {
            $countAfterAPIResponse[$entity]['added'] = 0;
            $countAfterAPIResponse[$entity]['skipped'] = 0;
            $countAfterAPIResponse[$entity]['error'] = 0;
        }

        $query = 'SELECT Entity, SUM(Error) AS skipped FROM ' . $temp_table . ' GROUP BY Entity;';

        $skippedEntities = DB::select($query);
        foreach ($skippedEntities as $skippedEntity) {
            $countAfterAPIResponse[$skippedEntity->Entity]['skipped'] = $skippedEntity->skipped;
        }

        $query = 'select Entity,Error,count(Error) ErrorCount from ' . $api_response_temp_table . ' group by Entity,Error;';
        $apiErrorEntities = \Illuminate\Support\Facades\DB::select($query);
        foreach ($apiErrorEntities as $apiErrorEntity) {
            if ($apiErrorEntity->Error == 1) {
                $countAfterAPIResponse[$apiErrorEntity->Entity]['error'] = $apiErrorEntity->ErrorCount;
            } else {
                $countAfterAPIResponse[$apiErrorEntity->Entity]['added'] = $apiErrorEntity->ErrorCount;
            }
        }

        return $countAfterAPIResponse;
    }

    public function getEntityProductionTable()
    {
        $tableNames = [
            ENTITY_RECIPIENT => 'recipients'
        ];
        return isset($tableNames[$this->bulk_upload_entity]) ? $tableNames[$this->bulk_upload_entity] : '';
    }

    public
    function storeBulkDataInDatabase($temp_table_name, $api_response_temp_table)
    {
        $currentUser = Auth::user();

        // dump validated data into api response table
        $query = "INSERT INTO $api_response_temp_table SELECT * FROM $temp_table_name where `Error`!='1'";
        DB::statement($query);

        $entityProductionTable = $this->getEntityProductionTable();

        if (te_compare_strings(ENTITY_RECIPIENT, $this->bulk_upload_entity)) {
            $query = "update `" . $api_response_temp_table . "` 
                        inner join `" . $entityProductionTable . "` 
                        on `recipients`.`phone` = `" . $api_response_temp_table . "`.`mobile` 
                        and `" . $api_response_temp_table . "`.`Entity` = '" . ENTITY_RECIPIENT . "' 
                        and `recipients`.`list_id` = " . $api_response_temp_table . ".`List Id`
                        set `" . $api_response_temp_table . "`.`Api Response` = 'Recipient already exists', `" . $api_response_temp_table . "`.`Error` = 1 ";
            Log::info($query);
            DB::statement($query);
            $insertJoinedCols = ['phone' => 'mobile', 'list_id' => 'List Id'];
            $insertWhereClause = " AND Error!='1' AND `Entity`='" . ENTITY_RECIPIENT . "' ";

            $errorInInsert = bulkInsertFromTempTableJoin(
                $entityProductionTable,
                $api_response_temp_table,
                'id',
                [
                    'first_name' => '{tempTableName}.`firstName`',
                    'last_name' => '{tempTableName}.`lastName`',
                    'email' => '{tempTableName}.`email`',
                    'phone' => 'concat({tempTableName}.`countryCode` , {tempTableName}.`mobile`)',
                    'country_code' => '{tempTableName}.`countryCode`',
                    'list_id' => '{tempTableName}.`List Id`',
                    'created_by' => "'" . $currentUser->id . "'",
                    'created_at' => "'" . Carbon::now()->toDateTimeString() . "'",
                    'updated_at' => "'" . Carbon::now()->toDateTimeString() . "'"
                ],
                $insertJoinedCols,
                $insertWhereClause,
                false
            );
        }

        if (!empty($errorInInsert)) {
            $updateQuery = "UPDATE $api_response_temp_table set `Error`='1', `Api Response`='" . $errorInInsert . "' WHERE 1=1 AND Error!='1' $insertWhereClause";
            DB::statement($updateQuery);
        } else {
            $updateQuery = "UPDATE $api_response_temp_table set `Error`='0', `Api Response`='Entity Created Successfully.' WHERE 1=1 AND Error!='1' $insertWhereClause";
            DB::statement($updateQuery);
        }
    }

    public
    function processRecipientValidation(&$entity, $csvHeaders)
    {
        $thisEntity = ENTITY_RECIPIENT;

        $entityFields = bulkUploadEntityColsEmptyValCheck($thisEntity);

        $requiredFields = [];
//        $requiredFields[] = 'First Name';
//        $requiredFields[] = 'Last Name';
//        $requiredFields[] = 'Email';
        $requiredFields[] = 'mobile';

        $campaignNumericFields = [];
        $campaignEnumFields = [];
        $checkEncodinngFields = [];

        $enums = [];
//        $enums['Campaign Type'] = ['sponsoredProducts'];

        foreach ($entityFields as $apiColName) {
            if (isset($entity[$apiColName])) {

                if (in_array($apiColName, $requiredFields) && empty($entity[$apiColName])) {
                    $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' is missing.';
                    $entity['Error'] = 1;
                }

                if (in_array($apiColName, $campaignNumericFields) && $entity[$apiColName] != '') {
                    if (!is_numeric($entity[$apiColName])) {
                        $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' must be a numeric value.';
                        $entity['Error'] = 1;
                    }
                }

                if (in_array($apiColName, $campaignEnumFields) && $entity[$apiColName] != '') {
                    $arrayCheck = iCheckInArray($entity[$apiColName], $enums[$apiColName]);
                    if ($arrayCheck != -1) {
                        $entity[$apiColName] = $arrayCheck;
                    } else {
                        $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' ' . $entity[$apiColName] . ' is not valid. Acceptable values are ' . implode(', ', $enums[$apiColName]) . '.';
                        $entity['Error'] = 1;
                    }
                }

                if ($entity[$apiColName] != '' && in_array($apiColName, $checkEncodinngFields) && convertIfEncodingIsNotUTF8($entity[$apiColName])) {
                    $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' must not contain special characters.';
                    $entity['Error'] = 1;
                }

                if (te_compare_strings($apiColName, 'Campaign Start Date')) {

                    if (!empty(trim($entity[$apiColName]))) {
                        $campaignStartDate = te_validate_date($entity[$apiColName]);
                        if ($campaignStartDate) {
                            $campaignStartDate = Carbon::createFromFormat('m/d/Y', $campaignStartDate)->startOfDay();
                        }

                        if ($campaignStartDate) {
                            Carbon::now()->startOfDay();
                            $curDate = Carbon::createFromFormat('m/d/Y', date('m/d/Y'))->startOfDay();
                            if ($campaignStartDate->lessThan($curDate)) {
                                $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' ' . $entity[$apiColName] . ' can not be in the past. Please enter a valid date.';
                                $entity['Error'] = 1;
                            } else {
                                $entity['Campaign Start Date'] = $campaignStartDate->format('m/d/Y');
                            }
                        } else {
                            $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' ' . $entity[$apiColName] . ' is invalid. Must be a valid format mm/dd/yyyy OR m/d/yy.';
                            $entity['Error'] = 1;
                        }

                    }
                }

            } else {
                $entity_array_keys = array_keys($entity);
                foreach ($requiredFields as $field) {
                    if (!in_array($field, $entity_array_keys)) {
                        $entity['Upload Status'][$apiColName] = 'The ' . $apiColName . ' is missing.';
                        $entity['Error'] = 1;
                    }
                }
            }
        }
        $entity['Upload Status'] = json_encode($entity['Upload Status']);
    }

    public
    function aggregate_columns_for_bulk()
    {
        $cols = [
            ENTITY_RECIPIENT => [
                'firstName',
                'lastName',
                'email',
                'mobile',
                'countryCode',
                'Error',
                'Upload Status',
                'Api Response',
            ]
        ];

        return isset($cols[$this->bulk_upload_entity]) ? $cols[$this->bulk_upload_entity] : [];
    }

    function getChooseableColumnsForBulk()
    {
        $cols = [
            ENTITY_RECIPIENT => [
                'firstName' => 'firstName',
                'lastName' => 'lastName',
                'email' => 'email',
                'mobile' => 'mobile',
                'countryCode' => 'countryCode',
                'Error' => 'Error',
                'Upload Status' => 'Upload Status',
            ]
        ];

        return isset($cols[$this->bulk_upload_entity]) ? $cols[$this->bulk_upload_entity] : [];
    }

}

if (!function_exists('assignKeysToCsvRowsForBulk')) {
    function assignKeysToCsvRowsForBulk($selected_csv_columns_with_keys, $isCompared = false, $aggregate_types = [], $target_csv_file_handle, $entity, $column_types, $entity_custom_labels = [], $afterAPICall = "No")
    {
        $interpreter = new \Goodby\CSV\Import\Standard\Interpreter();
        $newarray = [];
        try {

            $interpreter->addObserver(function (array $row) use (&$csvHeader, $selected_csv_columns_with_keys, $isCompared, $aggregate_types, $target_csv_file_handle, $entity, $column_types, $entity_custom_labels, $afterAPICall) {
                if (!$csvHeader) {
                    $csvHeader = $row;
                } else {
                    $data = [];
                    foreach ($row as $key => $value) {
                        $data_base_key = $csvHeader[$key];
                        $data[$data_base_key] = $value;
                    }

                    foreach ($selected_csv_columns_with_keys as $column_key => $column_value) {
                        if (isset($data[$column_key])) {
                            if ($column_key == 'Error' && $entity == 'bulk') {
                                $data[$column_key] = $data[$column_key] == 0 ? "No" : "Yes";
                            }
                            if ($column_key == 'Upload Status' && $entity == 'bulk') {
                                if ($afterAPICall == "Yes") {
                                    $data[$column_key] = 'Upload successful';
                                } else {
                                    $upload_status = $data[$column_key];
                                    $upload_status = json_decode($upload_status, true);
                                    $data[$column_key] = isset($upload_status) && is_array($upload_status) ? implode(',', $upload_status) : "Upload successful";
                                }
                            }
                            $newarray[$column_key] = $data[$column_key];
                        }
                    }
                    fputcsv($target_csv_file_handle, $newarray);
                }
            });
        } catch (\Exception $e) {
            notifyBugsnagError('error in assign Keys To Csv Rows For Bulk [' . $e->getMessage() . ']', [
                'newarray' => $newarray,
                'entity' => $entity,
                'column_types' => $column_types,
            ]);
        }
        return $interpreter;
    }
}
