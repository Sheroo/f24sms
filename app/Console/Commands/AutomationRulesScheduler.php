<?php

namespace App\Console\Commands;

use App\Automation;
use App\Jobs\ProcessAutomationRuleJob;
use Illuminate\Console\Command;

class AutomationRulesScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automation:schedule
        {--rules= : Comma separated Rule Ids}
        {--q= : Queue Name}
        {--day= : Day Name}
        {--hour= : Hour}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Automation Rules Scheduler cron job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $rulesIds = [];
        if ($this->option('rules') != '') {

            $rules = explode(',', $this->option('rules'));

            foreach ($rules as $ruleId) {
                $ruleId = trim($ruleId . '');

                if (!empty($ruleId)) {
                    $rulesIds[] = $ruleId;
                }
            }

        } else {

            $day = is_null($this->option('day')) ? '' : $this->option('day');
            $hour = is_null($this->option('hour')) ? '' : $this->option('hour');
            $rulesIds = sms_scheduler_rules(false, $day, $hour);
        }

        $queue_name = $this->option('q') ? $this->option('q') : Q_RULE_SYNC;
        if (count($rulesIds) > 0) {
            $this->info('Scheduling rules [ ' . implode(",", $rulesIds) . ' ]');
            foreach ($rulesIds as $ruleId) {

                $ruleToRun = Automation::where('id', $ruleId)->first();
                if ($ruleToRun) {
                    $job = new ProcessAutomationRuleJob($ruleToRun->id);
                    $job->onQueue($queue_name);
                    dispatch($job);
                    $this->info('Job is dispatched for Rule ID = ' . $ruleId);
                } else {
                    $this->info('Rule not found  with ID = ' . $ruleId);
                }
            }

        }
    }
}
