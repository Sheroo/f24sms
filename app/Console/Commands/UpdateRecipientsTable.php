<?php

namespace App\Console\Commands;

use App\Jobs\UpdateRecipientsForValidEmailJob;
use Illuminate\Console\Command;

class UpdateRecipientsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inactive:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job to Inactive the recipients for Bounced or unverified emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new UpdateRecipientsForValidEmailJob();
        dispatch($job);
        $this->info('Job is dispatched for update recipients');
    }
}
