<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientBulkUploadTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_bulk_upload_recipient', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('Entity', 32)->nullable()->default('');
            $table->unsignedBigInteger('List Id')->nullable();
            $table->string('List Name', 255)->nullable()->default('');
            $table->string('firstName', 255)->nullable()->default('');
            $table->string('lastName', 255)->nullable()->default('');
            $table->string('email', 255)->nullable()->default('');
            $table->string('mobile', 64)->nullable()->default('');
            $table->string('countryCode', 12)->nullable()->default('');

            $table->unsignedTinyInteger('Error')->default(0);
            $table->text('Upload Status')->nullable();
            $table->text('Api Response')->nullable();

            $table->index('List Id');
            $table->index('Email');
            $table->index('mobile');
            $table->index('Error');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
