<?php

namespace App\Console\Commands;

use App\Automation;
use App\Jobs\ProcessAutomationRuleJob;
use App\Jobs\ProcessSegmentAutomationRuleJob;
use App\Segment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SegmentAutomationRulesScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'segment:automation:schedule
        {--rules= : Comma separated Rule Ids}
        {--q= : Queue Name}
        {--day= : Day Name}
        {--hour= : Hour}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Automation Rules Scheduler cron job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.9
     *
     * @return mixed
     */
    public function handle()
    {
        //checks the rules
        //get the segment id from rule
        //gets the recipients whose time has come
        //dispatch the job at this 10 minutes


        $rulesIds = [];
        if ($this->option('rules') != '') {

            $rules = explode(',', $this->option('rules'));

            foreach ($rules as $ruleId) {
                $ruleId = trim($ruleId . '');

                if (!empty($ruleId)) {
                    $rulesIds[] = $ruleId;
                }
            }

        } else {

            $day = is_null($this->option('day')) ? '' : $this->option('day');
            $hour = is_null($this->option('hour')) ? '' : $this->option('hour');
            //scheduler rules have been defined here and can be get from its query

            $rulesIds = sms_scheduler_rules(false, $day , $hour, $print_query = false, $segment = true);
        }


        $queue_name = $this->option('q') ? $this->option('q') : Q_SEGMENT_RULE_SYNC;
        if (count($rulesIds) > 0) {
            $this->info('Scheduling rules [ ' . implode(",", $rulesIds) . ' ]');
            foreach ($rulesIds as $ruleId) {

                $ruleToRun = Automation::where('id', $ruleId)->first();

                if ($ruleToRun) {
                    $job = new ProcessSegmentAutomationRuleJob($ruleToRun->id);
                    $job->onQueue($queue_name);
                    dispatch($job);
                    $this->info('Job is dispatched for Rule ID segmentation= ' . $ruleId);
                } else {
                    $this->info('Rule not found  with ID = ' . $ruleId);
                }
            }

        }
    }



}
