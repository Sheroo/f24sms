<?php

namespace App\Http\Controllers;

use App\Automation;
use App\Contracts\CrudInterface;
use App\Jobs\ProcessAutomationRuleJob;
use App\Jobs\ProcessSegmentAutomationRuleJob;
use App\Modules\Traits\HandleCrud;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class AutomationController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new Automation;
        $this->db_table = $this->model->getTable();
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['this.segment'] = ['sorting' => false, 'label' => 'segment'];

        $tableCols['action'] = [];
        $tableCols['this.schedule_interval'] = ['sorting' => false, 'label' => 'Schedule'];
        unset($tableCols['this.id']);
        unset($tableCols['this.list_id']);
        unset($tableCols['this.message_id']);
        unset($tableCols['this.scheduled_date']);
        unset($tableCols['this.scheduled_day']);
        unset($tableCols['this.scheduled_hour']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.first_time_run']);
        unset($tableCols['this.deleted_at']);
        unset($tableCols['this.email_service']);
        unset($tableCols['this.created_by']);
        unset($tableCols['this.segment_id']);
        unset($tableCols['this.segment_scheduler_hours']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('u.name as created_by');
        $cols[] = DB::raw('s.id as segment');
        $cols[] = DB::raw('s.clicked as clicked');
        $cols[] = DB::raw('s.optin as optin');
        $cols[] = DB::raw('s.abandoned as abandoned');
        $cols[] = DB::raw('s.memberp1 as memberp1');
        $cols[] = DB::raw('s.memberp2 as memberp2');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        $eloquentObj->leftJoin(DB::raw('segments s'), function ($join) {
            $join->on('this.segment_id', '=', 's.id');
        });
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('this.name', 'LIKE', $search_keyword);
//                $q = $q->orWhere('content', 'LIKE', $search_keyword);
            });
        }
        $eloquentObj->whereNull('deleted_at');
        return $eloquentObj;
    }

    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
//        dd($item_form_data);

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);


        return $response;
    }

    public function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'run':
                return $this->runAutomationRule($request);
                break;
            default:
                dd($request->all());
                break;
        }
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {
        $formData['lists'] = DB::table('lists')->whereNull('deleted_at')->orderBy('name', 'asc')->get();
        $formData['messages'] = DB::table('messages')->whereNull('deleted_at')->where('type', 'LIKE', 'sms')->orderBy('subject', 'asc')->get();
        $formData['emails'] = DB::table('messages')->whereNull('deleted_at')->where('type', 'LIKE', 'email')->orderBy('subject', 'asc')->get();
        $formData['segments'] = DB::table('segments')->get();

    }

    public function _prepare_item_eloquent_obj($itemEloquentObj, $data)
    {
        $itemDBCols = $itemEloquentObj->getTableColumns();
        //if this is email template then
        if (@$data['type'] == 'email') {
            $data['message_id'] = @$data['email_id'];
        }

        foreach ($data as $key => $value) {

            if (in_array($key, $itemDBCols)) {
                if ($key == 'start_date' || $key == 'end_date') {
                    if (!empty($value)) {
                        $value = Carbon::createFromFormat('n/j/y', $value)->toDateString();
                    }
                }
                $itemEloquentObj->$key = $value;
            }
            if ($key == "hours") {
                if (!empty($data['days'])) {
                    $itemEloquentObj->segment_scheduler_hours = $data['days']*24 + $data['hours'];
                }else{
                    $itemEloquentObj->segment_scheduler_hours = $data['hours'];
                }
            }
        }
        return $itemEloquentObj;
    }

    public function _post_store_action(Request $request, $itemEloquentObj, &$response)
    {
        /*
         * this object is store for the first time
         */



        if ($response['form_action'] == 'Create') {
            $first_time_run = $request->get('first_time_run');
            $schedule_type = $request->get('scheduled_type');
            if($schedule_type == 'list') {
                if (!empty($first_time_run)) {
                    $queue_name = Q_RULE_SYNC;
                    $job = new ProcessAutomationRuleJob($itemEloquentObj->id);
                    $job->onQueue($queue_name);
                    $job->delay($first_time_run);
                    dispatch($job);
                }
            }
//            if($schedule_type == 'segment') {
//                    $queue_name = Q_RULE_SYNC;
//                    $job = new ProcessSegmentAutomationRuleJob($itemEloquentObj->id);
//                    $job->onQueue($queue_name);
//                    $job->delay($first_time_run);
//                    dispatch($job);
//            }
        }
    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }

    public function runAutomationRule(Request $request)
    {
        $response = [];
        $response['status'] = 'fail';
        $ruleId = trim($request->get('id', ''));
        $ruleToRun = Automation::where('id', $ruleId)->first();
        if ($ruleToRun->scheduled_type == 'list') {

            $queue_name = Q_RULE_SYNC;
            $job = new ProcessAutomationRuleJob($ruleToRun->id);
            $job->onQueue($queue_name);
            dispatch($job);
            $response['message'] = 'Job is dispatched';
            $response['status'] = 'success';
        }
//        if ($ruleToRun->scheduled_type == 'segment') {
//            $queue_name = Q_RULE_SYNC;
//            $job = new ProcessSegmentAutomationRuleJob($ruleToRun->id);
//            $job->onQueue($queue_name);
//            dispatch($job);
//            $response['message'] = 'Segment Job is dispatched';
//            $response['status'] = 'success';
//        }
        return response()->json($response);
    }


}
