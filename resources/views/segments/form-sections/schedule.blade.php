<style>
    .modal-dialog{
        text-align: -webkit-center !important;
    }
    .modal-content{
        max-width: 421px !important;
    }
    .form-group{
        min-width: 600px !important;
    }
</style>
<div class="item-form-section schedule-section"
     data-slug="{{$controller_slug}}"
     data-section="schedule">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Manage Segments
        </h3>
    </div>
    <div class="item-form-section-body clearfix">



        <?php
//        $scheduled_date = @$itemObj->scheduled_date;
//        $start_date = @$itemObj->start_date;
//        $end_date = @$itemObj->end_date;
//        if (!empty($start_date)) {
//            $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date)->format('n/j/y');
//        }
//        if (!empty($end_date)) {
//            $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date)->format('n/j/y');
//        }
        ?>

       
{{--{{dd($itemObj)}}--}}
            <div class="form-group clearfix">

                <div class="col-sm-6">
                    <div >
                        <input type="checkbox" class="form-check-input" name="clicked" <?php echo e(is_null(@$itemObj) ? 'checked':(@$itemObj->clicked == 1? 'checked' : '')); ?> />
                        <label for="clicked">clicked</label>

                    </div>
                </div>
            </div>

            <div class="form-group clearfix">

                <div class="row">
                   <div class="col-sm-2">
                       <select name="optin_s" id="optin" class="select-picker form-control form-white te-input te-required">
                        <option value="-1" <?php echo e(@$itemObj->optin == -1 ? 'selected':''); ?>>-</option>
                        <option value="1" <?php echo e(@$itemObj->optin == 1 ? 'selected':''); ?>>+</option>
                        </select>
                   </div>
                    <div class="col-sm-6">
                        <label for="optin">
                            <input type="checkbox" class="form-check-input" name="optin" <?php echo e(@$itemObj->optin == 1 ? 'checked' : (@$itemObj->optin == -1? 'checked' : '')); ?> />
                        Optin Fundivia</label>

                    </div>
                </div>
            </div>
            <div class="form-group clearfix">

                <div class="row">
                   <div class="col-sm-2">
                       <select name="memberp1_S" id="memberp1" class="select-picker form-control form-white te-input te-required">
                           <option value="-1" <?php echo e(@$itemObj->memberp1 == -1 ? 'selected':''); ?>>-</option>
                           <option value="1" <?php echo e(@$itemObj->memberp1 == 1 ? 'selected':''); ?>>+</option>
                        </select>
                   </div>
                    <div class="col-sm-6">
                        <input type="checkbox" class="form-check-input" name="memberp1" <?php echo e(@$itemObj->memberp1 == 1 ? 'checked' : (@$itemObj->memberp1 == -1? 'checked' : '')); ?> />
                        <label for="optin">F24 Member P1</label>

                    </div>
                </div>
            </div>
            <div class="form-group clearfix">

                <div class="row">
                   <div class="col-sm-2">
                       <select name="memberp2_s" id="memberp2" class="select-picker form-control form-white te-input te-required">
                           <option value="-1" <?php echo e(@$itemObj->memberp2 == -1 ? 'selected':''); ?>>-</option>
                           <option value="1" <?php echo e(@$itemObj->memberp2 == 1 ? 'selected':''); ?>>+</option>
                        </select>
                   </div>
                    <div class="col-sm-6">
                        <input type="checkbox" class="form-check-input" name="memberp2" <?php echo e(@$itemObj->memberp2 == 1 ? 'checked' : (@$itemObj->memberp2 == -1? 'checked' : '')); ?> />
                        <label for="optin">F24 Member P2</label>

                    </div>
                </div>
            </div>
            <div class="form-group clearfix">

                <div class="row">
                   <div class="col-sm-2">
                       <select name="abandoned_s" id="abandoned" class="select-picker form-control form-white te-input te-required">
                           <option value="-1" <?php echo e(@$itemObj->abandoned == -1 ? 'selected':''); ?>>-</option>
                           <option value="1" <?php echo e(@$itemObj->abandoned == 1 ? 'selected':''); ?>>+</option>
                        </select>
                   </div>
                    <div class="col-sm-6">
                        <input type="checkbox" class="form-check-input" name="abandoned" <?php echo e(@$itemObj->abandoned == 1 ? 'checked' : (@$itemObj->abandoned == -1? 'checked' : '')); ?> />
                        <label for="optin">F24 Abandoned</label>

                    </div>
                </div>
            </div>


    </div>


</div>
