<div class="item-form-section personal_information-section"
     data-slug="{{$controller_slug}}"
     data-section="personal_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Personal Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Name</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="name"
                       value="{{@$itemObj->name}}"
                       type="text"
                       placeholder="Name"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-validate-email"
                       name="email"
                       value="{{@$itemObj->email}}"
                       type="text"
                       placeholder="Email"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required" type="password" value="" name="password"
                       placeholder="Password"/>
            </div>
        </div>
    </div>
</div>
