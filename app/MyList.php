<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyList extends Model
{
    use SoftDeletes;
    protected $table = 'lists';

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function recipients()
    {
        return $this->hasMany('App\Recipient', 'list_id', 'id');

    }
}
