#!/usr/bin/env bash

if [[ $# > 0 ]]
	then

    php artisan queue:work --queue=$1 --timeout=100000 $2

fi
#file end
