@extends('layouts.visitor')

@section('content')

    <!-- BEGIN LOGIN BOX -->

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="logo-img" src="{{te_asset('images/f24-logo-150x150.png')}}">
                <form method="POST" action="{{ te_make_url_scheme_less(route('password.update')) }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group row">
                        {{--                        <label for="email"--}}
                        {{--                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                        <div class="col-md-12">
                            <input id="email" placeholder="Email" type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ $email ?? old('email') }}" required autocomplete="email"
                                   autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{--                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                        <div class="col-md-12">
                            <input id="password" type="password" placeholder="New Password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{--                        <label for="password-confirm"--}}
                        {{--                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                        <div class="col-md-12">
                            <input id="password-confirm" placeholder="Confirm New Password" type="password"
                                   class="form-control"
                                   name="password_confirmation" required autocomplete="new-password">
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
