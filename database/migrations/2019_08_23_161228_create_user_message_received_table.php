<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMessageReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_message_received', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recipient_id')->nullable();
            $table->string('body', 280)->nullable();
            $table->string('from_phone', 12)->nullable();
            $table->string('to_phone', 12)->nullable();
            $table->string('from_country', 4)->nullable();
            $table->string('to_country', 4)->nullable();
            $table->string('message_sid', 64)->nullable();
            $table->timestamps();

            $table->index(['from_phone']);
            $table->index(['recipient_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_message_received');
    }
}
