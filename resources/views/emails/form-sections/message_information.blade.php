
<div class="item-form-section message_information-section"
     data-slug="{{$controller_slug}}"
     data-section="message_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Email Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Subject</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="subject"
                       value="{{@$itemObj->subject}}"
                       type="text"
                       placeholder="Subject"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Template ID</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input"
                       name="template_id"
                       value="{{@$itemObj->template_id}}"
                       type="text"
                       placeholder="Template ID"/>
            </div>
        </div>



        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Content</label>


            <div class="col-sm-9">
                <textarea id="editor1" name="content"
                          placeholder="Content">{{@$itemObj->content}}</textarea>

            </div>
        </div>

        <script>
            CKEDITOR.replace('editor1', {
                uiColor: '#4989DB'
            });

        </script>

    </div>
</div>
