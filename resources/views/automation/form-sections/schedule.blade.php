<div class="item-form-section schedule-section"
     data-slug="{{$controller_slug}}"
     data-section="schedule">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Automation Schedule
        </h3>
    </div>
    <div class="item-form-section-body clearfix">
        <div class="list_sec" <?php echo e(@$itemObj->scheduled_type != 'segment' ? '':'style=display:none') ?>>
            <div class="form-group clearfix">
                <label class="col-sm-4 control-label">Interval</label>
                <div class="col-sm-8">
                    <select name="schedule_interval" id="schedule_interval"
                            class="select-picker form-control form-white te-input te-required te-bind schedule_interval">
                        <option value="">Select Interval</option>
                        @foreach(te_schedule_intervals() as $scheduleIntervalKey => $scheduleIntervalLabel)
                            <option
                                    {{@$itemObj->schedule_interval==$scheduleIntervalKey ? 'selected="selected"':''}} value="{{$scheduleIntervalKey}}">{{$scheduleIntervalLabel}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <?php
            $scheduled_date = @$itemObj->scheduled_date;
            $start_date = @$itemObj->start_date;
            $end_date = @$itemObj->end_date;
            if (!empty($start_date)) {
                $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date)->format('n/j/y');
            }
            if (!empty($end_date)) {
                $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date)->format('n/j/y');
            }
            ?>

            <div class="form-group clearfix" te-show="schedule_interval=monthly">
                <label class="col-sm-4 control-label">Scheduled Date</label>
                <div class="col-sm-8">
                    <select name="scheduled_date" id="scheduled_date"
                            class="select-picker form-control form-white te-input te-required">
                        <option value="">Select Day of th month</option>
                        @foreach(te_scheduled_month_dates() as $scheduleDayKey => $scheduleDayName)
                            <option
                                    {{@$itemObj->scheduled_date==$scheduleDayKey ? 'selected="selected"':''}} value="{{$scheduleDayKey}}">{{$scheduleDayName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group clearfix" te-show="schedule_interval=weekly || schedule_interval=bi-weekly">
                <label class="col-sm-4 control-label">Scheduled Day</label>
                <div class="col-sm-8">
                    <select name="scheduled_day" id="scheduled_day"
                            class="select-picker form-control form-white te-input te-required">
                        <option value="">Select Interval</option>
                        @foreach(te_scheduled_week_days() as $scheduleDayKey => $scheduleDayName)
                            <option
                                    {{@$itemObj->scheduled_day==$scheduleDayKey ? 'selected="selected"':''}} value="{{$scheduleDayKey}}">{{$scheduleDayName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group clearfix"
                 te-show="schedule_interval=weekly || schedule_interval=daily || schedule_interval=monthly || schedule_interval=bi-weekly ">
                <label class="col-sm-4 control-label">Scheduled Hour</label>
                <div class="col-sm-8">
                    <select name="scheduled_hour" id="scheduled_hour"
                            class="select-picker form-control form-white te-input te-required">
                        <option value="">Select Interval</option>
                        @foreach(te_scheduled_hours() as $scheduleHourKey => $scheduleHourName)
                            <option
                                    {{@$itemObj->scheduled_hour==$scheduleHourKey ? 'selected="selected"':''}} value="{{$scheduleHourKey}}">{{$scheduleHourName}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-sm-4 control-label">Start Date</label>
                <div class="col-sm-8">
                    <input name="start_date" id="start_date"
                           class="datepicker form-control form-white te-input te-required"
                           value="{{@$start_date}}"/>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-sm-4 control-label">End Date</label>
                <div class="col-sm-8">
                    <input name="end_date" id="end_date"
                           class="datepicker form-control form-white te-input te-required"
                           value="{{@$end_date}}"/>
                </div>
            </div>

            @if (!isset($itemObj))
                <div class="form-group clearfix list_sec">
                    <label class="col-sm-4 control-label">Run This Automation</label>
                    <div class="col-sm-8">

                        <select name="first_time_run" id="first_time_run"
                                class="select-picker form-control form-white te-input ">
                            <option value="">Select Interval</option>
                            @foreach([ 1 => 'Immediately' , 300 => 'After 5 Minutes', 600 => 'After 10 Minutes', 1800 => 'After 30 Minutes' ] as $firstTimeKey => $firstTimeLabel)
                                <option
                                        {{@$itemObj->first_time_run==$firstTimeKey ? 'selected="selected"':''}} value="{{$firstTimeKey}}">{{$firstTimeLabel}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label">Gateway</label>
            <div class="col-sm-8">
                <select name="gateway" id="gateway"
                        class="select-picker form-control form-white te-input te-required sms_msg">
                    <option {{@$itemObj->gateway=='linkmobility' ? 'selected="selected"':''}} value="linkmobility">Link
                        Mobility
                    </option>
                    <option {{@$itemObj->gateway=='twilio' ? 'selected="selected"':''}} value="twilio">Twilio
                    </option>
                    <option {{@$itemObj->gateway=='messente' ? 'selected="selected"':''}} value="messente">Messente
                    </option>

                </select>

                <select name="email_service" id="email_service"
                        class="select-picker form-control form-white te-input te-required email_msg"
                        style="display: none;">

                    <option {{@$itemObj->email_service=='mailgun' ? 'selected="selected"':''}} value="mailgun">
                        Mailgun
                    </option>
                    <option {{@$itemObj->email_service=='sendgrid' ? 'selected="selected"':''}} value="sendgrid">
                        Sendgrid
                    </option>
                </select>


            </div>

        </div>

        <?php

        if (isset($itemObj)) {
            $days = @$itemObj->segment_scheduler_hours / 24;
            $days = round($days);
            $hours = fmod(@$itemObj->segment_scheduler_hours, 24);
        }
        ?>
        <div class="seg_sec" <?php echo e(@$itemObj->scheduled_type == 'segment' ? '':'style=display:none') ?>>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label" style="line-height:1.3;">Segment Scheduler Days</label>
            <div class="col-sm-8">
                <input name="days"
                       class="form-control form-white te-input"
                       type="number"
                       placeholder="Days"
                       value="<?php echo e(is_null(@$itemObj) ? 0 : $days)?>"/>
            </div>

        </div>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label" style="line-height:1.3;">Segment Scheduler Hours</label>
            <div class="col-sm-8">
                <select name="hours"
                        class="select-picker form-control form-white te-input">
                    <option value="">Select Hours</option>
                    <option value="0" <?php echo e(@$hours == "0" ? "selected" : "");?>>0 hour</option>
                    <option value="1" <?php echo e(@$hours == 1 ? "selected" : "");?> >1 hour</option>
                    <option value="2" <?php echo e(@$hours == 2 ? "selected" : "");?> >2 hours</option>
                    <option value="3" <?php echo e(@$hours == 3 ? "selected" : "");?> >3 hours</option>
                    <option value="4" <?php echo e(@$hours == 4 ? "selected" : "");?> >4 hours</option>
                    <option value="5" <?php echo e(@$hours == 5 ? "selected" : "");?> >5 hours</option>
                    <option value="6" <?php echo e(@$hours == 6 ? "selected" : "");?> >6 hours</option>
                    <option value="7" <?php echo e(@$hours == 7 ? "selected" : "");?> >7 hours</option>
                    <option value="8" <?php echo e(@$hours == 8 ? "selected" : "");?> >8 hours</option>
                    <option value="9" <?php echo e(@$hours == 9 ? "selected" : "");?> >9 hours</option>
                    <option value="10" <?php echo e(@$hours == 10 ? "selected" : "");?> >10 hours</option>
                    <option value="11" <?php echo e(@$hours == 11 ? "selected" : "");?> >11 hours</option>
                    <option value="12" <?php echo e(@$hours == 12 ? "selected" : "");?> >12 hours</option>
                    <option value="13" <?php echo e(@$hours == 13 ? "selected" : "");?> >13 hours</option>
                    <option value="14" <?php echo e(@$hours == 14 ? "selected" : "");?> >14 hours</option>
                    <option value="15" <?php echo e(@$hours == 15 ? "selected" : "");?> >15 hours</option>
                    <option value="16" <?php echo e(@$hours == 16 ? "selected" : "");?> >16 hours</option>
                    <option value="17" <?php echo e(@$hours == 17 ? "selected" : "");?> >17 hours</option>
                    <option value="18" <?php echo e(@$hours == 18 ? "selected" : "");?> >18 hours</option>
                    <option value="19" <?php echo e(@$hours == 19 ? "selected" : "");?> >19 hours</option>
                    <option value="20" <?php echo e(@$hours == 20 ? "selected" : "");?> >20 hours</option>
                    <option value="21" <?php echo e(@$hours == 21 ? "selected" : "");?> >21 hours</option>
                    <option value="22" <?php echo e(@$hours == 22 ? "selected" : "");?> >22 hours</option>
                    <option value="23" <?php echo e(@$hours == 23 ? "selected" : "");?> >23 hours</option>
                    <option value="24" <?php echo e(@$hours == 24 ? "selected" : "");?> >24 hours</option>
                </select>

            </div>

        </div>
        </div>


    </div>
</div>
