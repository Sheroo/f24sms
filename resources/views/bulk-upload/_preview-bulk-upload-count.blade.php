@if(is_array($uploadedEntitiesCount) && count($uploadedEntitiesCount) > 0)
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Entity</th>
            <th>Successful</th>
            <th>Skipped</th>
            <th>Error</th>
        </tr>
        </thead>
        <tbody>
        @foreach($uploadedEntitiesCount as $entityKey => $counts)
            <tr>
                <td>{{bulkUploadEntityNamesPlural($entityKey)}}</td>
                <td>{{ $counts['added'] }}</td>
                <td>{{ $counts['skipped'] }}</td>
                <td>{{ $counts['error'] }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>All Entities are skipped because of errors in data.</p>
@endif
