<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'http://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5LQNTBG');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <link rel="shortcut icon" href="{{te_asset('global/images/favicon.png')}}" type="image/png">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>F24 SMS</title>

    <?php
    $styles = [
        'global/css/style.css',
        'global/css/theme.css',
        'global/css/ui.css',
        'admin/layout1/css/layout.css',
        'plugins/sweetalert/sweetalert.css',
        'plugins/dropzone/basic.css',
        'plugins/dropzone/dropzone.css',
        'css/form.css',
        'css/bulk-upload.css',
        'css/custom.css',
    ];
    style($styles);
    script('global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js');
    ?>
    @yield('styles')
    @yield('head')
</head>
<!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
<!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
<!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
<!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
<!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
<!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
<!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
<!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
<!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

<!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
<!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

<!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
<!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
<!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
<!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
<!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
<!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
<!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
<!-- BEGIN BODY -->
<body class="fixed-topbar fixed-sidebar theme-sdtl color-blue">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LQNTBG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar">
        <div class="logopanel">
            <h1>
                <a href="{{te_route('dashboard')}}"></a>
            </h1>
        </div>
        <div class="sidebar-inner">

            @include('includes.sidebar')

        </div>
    </div>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span
                            class="menu__handle"><span>Menu</span></span></a>
                </div>
            </div>
            <div class="header-right">
                <ul class="header-menu nav navbar-nav">

                    <li class="dropdown" id="user-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username">Hi, {{@$currentuser->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
{{--                            <li>--}}
{{--                                <a href="#"><i class="icon-user"></i><span>My Profile</span></a>--}}
{{--                            </li>--}}
                            <li>
                                <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                        class="icon-logout"></i><span>Logout</span></a>
                            </li>
                            <form id="logout-form" action="{{te_route('logout')}}" method="POST" style="display: none;">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </form>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                    <!-- CHAT BAR ICON -->
                    {{--                    <li id="quickview-toggle"><a href="#"><i class="icon-bubbles"></i></a></li>--}}
                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">

            @yield('content')

            <div class="footer">
                <div class="copyright">
                    <p class="pull-left sm-pull-reset">
                        <span>Copyright <span class="copyright">©</span> 2019 </span>
                        <span>F24Media</span>.
                        <span>All rights reserved. </span>
                    </p>
                    <p class="pull-right sm-pull-reset">
                        <span><a href="#" class="m-r-10">Support</a> | <a href="#"
                                                                          class="m-l-10 m-r-10">Terms of use</a> | <a
                                href="#" class="m-l-10">Privacy Policy</a></span>
                    </p>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END MAIN CONTENT -->
</section>

@section('modals')
@show

<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>


<!-- END PRELOADER -->
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
@include('includes.js-variables')
<?php
$scripts = [
    'global/plugins/jquery/jquery-3.1.0.min.js',
    'global/plugins/jquery/jquery-migrate-3.0.0.min.js',
    'global/plugins/jquery-ui/jquery-ui.min.js',
    'global/plugins/gsap/main-gsap.min.js',
    'global/plugins/tether/js/tether.min.js',
    'global/plugins/bootstrap/js/bootstrap.min.js',
    'global/plugins/appear/jquery.appear.js',
    'global/plugins/jquery-cookies/jquery.cookies.min.js',
    'global/plugins/jquery-block-ui/jquery.blockUI.min.js',
    'global/plugins/bootbox/bootbox.min.js',
    'global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js',
    'global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js',
    'global/plugins/charts-sparkline/sparkline.min.js',
    'global/plugins/retina/retina.min.js',
    'global/plugins/select2/dist/js/select2.full.min.js',
    'global/plugins/icheck/icheck.min.js',
    'global/plugins/backstretch/backstretch.min.js',
    'global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js',
    'global/plugins/charts-chartjs/Chart.min.js',
    'global/js/builder.js',
    'global/js/sidebar_hover.js',
    'global/js/application.js',
    'global/js/plugins.js',
    'global/js/widgets/notes.js',
    'global/js/quickview.js',
    'global/js/pages/search.js',
    'admin/layout1/js/layout.js',
    'plugins/dropzone/dropzone.js',
    'global/plugins/ckeditor/ckeditor.js',

    'plugins/sweetalert/sweetalert.min.js',
    'plugins/te-bind/te-bind.js',

    'js/bulk-upload.js',
    'js/plugins.js',
    'js/crud.js',

];
script($scripts);
?>
@yield('scripts')
</body>
</html>
