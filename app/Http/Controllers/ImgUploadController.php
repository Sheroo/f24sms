<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\ImageUpload;

class ImgUploadController extends Controller
{
    public function showImgUpload()
    {
        return view('cs.img_upload');
    }

    public function Upload(Request $request)
    {

        $image = $request->file('img');


        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);

//        $imageUpload = new ImageUpload();
//        $imageUpload->filename = $imageName;
//        $imageUpload->save();
        dd($imageName);
        return response()->json(['success'=>$imageName]);
    }

}
