@extends('layouts.master')

@section('content')

    <div class="header">
        <h2>Manage <strong>Lists</strong></h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{te_route('dashboard')}}">Home</a>
                </li>
                <li class="active">Lists</li>
            </ol>
        </div>
    </div>

    <div class="row">

        <div class="portlets">
            <div class="{{$controller_slug}}-table-wrapper items-table-wrapper"
                 id="{{$controller_slug.'-table-wrapper'}}"
                 data-slug="{{$controller_slug}}">
                @include($controller_slug.'._list')
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include($controller_slug.'._form-modal')
    @include('bulk-upload._modal-bulk-upload-template')
@append

@section('scripts')
    <?php page_script([$controller_slug . '/index']);?>
@append
