<div class="item-form-section information-section"
     data-slug="{{$controller_slug}}"
     data-section="information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Automation Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Name</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="name"
                       value="{{@$itemObj->name}}"
                       type="text"
                       placeholder="Automation Name"/>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
                <select name="status" id="status"
                        class="select-picker form-control form-white te-input te-required">
                    <option {{(@$itemObj->status==1 || is_null(@$itemObj)) ? 'selected="selected"':''}} value="1">
                        Enabled
                    </option>
                    <option {{@$itemObj->status===0 ? 'selected="selected"':''}} value="0">Paused</option>
                </select>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Type</label>
            <div class="col-sm-9">
                <select name="type" id="type"
                        class="select-picker form-control form-white te-input te-required type">
                    <option {{@$itemObj->type=='sms' ? 'selected="selected"':''}} value="sms">SMS</option>
                    <option {{@$itemObj->type=='email' ? 'selected="selected"':''}} value="email">Email</option>
                </select>
            </div>
        </div>


    </div>
</div>
