function _create_form_callback__automation(success_resp, slug) {
    console.log(success_resp);

    jQuery('.te-item-form[data-slug=' + slug + ']').find('#schedule_interval').change();
    jQuery('.te-item-form[data-slug=' + slug + ']').find('#type').change();
}

function _edit_form_callback__automation(success_resp, slug) {
    console.log(success_resp);

    jQuery('.te-item-form[data-slug=' + slug + ']').find('#schedule_interval').change();
    jQuery('.te-item-form[data-slug=' + slug + ']').find('#type').change();
    validate_all_sections('automation');
}

function custom_item_action_automation_run(thisbtn) {

    swal({
            title: "Are you sure you want to run this automation?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, run it!",
            closeOnConfirm: true

        },
        function () {
            var slug = jQuery(thisbtn).attr('data-slug');
            var id = jQuery(thisbtn).attr('data-id');
            var serialized_data = jQuery('.' + slug + '-search-form').serializeToObject();
            serialized_data['id'] = id;
            serialized_data['method'] = 'post';
            var automation_run_action = prepareAjaxAction(slug);
            serialized_data = addAjaxActionToSerializedData(serialized_data, 'run');
            te_ajax(
                automation_run_action,
                serialized_data,
                function (resp) {
                    jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert(resp.message, 'success');
                },
                function (resp) {
                    jQuery('.items-table-wrapper[data-slug=' + slug + ']').te_hide_alert().te_show_alert(resp.message, 'danger');
                },
                function (error_resp) {
                },
                jQuery('.items-table-wrapper[data-slug=' + slug + '] .panel-content')
            );
        });

}

jQuery(document).ready(function () {

    jQuery(document).on('change', '.type', function (e) {

        e.preventDefault();

        var value = jQuery(this).val();

        if (value == 'sms') {

            jQuery('.email_msg').hide();
            jQuery('.sms_msg').show();

        } else if (value == 'email') {

            jQuery('.sms_msg').hide();
            jQuery('.email_msg').show();

        }
        validate_form_automation_section_list_message('automation', 'list_message');

    });
    jQuery(document).on('change', '.scheduled_type', function (e) {

        e.preventDefault();

        var value = jQuery(this).val();

        if (value == 'list') {

            jQuery('.seg_sec').hide();
            jQuery('.seg_sec').find('select , input').removeClass("te-required");
            jQuery('.list_sec').find('select , input').addClass("te-required");
            jQuery('#first_time_run').removeClass("te-required");
            jQuery('.list_sec').show();
            jQuery('#list_id').addClass("te-required");
            jQuery('#segment_id').removeClass("te-required");

        } else if (value == 'segment') {

            jQuery('.list_sec').hide();
            jQuery('.seg_sec').show();
            jQuery('.list_sec').find('select , input').removeClass("te-required");
            jQuery('.seg_sec').find('select , input').addClass("te-required");
            jQuery('#country').removeClass("te-required");
            jQuery('#list_id').removeClass("te-required");
            jQuery('#segment_id').addClass("te-required");

        }
        e.preventDefault();
        var itsForm = jQuery(this).parents('.te-item-form');
        var itsSlug = itsForm.data('slug');
        validate_all_sections(itsSlug);

        validate_form_automation_section_list_message('automation', 'list_message');

    });
});


function validate_form_automation_section_list_message(slug, section_name) {
    var thisForm = jQuery('.te-item-form[data-slug=' + slug + ']');
    var thisSection = thisForm.find('.item-form-section[data-section=' + section_name + ']');
    var scheduler_type = thisForm.find('#type').val();

    var message_id = '';
    var list_id = '';
    var seg_id = '';
    var schedule_req = false;

    if (scheduler_type == 'sms') {
        message_id = thisSection.find('#message_id').val();
    } else if (scheduler_type == 'email') {
        message_id = thisSection.find('#email_id').val();
    }

    if(thisSection.find('#list_id').hasClass('te-required')){
         list_id = thisSection.find('#list_id').val();

        if(list_id){
            schedule_req = true;
        }
    }else if(thisSection.find('#segment_id').hasClass('te-required')) {
         seg_id = thisSection.find('#segment_id').val();

        if(seg_id){
            schedule_req = true;
        }
    }else{
        schedule_req = true;
    }

    var okey = message_id != '' && schedule_req;

    // var okey = message_id!= '';
    if (okey) {
        thisSection.addClass('br-ok');
    } else {
        thisSection.removeClass('br-ok');
    }
    return okey;
}
