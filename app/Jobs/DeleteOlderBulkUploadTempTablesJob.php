<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class DeleteOlderBulkUploadTempTablesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteBulkUploadOlderTempTables();
    }

    private function deleteBulkUploadOlderTempTables()
    {
        $date = Carbon::yesterday();

        $table_name_clauses = [];
        $table_name_clauses[] = " table_name LIKE 'bu_temp_recipient_" . $date->format('Ymd') . "_%' ";
        for ($i = 1; $i <= 6; $i++) {
            $date = $date->subDay(1);
            $table_name_clauses[] = " table_name LIKE 'bu_temp_recipient_" . $date->format('Ymd') . "_%' ";
        }
        $table_name_clause = implode(' OR ', $table_name_clauses);

        $query = "SELECT table_name FROM information_schema.tables WHERE table_schema='" . env('DB_DATABASE') . "' AND (" . $table_name_clause . ")";
        try {
            $result = DB::select($query);
            foreach ($result as $res) {
                $DropQuery = "DROP TABLE IF EXISTS " . $res->table_name;
                DB::statement($DropQuery);
            }
        } catch (\Exception $e) {
            notifyBugsnagError('Error in deleting temp table', [
                'detail' => $e->getMessage(),
                'query' => $query,
            ], 'info');
        }
    }

}
