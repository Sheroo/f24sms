<form name="item_form" data-slug="{{$controller_slug}}" class="te-item-form" id="item_form" method="post">
    <div class="modal-header bg-primary">
        <h2>Automation Form</h2>
        <input type="hidden" name="id" id="item-id" value="{{@$itemObj->id}}"/>
        <a class="item-form-action-btn"
           data-action="save"
           data-slug="{{$controller_slug}}"
           id="save-item-btn"
           href="javascript:void(0)"> Save <i class=" zmdi zmdi-chevron-right"></i></a>
        <a class="item-form-action-btn"
           data-action="close"
           data-slug="{{$controller_slug}}"
           href="javascript:void(0)"><i
                class="zmdi zmdi-close"></i> Close</a>
    </div>
    <div class="modal-body item-change-table">

        <div class="panel-wrapper item-form-panel-body">
            <div class="clearfix item-form-controls-wrapper">
                <div class="row">
                    <div class="col-md-6 item-form-column">
                        @include('automation.form-sections.information')
                        @include('automation.form-sections.list_message')
{{--                        @include('automation.form-sections.segment')--}}
                    </div>
                    <div class="col-md-6 item-form-column">
                        @include('automation.form-sections.schedule')
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button class="btn item-action-btn" type="button" id="save-item-footer-btn" data-action="save">
            Save
        </button>
    </div>
</form>
