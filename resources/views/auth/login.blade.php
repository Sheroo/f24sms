@extends('layouts.visitor')

@section('content')

    <!-- BEGIN LOGIN BOX -->
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="logo-img" src="{{te_asset('images/f24-logo-150x150.png')}}">
{{--                <i class="user-img icons-faces-users-03"></i>--}}
                <form method="POST" action="{{ te_make_url_scheme_less(route('login')) }}">
                    @csrf

                    <div class="form-group row">
                        {{--                            <label for="email"--}}
                        {{--                                   class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                        <div class="col-md-12">
                            <input id="email" type="email"
                                   placeholder="Email"
                                   class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{--                            <label for="password"--}}
                        {{--                                   class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                        <div class="col-md-12">
                            <input id="password" type="password"
                                   placeholder="Password"
                                   class="form-control @error('password') is-invalid @enderror" name="password"
                                   required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    {{--                        <div class="form-group row">--}}
                    {{--                            <div class="col-md-6 offset-md-4">--}}
                    {{--                                <div class="form-check">--}}
                    {{--                                    <input class="form-check-input" type="checkbox" name="remember"--}}
                    {{--                                           id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                    {{--                                    <label class="form-check-label" for="remember">--}}
                    {{--                                        {{ __('Remember Me') }}--}}
                    {{--                                    </label>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}

                    <div class="form-group row mb-0">
                        <div class="col-lg-12 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ te_url('password/reset') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
