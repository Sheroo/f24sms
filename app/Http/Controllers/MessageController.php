<?php

namespace App\Http\Controllers;

use App\Contracts\CrudInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Message;
use App\Modules\Traits\HandleCrud;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller implements CrudInterface
{
    Use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new Message;
        $this->db_table = $this->model->getTable();
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();
        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = ['sorting' => false];
        unset($tableCols['this.id']);
        unset($tableCols['this.type']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.deleted_at']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('u.name as created_by');
        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $eloquentObj = $eloquentObj->where('type', 'LIKE', 'sms');
        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('subject', 'LIKE', $search_keyword);
                $q = $q->orWhere('content', 'LIKE', $search_keyword);
            });
        }
        $eloquentObj->whereNull('this.deleted_at');
        return $eloquentObj;
    }

    public function customAjaxActions(Request $request)
    {
        // TODO: Implement customAjaxActions() method.
    }


    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
        $item_form_data['type']='sms';
        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);


        return $response;
    }



    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }
}
