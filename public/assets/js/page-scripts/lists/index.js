jQuery(document).ready(function () {

    jQuery('body').on('click', '.btn-add-recipient', function (e) {
        jQuery('.recipient-info-wrapper').append(jQuery('.recipient-info-default').clone().removeClass('recipient-info-default'));
    });

    jQuery('body').on('click', '.delete_button', function (e) {
        jQuery(this).parents('.recipient-info').remove();
    });

});

function custom_item_action_lists_bulk_upload(itemActionBtn) {
    var id = jQuery(itemActionBtn).data('id');
    var slug = jQuery(itemActionBtn).data('slug');
    var action = jQuery(itemActionBtn).data('action');
    var bulk_upload_entity = jQuery(itemActionBtn).attr('data-bulk_upload_entity');
    var hidden_data = {
        list_id: id,
        bulk_upload_entity: bulk_upload_entity,
        current_slug: slug,
    };
    show_bulk_upload_first_step_modal(bulk_upload_entity, hidden_data);
}
