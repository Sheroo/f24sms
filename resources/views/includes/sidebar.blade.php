<ul class="nav nav-sidebar">
    @foreach ($sidebarMainNav as $navSectionindex => $navSection)
        <li class="{{$navSection['slug'] == $current_navitem_slug ? 'active':''}}">
            <a href="{{te_route($navSection['slug'])}}">
                <i class="{{$navSection['icon']}}"></i>
                <span>{{$navSection['name']}}</span>
            </a>
        </li>
    @endforeach
    {{--    <li><a href="{{te_route('lists')}}"><i class="fa fa-table"></i><span>Lists</span></a></li>--}}

    {{--    <li class="nav-parent nav-active active">--}}
    {{--        <a href=""><i class="fa fa-table"></i><span>Lists</span><span class="fa arrow"></span></a>--}}
    {{--        <ul class="children collapse">--}}
    {{--            <li class="active"><a href="tables.html"> Tables Styling</a></li>--}}
    {{--            <li><a href="tables-dynamic.html"> Tables Dynamic</a></li>--}}
    {{--            <li><a href="tables-filter.html"> Tables Filter</a></li>--}}
    {{--            <li><a href="tables-editable.html"> Tables Editable</a></li>--}}
    {{--        </ul>--}}
    {{--    </li>--}}
</ul>
