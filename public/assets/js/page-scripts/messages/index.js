jQuery(document).ready(function () {

    //checking when input values are changed
    jQuery('body').on('input change', '#message-content', function (e) {
        var content = jQuery(this).val();
        var length = content.length;
        var smsCount = Math.ceil(length / 140);
        var action = $(this).data('action');
        jQuery('.message-content-length').html(content.length + ' chars (' + smsCount + ' sms)');
    });
    jQuery('body').on('input change', '#unsub-content', function (e) {
        var content_unsub = jQuery(this).val();
        var length_unsub = content_unsub.length;
        var smsCount_unsub = Math.ceil(length_unsub / 140);

        jQuery('.unsub-content').html(content_unsub.length + ' chars (' + smsCount_unsub + ' sms)');
    });
});

function _edit_form_callback__messages(success_resp, slug) {
    var content = jQuery('.te-item-form[data-slug=' + slug + ']').find('#message-content').val();
    var content_unsub = jQuery('.te-item-form[data-slug=' + slug + ']').find('#unsub-content').val();
    var length = content.length;
    var length_unsub = content_unsub.length;
    var smsCount = Math.ceil(length / 140);
    var smsCount_unsub = Math.ceil(length_unsub / 140);
    jQuery('.message-content-length').html(content.length + ' chars (' + smsCount + ' sms)');
    jQuery('.unsub-content').html(content_unsub.length + ' chars (' + smsCount_unsub + ' sms)');
}
