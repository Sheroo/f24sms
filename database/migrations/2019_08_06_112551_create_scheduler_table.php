<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 255);
            $table->boolean('status');
            $table->string('schedule_interval', 12);
            $table->unsignedInteger('scheduled_date')->default(0);
            $table->string('scheduled_day', 32)->nullable();
            $table->unsignedInteger('scheduled_hour')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            $table->unsignedBigInteger('list_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('message_id')->nullable();

            $table->foreign('list_id')->references('id')->on('lists');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('message_id')->references('id')->on('messages');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler');
    }
}
