<?php

namespace App\Jobs;

use App\MailChimpContact;
use DrewM\MailChimp\MailChimp;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class UnsubscribeMailChimpContacts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Rune account api key
        $apiKey = env('MAILCHIMP_KEY');
        $mailChimp = new MailChimp($apiKey);
        $lists = $mailChimp->get('lists');

        $emails = [
            'henrik@front.no',
            'leo.hatonen@zmartagroup.com',
            'timo.vaittinen@zmartagroup.com',
            'monique.hoogervorst@zmartagroup.com',
            'karolina.svensson@zmartagroup.com',
            'karolina@zmartagroup.com',
            'nicolai.hobaek.jacobsen@apsis.com',
            'Fredrik@front.no',
            'simen@orionmedia.no',
            'jorn@gmail.com',
            'jorn.agnar.gundersen@gmail.com',
            'romsgausbrumund@dahl.no',
            'andreaslied@hotmail.com',
            'Force_kaisa@hotmail.com',
            'Jouko.lindholm@outlook.com',
            'jenewerts@live.com',
            'kaffekroken@mail.com',
            'h-svinge@online.no',
            'hertz.bronnoysund@hertz.no',
            'tho_bra@hotmail.com',
            'tho_bra@homail.com',
            'trinkvam@hotmail.com',
            'stefan.a.solvang@gmail.com',
            'stefansolvang12@hotmail.com',
            'kennethbrann7422@gmail.com',
            'hassanabba@outlook.com',
            'anette.lang@caproim.se',
            'anette.lang@caproim.se',
            'anne.nissinen@eksote.fi',
            'fabmo@online.no',
            'emil.marteby@hotmail.com',
            'cathy_samm@hotmail.com',
            'jetnisse@hotmail.com',
            'hybert@hotmail.no',
            'hanifa238@hotmail.com',
            'soren.ekdahl@uvtcmail.se',
            'js222@hotmail.com',
            'Carina.85@hotmail.com',
            'D.n@homemail.com',
            'arve@kransberg.no',
            'arve.apneseth@me.com',
            'Markusnyberg.1981@hotmail.com',
            'owe.nordstrom@soiltech.nu',
            'vonsan66@live.se',
            'soren.ekdahl@uvtcmail.se',
            'dan.sollien@glocalnet.net',
            'peter.asklund@sweco.se',
            'linda.linus@hotmail.com',
            'ingmarie.malmborg@gmail.com',
            'kroggen67@hotmail.com',
            'harjuk9@gmail.com',
            'andreemartinz@yahoo.com',
            'andreemartinz@yahoo.com',
            'liliann.idstrom@comhem.se',
            'ralsalihi@outlook.com',
            'elinor10@hotmail.no',
            'kundeservice@axofinans.no',
            'per_erik67@msn.com',
            'sabina24@live.se',
            'hr_alameen@yahoo.com',
            'mia_bue@msn.com',
            'tonjelindqvist89@gmail.com',
            'aune_lars@yahoo.com',
            'larsa6688@yahoo.no',
            'mariushansen30@gmail.com',
            'morten_juvdal@yahoo.no',
            'morten.juvdal@gmail.com',
            'jetnisse@hotmail.com',
            'wahlstrom.m@outlook.com',
            'linda.linus@hotmail.com',
            'joel.bodin@aurika.se',
            'hans.john@ecophon.se',
            'jan.erik.knudsen@fretex.no',
            'gu.marie@hotmail.com',
            'wigdis.simsoe@hotmail.com',
            'jakel2007@hotmail.com',
            'frankturesson2009@hotmail.com',
            'mrluckystrike123@protonmail.com',
            'm.eugeniagaona@hotmail.com',
            'bengt_georg@hotmail.com',
            'emilia_magnusson@hotmail.se',
            'tonytynkkynen@live.se',
            'asabogren@hotmail.se',
            'l-g.blomqvist@comhem.se',
            'lars-gunnar.blomqvist@comhem.se',
            'besnik@telifyab.se'
        ];
        if (isset($lists['lists'])) {

            foreach ($lists['lists'] as $listCol => $listValue) {
                $list_id = $listValue['id'];
                foreach ($emails as $email) {
                    try {
                        $subscriber_hash = MailChimp::subscriberHash($email);

                        //$result = $mailChimp->patch("lists/{$list_id}/members/" . md5(strtolower($email)));
                        echo "List Id " . $list_id . PHP_EOL;
                        echo "Email  " . $email . PHP_EOL;
                        echo "lists/$list_id/members/$subscriber_hash" . PHP_EOL;
                        $result = $mailChimp->put("lists/$list_id/members/$subscriber_hash", [
                            'status' => 'unsubscribed',
                            'email_address' => $email,
                        ]);

                        if (isset($result['id'])) {
                            echo "Email <" . $email . "> removed" . PHP_EOL;
                        } else {
                            echo "Email <" . $email . "> not removed" . PHP_EOL;
                        }
                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                        Log::info('---------------------------');
                    }
                }
            }
        }
    }
}
