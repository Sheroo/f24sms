<div class="item-form-section message_information-section"
     data-slug="{{$controller_slug}}"
     data-section="message_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            Message Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Subject</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="subject"
                       value="{{@$itemObj->subject}}"
                       type="text"
                       placeholder="Subject"/>
            </div>
        </div>


        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Content</label>
            <div class="col-sm-9">
                <textarea style="height: 150px;" id="message-content"
                          class="form-control form-white te-input te-required"
                          name="content"
                          data-action="content"
                          placeholder="Content">{{@$itemObj->content}}</textarea>
                <span style="float: right;font-style: italic;font-size: 11px;color: #777;"
                      class="message-content-length">0</span>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Url</label>
            <div class="col-sm-9">
                <input class="form-control form-white"
                       name="url"
                       value="{{@$itemObj->url}}"
                       type="text"
                       placeholder="Url"/>
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Unsubscribe Message</label>
            <div class="col-sm-9">
                <textarea style="height: 150px;" id="unsub-content"
                          class="form-control form-white"
                          name="unsub_content"
                          data-action="un-subscribe"
                          placeholder="Unsubscribe Message">{{@$itemObj->unsub_content}}</textarea>
                <span style="float: right;font-style: italic;font-size: 11px;color: #777;"
                      class="unsub-content">0</span>
            </div>
        </div>

    </div>
</div>
