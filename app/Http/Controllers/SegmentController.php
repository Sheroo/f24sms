<?php

namespace App\Http\Controllers;
use App\Segment;
use App\Contracts\CrudInterface;
use App\Jobs\ProcessAutomationRuleJob;
use App\Modules\Traits\HandleCrud;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class SegmentController extends Controller implements CrudInterface
{
    use HandleCrud;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new Segment;
        $this->db_table = $this->model->getTable();
//        dd($this->tableColumns());
    }

    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();

        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }
        $tableCols['action'] = [];
        unset($tableCols['this.id']);
        unset($tableCols['this.updated_at']);
        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.*');
        $cols[] = DB::raw('u.name as created_by');

        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));
//        if (!empty($search_keyword)) {
//            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
//                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
//                $q = $q->where('subject', 'LIKE', $search_keyword);
//                $q = $q->orWhere('content', 'LIKE', $search_keyword);
//            });
//        }
//        $eloquentObj->whereNull('deleted_at');
        return $eloquentObj;
    }

    public function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'run':
                return $this->runAutomationRule($request);
                break;
            default:
                dd($request->all());
                break;
        }
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {
//        $formData['lists'] = DB::table('lists')->whereNull('deleted_at')->orderBy('name', 'asc')->get();
//        $formData['messages'] = DB::table('messages')->whereNull('deleted_at')->where('type', 'LIKE', 'sms')->orderBy('subject', 'asc')->get();
//        $formData['emails'] = DB::table('messages')->whereNull('deleted_at')->where('type', 'LIKE', 'email')->orderBy('subject', 'asc')->get();
    }

    public function _prepare_item_eloquent_obj($itemEloquentObj, $data)
    {
        $itemDBCols = $itemEloquentObj->getTableColumns();
        //if this is email template then
        if (@$data['type'] == 'email') {
            $data['message_id'] = @$data['email_id'];
        }

        foreach ($data as $key => $value) {
//dd($value);
            if (in_array($key, $itemDBCols)) {
                if ($key == 'start_date' || $key == 'end_date') {
                    if (!empty($value)) {
                        $value = Carbon::createFromFormat('n/j/y', $value)->toDateString();
                    }
                }
                if($key == "optin"){
                    if($value == "on"){
                        $itemEloquentObj->$key = $data['optin_s'];
                    }
                }elseif ($key == "memberp1"){
                    if($value == "on"){
                        $itemEloquentObj->$key = $data['memberp1_S'];
                    }
                }elseif ($key == "memberp2"){
                    if($value == "on"){
                        $itemEloquentObj->$key = $data['memberp2_s'];
                    }
                }elseif ($key == "abandoned"){
                    if($value == "on"){
                        $itemEloquentObj->$key = $data['abandoned_s'];
                    }
                }elseif ($key == "clicked"){
                    if($value == "on"){
                        $itemEloquentObj->$key = 1;
                    }
                }else{
                $itemEloquentObj->$key = $value;
                }
            }
        }
        return $itemEloquentObj;
    }

    /**
     * Store a newly created resource in storage.
     * Update an already existing resource in database
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response| array
     */
    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();
//        dd($item_form_data);

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
//        dd($itemEloquentObj);
        $itemEloquentObj->save();


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $this->_post_store_action($request, $itemEloquentObj, $response);

        $response = $this->getItemsData($request, true);


        return $response;
    }

    public function _post_store_action(Request $request, $itemEloquentObj, &$response)
    {
//        /*
//         * this object is store for the first time
//         */
//        if ($response['form_action'] == 'Create') {
//            $first_time_run = $request->get('first_time_run');
//
//
//            if (!empty($first_time_run)) {
//                $queue_name = Q_RULE_SYNC;
//                $job = new ProcessAutomationRuleJob($itemEloquentObj->id);
//                $job->onQueue($queue_name);
//                $job->delay($first_time_run);
//                dispatch($job);
//            }
//        }
    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        return $viewData;
    }

    public function runAutomationRule(Request $request)
    {
//        $response = [];
//        $response['status'] = 'fail';
//
//
//        $ruleId = trim($request->get('id', ''));
//        $ruleToRun = Automation::where('id', $ruleId)->first();
//        if ($ruleToRun) {
//
//            $queue_name = Q_RULE_SYNC;
//            $job = new ProcessAutomationRuleJob($ruleToRun->id);
//            $job->onQueue($queue_name);
//            dispatch($job);
//            $response['message'] = 'Job is dispatched';
//            $response['status'] = 'success';
//        }
//        return response()->json($response);
    }
}
