<?php

namespace App\Http\Controllers;

use App\Contracts\BulkUploadInterface;
use App\Contracts\CrudInterface;
use App\Message;
use App\Modules\Traits\HandleBulkUpload;
use App\Modules\Traits\HandleCrud;
use App\MyList;
use App\Recipient;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class ListController extends Controller implements CrudInterface, BulkUploadInterface
{
    Use HandleCrud;
    Use HandleBulkUpload;

    /*
    * this page need auth middle. we did not apply any middleware on this page on route file
    * as this page needs to show to manager and admin user type
    */
    public function __construct()
    {
        $this->middleware('auth');
        $this->setDefaultCrudConfiguration();
        $this->model = new MyList;
        $this->db_table = $this->model->getTable();
    }


    public function tableColumns()
    {
        $columns = $this->model->getTableColumns();

        $tableCols = [];
        foreach ($columns as $col) {
            $tableCols["this." . $col] = ['sorting' => true];
        }

        $tableCols['total'] = ['sorting' => false];
        $tableCols['action'] = ['sorting' => false];
        unset($tableCols['this.id']);
        unset($tableCols['this.updated_at']);
        unset($tableCols['this.deleted_at']);

        return $tableCols;
    }

    public function selectColumns(Request $request, $eloquentObj)
    {
        $cols = [];
        $cols[] = DB::raw('this.id as id');
        $cols[] = DB::raw('this.name as name');
        $cols[] = DB::raw('u.name as created_by');
        $cols[] = DB::raw('this.created_at as created_at');
        $cols[] = DB::raw('this.updated_at as updated_at');
        $cols[] = DB::raw('count(r.list_id) as total');

        $eloquentObj = $eloquentObj->select($cols);
        return $eloquentObj;
    }

    public function applyJoins(Request $request, $eloquentObj)
    {
        $eloquentObj->leftJoin(DB::raw('users u'), function ($join) {
            $join->on('this.created_by', '=', 'u.id');
        });
        $eloquentObj->leftJoin(DB::raw('recipients r'), function ($join) {
            $join->on('this.id', '=', 'r.list_id');
        });
        $eloquentObj->groupBy('this.id');
        return $eloquentObj;
    }


    public function searchItemsDataByKeyword(Request $request, $eloquentObj)
    {
        $search_keyword = trim($request->get('search_keyword', ''));
        if (!empty($search_keyword)) {
            $eloquentObj = $eloquentObj->where(function ($q) use ($search_keyword) {
                $search_keyword = DB::raw("'%" . db_esc_like_raw(strtolower($search_keyword)) . "%'");
                $q = $q->where('name', 'LIKE', $search_keyword);
            });
        }
        $eloquentObj->whereNull('this.deleted_at');
        return $eloquentObj;
    }

    public
    function customAjaxActions(Request $request)
    {
        $action = $request->get('ajax_action');
        switch ($action) {
            case 'ajax_upload_entities':
                return $this->uploadEntities($request);
                break;
            case 'ajax_upload_entity_data':
                return $this->uploadEntityData($request);
                break;
            case 'ajax_download_bulkupload_summary_csv':
                return $this->downloadBulkUploadSummaryCSV($request);
                break;
            default:
                dd($request->all());
                break;
        }
    }

    public function appendCreateEditFormData(Request $request, $itemObj, &$formData)
    {

    }

    public
    function store(
        Request $request
    )
    {

        $cuser = Auth::user();
        $response['status'] = 'success';
        $response['message'] = 'Item saved successfully';
        $response['form_action'] = 'Create';

        $item_form_data = Input::all();

        $itemEloquentObj = clone($this->model);
        if (!empty($item_form_data['id'])) {

            $alreadyExistedItem = clone($this->model);
            $alreadyExistedItem = $alreadyExistedItem->where('id', $item_form_data['id'])->first();
            $itemEloquentObj = $alreadyExistedItem ? $alreadyExistedItem : $itemEloquentObj;
            $response['form_action'] = 'Edit';
        }

        $itemEloquentObj = $this->_prepare_item_eloquent_obj($itemEloquentObj, $item_form_data);
        $itemEloquentObj->created_by = $cuser->id;
        $itemEloquentObj->save();

//        foreach ($item_form_data as $) {
//
//        }


        $request->merge(['itemid' => $itemEloquentObj->id]);

        $response = $this->getItemsData($request, true);

        return response()->json($response);
    }

    public function appendViewDataInGetItemData(Request $request, $viewData)
    {
        //dd($viewData);
        return $viewData;
    }



}
