<?php

namespace App\Contracts;

use Illuminate\Http\Request;

interface BulkUploadInterface
{
    public function getEntitiesToBeUploaded();

    public function getRequiredColumnsForCSV();

    public function storeBulkDataInDatabase($temp_table, $api_response_table);
}
