<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

function te_bulk_upload_page_slug($slug)
{
    $slugs = [
        'lists' => 'lists',
    ];
    return isset($slugs[$slug]) ? $slugs[$slug] : $slug;
}

function te_bulk_upload_csv_url($slug)
{
    $slugs = [
        'lists' => 'recipient-bulk-upload-sample-file.csv',
    ];
    $generalBulkUploadSampleFile = 'recipient-bulk-upload-sample-file.csv';
    return isset($slugs[$slug]) ? te_asset($slugs[$slug]) : te_asset($generalBulkUploadSampleFile);
}

function bulkUploadEntityAttributeMapping($entity = '')
{
    $cols = [
        ENTITY_RECIPIENT => [
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'email' => 'email',
            'mobile' => 'phone',
            'countryCode' => 'phone',
            'List Id' => 'list_id',
        ]
    ];

    if (!empty($entity)) {
        $cols = isset($cols[$entity]) ? $cols[$entity] : $cols;
    }

    return $cols;
}

function bulkUploadEntityColsEmptyValCheck($entity = '', $controller_slug = '')
{
    $cols = [];

    $recipient = [];
    $recipient[] = 'List Id';
    $recipient[] = 'List Name';
    $recipient[] = 'firstName';
    $recipient[] = 'lastName';
    $recipient[] = 'mobile';
    $recipient[] = 'countryCode';
    $recipient[] = 'email';

    $cols[ENTITY_RECIPIENT] = $recipient;

    if (!empty($entity)) {
        $cols = isset($cols[$entity]) ? $cols[$entity] : $cols;
    }
    return $cols;
}

function bulkUploadEntityNames($entity = '')
{
    $cols = [
        ENTITY_RECIPIENT => 'Recipient',
    ];

    if (!empty($entity)) {
        $cols = isset($cols[$entity]) ? $cols[$entity] : $cols;
    }

    return $cols;
}

function bulkUploadEntityNamesPlural($entity = '')
{
    $cols = [
        ENTITY_RECIPIENT => 'Recipients',
    ];

    if (!empty($entity)) {
        $cols = isset($cols[$entity]) ? $cols[$entity] : $cols;
    }

    return $cols;
}

if (!function_exists('prepare_dummy_response_for_error_from_api')) {
    function prepare_dummy_response_for_error_from_api($response, $requestData)
    {
        $errorResponseDummy = [];
        foreach ($requestData as $index => $data) {
            $errorResponseDummy[] = $response;
        }
        return $errorResponseDummy;
    }
}

if (!function_exists('prepare_recipient_for_api_call')) {
    function prepare_recipient_for_api_call($_recipient, $action)
    {
        $recipient = [];
        $entityAttrs = bulkUploadEntityAttributeMapping(ENTITY_RECIPIENT);
        foreach ($entityAttrs as $index => $apiColName) {
            if (!empty($_recipient[$index])) {
                $value = trim($_recipient[$index]);
                if ($index == 'Campaign Start Date' || $index == 'Campaign End Date') {
                    $value = Carbon::parse($_recipient[$index])->format('Y-m-d');
                }
                $recipient[$apiColName] = $value;
            }
        }

        return $recipient;
    }
}

if (!function_exists('bulk_upload_chunk_size')) {
    function bulk_upload_chunk_size($entity = '')
    {
        $chunkSize = [
            ENTITY_RECIPIENT => 100,
        ];
        return empty($entity) ? $chunkSize : (isset($chunkSize[$entity]) ? $chunkSize[$entity] : 100);
    }
}

function prepare_load_infile_query_for_bulk_upload_temp_table($csvColumnsMapping, $csvHeader, $tempTableName, $fileName, $fieldTermination = ",", $lineTermination = "\\r\\n", $character_set = 'UTF8')
{
    $queryTemplate = " LOAD DATA LOCAL INFILE '{amazon_data_file}' INTO TABLE {amazon_data_table} CHARACTER SET {$character_set} FIELDS TERMINATED BY '{$fieldTermination}' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '{$lineTermination}' IGNORE 1 LINES ";

    $loadInFileColumns = getBulkUploadTempTableColumnsToImport($csvHeader, $csvColumnsMapping);
    $queryTemplate = str_replace('{amazon_data_table}', $tempTableName, $queryTemplate);   /*create staging table and load data*/

    $queryTemplate .= $loadInFileColumns['columns'] . " SET " . $loadInFileColumns['format'];
    $queryTemplate = str_replace('{amazon_data_file}', $fileName, $queryTemplate);

    return $queryTemplate;
}

function getBulkUploadTempTableColumnsToImport($csvHeader, $columns_mapping)
{

    $formattingColumns = [];
    foreach ($csvHeader as $col) {
        if (in_array($col, $columns_mapping)) {
            $formattingColumns[$col] = $col;
        }
    }

    $columnsForImport = '(';
    $columnsFormating = ' ';
    foreach ($csvHeader as $oneColumn) {
        if (in_array($oneColumn, $columns_mapping)) {
            $columnsForImport .= '@`' . $oneColumn . '`,';
        }
    }
    foreach ($formattingColumns as $colKey => $oneColumn) {
        $columnsFormating .= '`' . $colKey . '` = TRIM(@`' . $oneColumn . '`) ,';
    }
    $columnsForImport = rtrim($columnsForImport, ',');
    $columnsFormating = rtrim($columnsFormating, ',');
    $columnsForImport .= ')';

    return ['columns' => $columnsForImport, 'format' => $columnsFormating];
}

function getBulkUploadTempTableName($user_id, $bulk_upload_entity)
{
    return $TempTableName = "bu_temp_" . $bulk_upload_entity . "_" . date('Ymd') . '_' . $user_id . '_' . time();
}

function getBulkUploadTempMainTableName($temp_table)
{
    return $temp_table . '_main';
}

function createBulkUploadTempTable($bulk_upload_entity, $tempTableName)
{
    if (te_compare_strings(ENTITY_RECIPIENT, $bulk_upload_entity)) {
        $template_table_name = strtolower('template_bulk_upload_' . ENTITY_RECIPIENT);
    }
    $query = "CREATE TABLE IF NOT EXISTS " . $tempTableName . "  LIKE $template_table_name;";
    \Illuminate\Support\Facades\DB::statement($query);
    return $tempTableName;
}

function createBulkUploadChangeHistoryTempTable($temp_table_name, $connection)
{
    $TempTableName = '';

    $template_table_name = 'template_bulkupload_change_history';
    $query = "CREATE TABLE IF NOT EXISTS " . $temp_table_name . "  LIKE $template_table_name;";
    \Illuminate\Support\Facades\DB::connection($connection)->statement($query);

    return $TempTableName;
}

function checkUserAccountAccess(\App\Account $thisAccount, $user)
{
    $hasAccess = false;

    if (in_array($user->role, [ROLE_ADMIN, ROLE_CLIENT_SUPPORT])) {
        $hasAccess = true;
    } else if ($user->role == ROLE_ANALYST) {
        if ($thisAccount->account_type == ACCOUNT_INTERNAL) {
            $hasAccess = true;
        }
    } else if ($user->role == ROLE_EXTERNAL || $user->role == ROLE_CLIENT_VIEW || $user->role == ROLE_CLIENT_FULL || $user->role == ROLE_CLIENT_SUPER) {
        $usersAccountExist = \App\UserAccounts::where('account_id', $thisAccount->id)
            ->where('user_id', $user->id)
            ->count();
        if ($usersAccountExist > 0) {
            $hasAccess = true;
        }
    }

    return $hasAccess;
}

function changeOrderOfAssocArraySameAsOtherArray($array, $colOrder)
{
    $ordersArray = [];
    foreach ($colOrder as $col) {
        if (isset($array[$col])) {
            $ordersArray[$col] = $array[$col];
        } else {
            $ordersArray[$col] = '';
        }
    }
    return $ordersArray;
}

function allAvailableConnections()
{
    $accountAdminConfigs = new \App\DatabaseConfig();
    $accountAdminConfigs = $accountAdminConfigs->join(DB::raw("accounts as acc"), function ($join) {
        $join->on('acc.db_config_id', '=', 'database_config.id');
    });
    $accountAdminConfigs = $accountAdminConfigs->select(['write_host', 'acc.id as account_id', 'database', DB::raw("concat(write_host,'_',`database`) as `host`")])->get();
    $accountAdminConfigs = $accountAdminConfigs->groupBy('host')
        ->map(function ($value) {
            return collect($value)->pluck('acc.id');
        })->toArray();
    $accountAdminConfigs[env('WRITE_HOST', 'mysql') . '_' . env('DB_DATABASE', 'ams')] = [];
    return $accountAdminConfigs;
}

function adminConfigOfProvidedAccounts($accountIds)
{

    $accountAdminConfigs = new \App\DatabaseConfig();
    $accountAdminConfigs = $accountAdminConfigs->join(DB::raw("accounts as acc"), function ($join) {
        $join->on('acc.db_config_id', '=', 'database_config.id');
    });
    $accountAdminConfigs = $accountAdminConfigs
        ->select(['write_host', 'acc.id as account_id', 'database', DB::raw("concat(write_host,'_',`database`) as `host`")])
        ->get();
    $accountAdminConfigsCount = $accountAdminConfigs->count();
    $adminConfigAccountIds = (clone $accountAdminConfigs)->pluck('account_id')->toArray();
    $accountAdminConfigs = $accountAdminConfigs->groupBy('host')
        ->map(function ($value) {
            return collect($value)->pluck('account_id');
        })->toArray();
    // if all accounts in the file have sepearate DB, then we will not add main DB
    if ($accountAdminConfigsCount != count($accountIds)) {
        $accountAdminConfigs[env('WRITE_HOST', 'mysql') . '_' . env('DB_DATABASE', 'ams')] = [];
    }
    return [$accountAdminConfigs, $adminConfigAccountIds];
}

function labelsExportedHeaderBulkUpload()
{
    return ['campaignId', 'label', 'color', 'entity', 'channel_id', 'account_id', 'user_id', 'delete_label'];
}

function summaryFileNameToUploadOnS3($tempTableName)
{
    return 'BulkUpload-Processing-Summary-' . $tempTableName . '.csv';
}

if (!function_exists('validateAndDecodeTargetExpressionFromCSV')) {
    function validateAndDecodeTargetExpressionFromCSV($targetExpression)
    {
        $valid = true;
        $targetExpression = trim($targetExpression);

        $expressionArray = [];

        if (strpos($targetExpression, '=') !== false) {

            $operator = '=';

            $parsedExpression = explode($operator, $targetExpression);

            if (count($parsedExpression) == 2) {
                $precidate = trim($parsedExpression[0]);
                $value = trim($parsedExpression[1]);
                $value = str_replace('"', '', $value);
                if (te_compare_strings($precidate, 'asin') && !empty($value)) {
                    $expressionArray[] = [
                        'type' => 'asinSameAs',
                        'value' => $value
                    ];
                } else {
                    $valid = false;
                }

            } else {
                $valid = false;
            }

        } else {
            $valid = false;
        }

        return [$valid, $valid ? $expressionArray : $targetExpression];
    }
}
