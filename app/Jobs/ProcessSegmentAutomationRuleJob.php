<?php

namespace App\Jobs;

use Aloha\Twilio\Twilio;
use App\Automation;
use App\Recipient;
use App\SchedulerDeliveryReport;
use App\SchedulerRunHistory;
use App\Segment;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessSegmentAutomationRuleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $rule_id;
    public $rule;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($rule_id)
    {
        $this->rule_id = $rule_id;
        $this->onQueue(Q_SEGMENT_RULE_SYNC);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        Log::info('Running automation for Segment Id ' . $this->rule_id);
        $this->rule = Automation::where('id', $this->rule_id)->first();
        $segment = Segment::where('id', $this->rule->segment_id)->first();

        $clicked = $segment->clicked;
        $optin = $segment->optin;
        $memberp1 = $segment->memberp1;
        $memberp2 = $segment->memberp2;
        $abandoned = $segment->abandoned;

        $recipients_clicked = $this->join_for_segment(TAG_CLICKED);
        $recipients_optin = $this->join_for_segment(TAG_OPTIN);
        $recipients_memberp1 = $this->join_for_segment(TAG_MEMBER_P1);
        $recipients_memberp2 = $this->join_for_segment(TAG_MEMBER_P2);
        $recipients_abondoned = $this->join_for_segment(TAG_ABONDONED);
        $recipients = [];
        if ($clicked == 1) {
            $recipients = $recipients_clicked;
        } else if ($optin == 1) {
            $recipients = $recipients_optin;
        } else if ($memberp1 == 1) {
            $recipients = $recipients_memberp1;
        } else if ($memberp2 == 1) {
            $recipients = $recipients_memberp2;
        } else if ($abandoned == 1) {
            $recipients = $recipients_abondoned;
        }

        $recipients = $recipients == 1 ? [] : $recipients;
        if (count($recipients) > 0) {
            if ($clicked == -1) {
                $recipients = array_diff($recipients, $recipients_clicked);
            }
            if ($optin == -1) {
                $recipients = array_diff($recipients, $recipients_optin);
            }


            if ($memberp1 == -1) {
                $recipients = array_diff($recipients, $recipients_memberp1);
            }
            if ($memberp2 == -1) {
                $recipients = array_diff($recipients, $recipients_memberp2);
            }

            if ($abandoned == -1) {
                $recipients = array_diff($recipients, $recipients_abondoned);
            }
        }

        if ($this->rule) {

            $ruleHistory = SchedulerRunHistory::create(['scheduler_id' => $this->rule->id]);

            $list = $this->rule->mylist;
            $seg = $this->rule->segment_id;

            if ($seg) {

                $message = $this->rule->message;
                if ($message) {

                    if ($message->type == 'email') {

                        $cols = [];
                        $cols[] = DB::raw('id');
                        $cols[] = DB::raw('email as email');

                        $query = "select r.* from recipients r left join bounced_email b on b.email=r.email where list_id='{$list->id}' and b.id is null and r.email is not null and r.active=1";
                        $recipientsList = DB::select($query);

                        $ruleHistory->total_recipients = count($recipientsList);
                        $ruleHistory->save();

                        $chunks = createChunksOfData($recipientsList, 500);

                        foreach ($chunks as $chunk) {

                            $templateId = empty($message->template_id) || is_null($message->template_id) || !isset($message->template_id) ? '' : $message->template_id;
                            if ($this->rule->email_service == 'sendgrid') {

                                $response = sendEmails(collect($chunk)->pluck('email')->toArray(), $message->subject, $message->content, $templateId, $ruleHistory->id, $list->id);
                                $header = $response->headers();
                                $sendGridHeaderParts = explode(':', @$header[5]);


                            } else {

                                $response = send_email_through_mailgun(collect($chunk)->pluck('email')->toArray(), $message->subject, $message->content, $templateId, $ruleHistory->id, $list->id);
                                $sendGridHeaderParts[1] = rtrim(ltrim($response->getId(), '<'), '>');

                            }

                            $sgMessageId = @$sendGridHeaderParts[1];

                            //some time we get $sgMessageId =  application/json . so let's check that

                            if (stripos($sgMessageId, 'json') !== false) {
                                Log::info('Running automation for Id ' . $this->rule_id . " --> Header info");
                                Log::info($header);

                                try {
                                    Log::info('Running automation for Id ' . $this->rule_id . " --> Body info");
                                    Log::info($response->body());
                                } catch (\Exception $e) {
                                }

                                try {

                                    Log::info('Running automation for Id ' . $this->rule_id . " --> Complete response");
                                    Log::info($response);
                                } catch (\Exception $e) {
                                }
                            }
                            /*
                             * save to database
                             */
                            $dbInsertChunks = createChunksOfData($chunk, 500);
                            foreach ($dbInsertChunks as $dbInsertChunk) {
                                $SchedulerDeliveryReportData = [];
                                foreach ($dbInsertChunk as $oneRecipient) {
                                    $SchedulerDeliveryReportData[] = [
                                        'scheduler_history_id' => $ruleHistory->id,
                                        'recipient_id' => $oneRecipient->id,
                                        'message_sid' => $sgMessageId,
                                        'message_status' => 'not sent',
                                        'created_at' => Carbon::now()->toDateTimeString(),
                                        'updated_at' => Carbon::now()->toDateTimeString(),
                                    ];
                                }
                                SchedulerDeliveryReport::insert($SchedulerDeliveryReportData);
                            }

                        }


                    } elseif ($message->type == 'sms') {

                        if ($this->rule->scheduled_type == 'list') {
                            $cols = [];
                            $cols[] = DB::raw('id');
                            $cols[] = DB::raw('phone as phone');
                            $recipientsList = $list->recipients()
                                ->where([['active', '=', 1], ['list_id', '<>', 454],])
                                ->select($cols)
                                ->get()
                                ->toArray();
                        }
                        if ($this->rule->scheduled_type == 'segment') {

                            $hours = $this->rule->segment_scheduler_hours;
                            $country = $this->rule->country;
                            if(is_null($country)){
                                $country = '%';
                            }
                            $scheduler_now = $this->rule->created_at;
                            $recipients_where = count($recipients)>0 ? implode(",",$recipients) : 0;

                            $query = "select r.phone,r.id,t.created_at from recipient_tag t
                                        INNER JOIN recipients r ON r.id = t.recipient_id
                                        where r.phone IN ({$recipients_where})
                                        AND r.phone LIKE '%{$country}'
                                        AND t.created_at <= DATE_SUB(NOW(), INTERVAL {$hours} HOUR)
                                        AND t.created_at > '{$scheduler_now}'
                                         ";

                            $recipientsList = DB::select($query);


                        }

                        $ruleHistory->total_recipients = count($recipientsList);
                        $ruleHistory->save();

                        $chunks = createChunksOfData($recipientsList, 500);
                        foreach ($chunks as $chunk) {


                            /*
                             * save to database
                             */
                            $dbInsertChunks = createChunksOfData($chunk, 500);

                            foreach ($dbInsertChunks as $dbInsertChunk) {
                                $SchedulerDeliveryReportData = [];
                                $alreadies = [];
                                foreach ($dbInsertChunk as $oneRecipient) {
                                    $already = SchedulerDeliveryReport::where([['scheduler_id', '=', $this->rule->id] , ['recipient_id', '=', $oneRecipient->id], ])
                                        ->join('recipients', 'recipients.id', '=', 'scheduler_delivery_report.recipient_id')
                                        ->select(['recipients.phone', 'recipients.id'])
                                        ->get();
                                    if (!(count($already) > 0)) {
                                        $SchedulerDeliveryReportData[] = [
                                            'scheduler_history_id' => $ruleHistory->id,
                                            'recipient_id' => $oneRecipient->id,
                                            'created_at' => Carbon::now()->toDateTimeString(),
                                            'updated_at' => Carbon::now()->toDateTimeString(),
                                            'scheduler_id' => $this->rule->id,
                                        ];
                                    } else {
                                        $already = $already->toArray();
                                        $alreadies[] = $already[0];

                                    }
                                }

                                SchedulerDeliveryReport::insert($SchedulerDeliveryReportData);
                            }

                            $ids = collect($chunk)->pluck('id')->toArray();
                            $numbers = collect($chunk)->pluck('phone')->toArray();

                            /*
                             * numbers                  alread
                             * 1234343                  1234343
                             * 345345435                345345435
                             * 883838
                             * 65454
                             */
                            $temp_already_numbers = array_map(function($obj){
                                return $obj['phone'];
                            },$alreadies);



                              $temp_already_ids = array_map(function($obj){
                                return $obj['id'];
                            },$alreadies);



                            $numbers = array_unique($numbers);       // make it unique first
                            $ids = array_unique($ids);       // make it unique first


                            /*
                             * 3=>123423423443
                             */

                            $numbers =  array_values(array_diff($numbers,$temp_already_numbers));
                            $ids =  array_values(array_diff($ids,$temp_already_ids));


                            if ($this->rule->gateway == 'linkmobility') {
                                $notificationId = sendLinkMobilitySMSinBulk($numbers, $message->content, $message->id, $message->unsub_content, $this->rule, $ruleHistory, $ids);
                                //cool down sending speed
                                sleep(30);
                            }elseif($this->rule->gateway == 'messente'){
                                $notificationId = sendMessenteSMSinBulk($numbers, $message->content, $message->id, $message->unsub_content, $this->rule, $ruleHistory, $ids);
                            } else {
                                $notificationId = sendTwillioSMSinBulk($numbers, $message->content, $ruleHistory->id);
                            }

                        }

                    }
                }


            }
        } else {
            Log::error("Rule not found " . $this->rule_id);
        }
    }

    public function join_for_segment($tag_id)
    {
//        $recipients = Recipient::all()->join('recipient_tag','tag_id',$tag_id)->select('phone')->get();
        $recipients = DB::table('recipients')
            ->join('recipient_tag', 'recipients.id', '=', 'recipient_tag.recipient_id')
            ->select('recipients.phone')
            ->where('recipient_tag.tag_id', $tag_id)
            ->distinct()
            ->get()
            ->toArray();
        if (count($recipients) > 0) {
            $numbers = array_map(function ($obj) {
                return $obj->phone;
            }, $recipients);
            return $numbers;
        } else {
            return [];
        }
    }

}
