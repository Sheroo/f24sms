<div class="item-form-section list-section"
     data-slug="{{$controller_slug}}"
     data-section="list">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            List
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <?php
        $lists = isset($lists) ? $lists : [];
        ?>
        <div class="form-group clearfix">

            <label class="col-sm-3 control-label">Select a List</label>

            <div class="col-sm-9">
                <select name="list_id" class="select-picker form-control form-white te-input te-required">
                    <option value="">Select a List</option>
                    @foreach($lists as $list)
                        <option
                            {{$list->id==@$itemObj->list_id ? 'selected="selected"':''}}
                            value="{{$list->id}}">{{$list->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
</div>
