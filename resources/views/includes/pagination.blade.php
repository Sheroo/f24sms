@if(count($itemdata)>0)

    <?php
    $current_page = $itemdata->currentPage();
    $from_item_no = $itemdata->perPage() * ($itemdata->currentPage() - 1) + 1;
    $to_item_no = $itemdata->perPage() + $from_item_no - 1;
    $total_items = $totalRows;

    $to_item_no = $to_item_no > $total_items ? $total_items : $to_item_no;
    $last_page = ceil($total_items / $itemdata->perPage());
    $prev_page = $current_page - 1;
    $prev_page = $prev_page < 1 ? '' : $prev_page;
    $next_page = $current_page + 1;
    $next_page = $next_page > $last_page ? '' : $next_page;
    ?>

    <div class="te-pagination clearfix">

        <ul class="te-pagination-ul" data-total_rows="{{$total_items}}" data-per_page="{{$itemdata->perPage()}}">
            <li>
                <a href="#"
                   data-page_no="1"
                   data-slug="{{$controller_slug}}"
                   class="te-pagination-link first {{$current_page==1 ? 'te-current-page' : ''}}">
                    <img class="first-page-img" src="{{te_asset('images/svg-icons/first-page.svg')}}">
                    <img class="first-page-disabled-img"
                         src="{{te_asset('images/svg-icons/first-page-disabled.svg')}}">
                </a>
            </li>
            <li><a href="#"
                   data-page_no="{{$prev_page}}"
                   data-slug="{{$controller_slug}}"
                   class="te-pagination-link prev {{empty($prev_page) ? 'te-current-page' : ''}}"><i
                        class="fa fa-chevron-left"></i></a></li>
            <li><span>{{number_format($from_item_no,0)}} to {{number_format($to_item_no,0)}}
                        of {{number_format($total_items,0)}}</span></li>
            <li><a href="#"
                   data-page_no="{{$next_page}}"
                   data-slug="{{$controller_slug}}"
                   class="te-pagination-link next {{empty($next_page) ? 'te-current-page' : ''}}"><i
                        class="fa fa-chevron-right"></i></a></li>
            <li><a href="#"
                   data-page_no="{{$last_page}}"
                   data-slug="{{$controller_slug}}"
                   class="te-pagination-link last {{$current_page==$last_page ? 'te-current-page' : ''}}">
                    <img class="last-page-img" src="{{te_asset('images/svg-icons/last-page.svg')}}">
                    <img class="last-page-disabled-img"
                         src="{{te_asset('images/svg-icons/last-page-disabled.svg')}}">
                </a>
            </li>

        </ul>

        @if(!isset($page_entries))
            <select class="select-picker te-select3 te-per-page"
                    data-slug="{{$controller_slug}}"
                    data-width="80px"
                    style="width:80px; float:right;">
                @foreach($perpagelist as $perpage)
                    <option value="{{$perpage}}" {{$per_page==$perpage ? 'selected' : ''}}>{{$perpage}}</option>
                @endforeach
            </select>
        @endif

    </div>

@endif
