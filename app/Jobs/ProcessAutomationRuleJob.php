<?php

namespace App\Jobs;

use Aloha\Twilio\Twilio;
use App\Automation;
use App\Recipient;
use App\SchedulerDeliveryReport;
use App\SchedulerRunHistory;
use App\Segment;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessAutomationRuleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $rule_id;
    public $rule;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($rule_id)
    {
        $this->rule_id = $rule_id;
        $this->onQueue(Q_RULE_SYNC);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Log::info('Running automation for Id ' . $this->rule_id);
        $this->rule = Automation::where('id', $this->rule_id)->first();

        if ($this->rule) {
            $ruleHistory = SchedulerRunHistory::create(['scheduler_id' => $this->rule->id]);

            $list = $this->rule->mylist;
            if ($list) {

                $message = $this->rule->message;
                if ($message) {

                    if ($message->type == 'email') {

                        $cols = [];
                        $cols[] = DB::raw('id');
                        $cols[] = DB::raw('email as email');
//                        $recipientsList = $list->recipients()
//                            ->where('active', 1)
//                            ->select($cols)
//                            ->get()
//                            ->toArray();
                        $query = "select r.* from recipients r left join bounced_email b on b.email=r.email where list_id='{$list->id}' and b.id is null and r.email is not null and r.active=1";
                        $recipientsList = DB::select($query);

                        $ruleHistory->total_recipients = count($recipientsList);
                        $ruleHistory->save();

                        $chunks = createChunksOfData($recipientsList, 500);

                        foreach ($chunks as $chunk) {

                            $templateId = empty($message->template_id) || is_null($message->template_id) || !isset($message->template_id) ? '' : $message->template_id;
                            if ($this->rule->email_service == 'sendgrid') {

                                $response = sendEmails(collect($chunk)->pluck('email')->toArray(), $message->subject, $message->content, $templateId, $ruleHistory->id, $list->id);
                                $header = $response->headers();
                                $sendGridHeaderParts = explode(':', @$header[5]);


                            } else {

                                $response = send_email_through_mailgun(collect($chunk)->pluck('email')->toArray(), $message->subject, $message->content, $templateId, $ruleHistory->id, $list->id);
                                $sendGridHeaderParts[1] = rtrim(ltrim($response->getId(), '<'), '>');

                            }

                            $sgMessageId = @$sendGridHeaderParts[1];

                            //some time we get $sgMessageId =  application/json . so let's check that

                            if (stripos($sgMessageId, 'json') !== false) {
                                Log::info('Running automation for Id ' . $this->rule_id . " --> Header info");
                                Log::info($header);

                                try {
                                    Log::info('Running automation for Id ' . $this->rule_id . " --> Body info");
                                    Log::info($response->body());
                                } catch (\Exception $e) {
                                }

                                try {

                                    Log::info('Running automation for Id ' . $this->rule_id . " --> Complete response");
                                    Log::info($response);
                                } catch (\Exception $e) {
                                }
                            }
                            /*
                             * save to database
                             */
                            $dbInsertChunks = createChunksOfData($chunk, 500);
                            foreach ($dbInsertChunks as $dbInsertChunk) {
                                $SchedulerDeliveryReportData = [];
                                foreach ($dbInsertChunk as $oneRecipient) {
                                    $SchedulerDeliveryReportData[] = [
                                        'scheduler_history_id' => $ruleHistory->id,
                                        'recipient_id' => $oneRecipient->id,
                                        'message_sid' => $sgMessageId,
                                        'message_status' => 'not sent',
                                        'created_at' => Carbon::now()->toDateTimeString(),
                                        'updated_at' => Carbon::now()->toDateTimeString(),
                                    ];
                                }
                                SchedulerDeliveryReport::insert($SchedulerDeliveryReportData);
                            }

                        }


                    } elseif ($message->type == 'sms') {

                        if ($this->rule->scheduled_type == 'list') {
                            $cols = [];
                            $cols[] = DB::raw('id');
                            $cols[] = DB::raw('phone as phone');
                            $recipientsList = $list->recipients()
                                ->where([['active', '=', 1], ['list_id', '<>', 454],])
                                ->select($cols)
                                ->get()
                                ->toArray();
                        }

                        $ruleHistory->total_recipients = count($recipientsList);
                        $ruleHistory->save();

                        $chunks = createChunksOfData($recipientsList, 500);

                        foreach ($chunks as $chunk) {


                            /*
                             * save to database
                             */
                            $dbInsertChunks = createChunksOfData($chunk, 500);

                            foreach ($dbInsertChunks as $dbInsertChunk) {
                                $SchedulerDeliveryReportData = [];
                                $SchedulerDeliveryReportids = [];
                                foreach ($dbInsertChunk as $oneRecipient) {

                                    $SchedulerDeliveryReportData = [
                                        'scheduler_history_id' => $ruleHistory['id'],
                                        'recipient_id' => $oneRecipient['id'],
                                        'created_at' => Carbon::now()->toDateTimeString(),
                                        'updated_at' => Carbon::now()->toDateTimeString(),
                                    ];

                                    $SchedulerDeliveryReportids[] = SchedulerDeliveryReport::insertGetId($SchedulerDeliveryReportData);
                                }
                            }


                            $numbers = collect($chunk)->pluck('phone')->toArray();
                            $ids = collect($chunk)->pluck('id')->toArray();

//                            dd($SchedulerDeliveryReportids,$numbers,$ids);

                            if ($this->rule->gateway == 'linkmobility') {
                                $notificationId = sendLinkMobilitySMSinBulk($numbers, $message->content, $message->id, $message->unsub_content, $this->rule, $ruleHistory, $ids);
                                //cool down sending speed
                                sleep(30);
                            } elseif ($this->rule->gateway == 'messente') {
                                $notificationId = sendMessenteSMSinBulk($numbers, $message->content, $message->id, $message->unsub_content, $this->rule, $ruleHistory, $ids,$SchedulerDeliveryReportids);
                            } else {
                                $notificationId = sendTwillioSMSinBulk($numbers, $message->content, $ruleHistory->id);
                            }

                        }

                    }
                }


            }
        } else {
            Log::error("Rule not found " . $this->rule_id);
        }
    }
}
