<div class="item-form-section message_information-section"
     data-slug="{{$controller_slug}}"
     data-section="message_information">
    <div class="item-form-section-head">
        <h3>
            <span class="item-form-section-head-circle"></span>
            List Information
        </h3>
    </div>
    <div class="item-form-section-body clearfix">

        <div class="form-group clearfix">
            <label class="col-sm-3 control-label">Name</label>
            <div class="col-sm-9">
                <input class="form-control form-white te-input te-required"
                       name="name"
                       value="{{@$itemObj->name}}"
                       type="text"
                       placeholder="Name"/>
            </div>
        </div>

    </div>

</div>
